<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
require_once("common_functions.php");
checkLoggedIn();
?>
<link rel="stylesheet" type="text/css" media="screen" href="../css/zavuch_area.css" />
<div class="header_zavuch_area">
	<div class="header_title"><h1>Работа с отчетами</h1></div>
	<div class="header_user_block"><?php echo User_Menu_By_The_Rights(); ?></div>
	<div class="clearfix"></div>
</div>
