var Journal_Page_area_is_granted=false;
var first_sem_month = [ '9','10','11','12' ];
var second_sem_month = [ '1','2','3','4','5' ];
var semestr=0;
var year=0;
var NeedToUpdateJournalList=false;

function eliminateDuplicates(arr)
{
var i,len=arr.length,out=[],obj={};

for (i=0;i<len;i++)
	{
		obj[arr[i]]=0;
	}

for (i in obj)
	{
		out.push(i);
	}

if (out.length!=arr.length)
	{
		return 1;
	}	

return 0;
}

function set_disc_select(data) {
	$("#select_disc").empty();
	for (var number in data.disc)
	$("#select_disc").append( $('<option value="'+data.disc [number] ['discipline_id']+'">'+data.disc [number] ['title']+'</option>'));
	$("#select_disc :nth-child(1)").attr("selected", "selected");
}

function set_month_select(data) {
	$("#select_month").empty();
	for (var number in data.month)
		{
			$("#select_month").append( $('<option value="'+data.month [number] ['month_id']+'">'+data.month [number] ['title']+'</option>'));	
		}
	$("#select_month :nth-child(1)").attr("selected", "selected");		
}

function set_prepod_select(data) {
	$("#select_prepod").empty();
	for (var number in data.prepod)
		{
			$("#select_prepod").append( $('<option value="'+data.prepod [number] ['prepod_id']+'">'+data.prepod [number] ['prep_FIO']+'</option>'));	
		}
	$("#select_prepod :nth-child(1)").attr("selected", "selected");	
}

function clear_edit_lesson_form() {
	  // $("#edit_form_disc_1").empty();
	  // $("#edit_form_disc_2").empty();
	  // $("#edit_form_prepod_1").empty();
	  // $("#edit_form_prepod_2").empty();
	 
	  $("#edit_form_lesson_date").val("");
	  $("#edit_form_theme").val("");
	  $("#edit_form_homework").val("");
	  // $("#edit_form_integr").attr("checked",false);
	  // $("#check_row_table_1").attr("checked",false);
	  // $("#check_row_table_2").attr("checked",false);
	  //$("#edit_form_table").hide();
	  $("#Lesson_Edit_form div#status").hide();
	  $(".lesson_id").attr("id",'');
}

function clear_edit_lesson_form_without_select() {
	  $("#edit_form_lesson_date").val("");
	  $("#edit_form_theme").val("");
	  $("#edit_form_homework").val("");
	  // $("#edit_form_integr").attr("checked",false);
	  // $("#check_row_table_1").attr("checked",false);
	  // $("#check_row_table_2").attr("checked",false);
	  //$("#edit_form_table").hide();
	  $("#Lesson_Edit_form div#status").hide();
	  $(".lesson_id").attr("id",'');
}

function clear_edit_grades_form(){
	$("#is_attend").attr("checked",false);
	$("#grade_status").hide();
	$("#grade_container").html("");
}

function clear_edit_attest_form() {
		$("#edit_form_attest_date").val("");
		$("#att_status").hide();
		$(".att_form_lesson_id").attr("id",'');
}

function set_Calendar_Date_range() {
	
		$.ajax({
			 success: function(data) {
				first_semestr_year=data.date_range[1]['year']; //год первого семестра
				second_semestr_year=data.date_range[2]['year'];//год второго семестра
				month=$("#select_month option:selected").val();
				if (jQuery.inArray(month, first_sem_month)>=0)
				{semestr=1; year=first_semestr_year;}  
				if (jQuery.inArray(month, second_sem_month)>=0)
				{semestr=2; year=second_semestr_year;}  
				days_in_Month_Count = 32 - new Date(year, month-1, 32).getDate();
					  
				$("#edit_form_lesson_date").datepicker("option","maxDate",new Date(year, month-1 , days_in_Month_Count));
				$("#edit_form_lesson_date").datepicker("option","minDate",new Date(year, month-1 , 1));	
				
				$("#edit_form_attest_date").datepicker("option","maxDate",new Date(year, month-1 , days_in_Month_Count));
				$("#edit_form_attest_date").datepicker("option","minDate",new Date(year, month-1 , 1));	
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, обновите страницу: ");
			 },
			 data: { status:'get_date_range'}
		  });
		
}

function reload_journal_lesson() {
       $.ajax({
			beforeSend:function(){
				$("div#ajax_status").dialog("open");
			},
			 success: function(data) {
				$("div#ajax_status").dialog("close");
				$("div#Journal_Page").html(data);
				Journal_Page_area_is_granted=true;
				set_Journal_Area_Granted();
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, обновите страницу");
			 },
			 data: { group:$("#select_group option:selected").val(), subgroup:$("#select_subgroup option:selected").val(), month:$("#select_month option:selected").val(), disc:$("#select_disc option:selected").val(), prepod:$("#select_prepod option:selected").val(), status:'get_journal_list'}
		  });
}

function save_lesson() {
         
		var disc_array=[], disc_and_prepod_arr=[];
		
		disc_array.push($("#select_disc option:selected").val());
		disc_and_prepod_arr.push({'disc':$("#select_disc option:selected").val(),'prepod':$("#select_prepod option:selected").val() });
		
		//заполняем дисциплины и преподов, если урок интегрированный
		// if ($("#edit_form_integr").is(":checked"))
		// {
			// if ($("#check_row_table_1").is(":checked"))
			// {
				// disc_array.push($("#edit_form_disc_1 option:selected").val());
				// disc_and_prepod_arr.push({'disc':$("#edit_form_disc_1 option:selected").val(),'prepod':$('#edit_form_prepod_1').attr('id') });
			// }
			
			// if ($("#check_row_table_2").is(":checked") )
			// {
				// disc_array.push($("#edit_form_disc_2 option:selected").val());
				// disc_and_prepod_arr.push({'disc':$("#edit_form_disc_2 option:selected").val(),'prepod':$('#edit_form_prepod_2').attr('id') });
			// }
		// }
		//заполняем дисциплины и преподов, если урок интегрированный
		
		if (eliminateDuplicates(disc_array))
			{
				alert('Повторно указаны одинаковые дисциплины');
			}
		else
		{
			$.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							$("div#status").html('Занятие успешно записано').css({color:"green"}).fadeIn(1000);
							NeedToUpdateJournalList=true;
						}
					},
					error: function(obj,error) {
						$("div#status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data: 
					{ 'lesson_date':$("#edit_form_lesson_date").val(), 'lesson_theme':$("#edit_form_theme").val(),
					'homework':$("#edit_form_homework").val(), 'group':$("#select_group option:selected").val(), 'disc_and_prepod':disc_and_prepod_arr, 'status':'save_lesson','lesson_id':$(".lesson_id").attr("id"),'lesson_type':$("#select_lesson_type option:selected").val(),'is_att':0}
			});
		}
}

function save_grades() {
	//options=eval('('+$("#Grades_Edit_form #lesson_student_data").data('options')+')');
	
	options=$("#Grades_Edit_form #lesson_student_data").data('options');
	if ($("input[id*='is_attend']").is(":checked"))
	{
		attend_case=$("select#attend_case").val();
	}
	else
	{
		attend_case=0;
	}
	

	grade_arr=[];
	$("div#grade_str").each(function(){
		ind=$("#grade_container>div#grade_str").index($(this))+1;
				if ($(this).find("input#grade_otr").is(":checked"))
				{
					
					grade_arr.push({'number':ind,'grade':0});
				}
				else
				{
					grade_arr.push({'number':ind,'grade':$(this).find("select#grade_inp").val() });
				}
			}
		)
	
     $.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#grade_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							$("div#grade_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);
							
							$('td[id*="GradeCell '+options.student+'_les_'+options.lesson+'"]').data('gr_att',{'grade':grade_arr,'attend':attend_case});
							
							Cell_Text="";
							if (attend_case!=0)
							Cell_Text+=$("#attendance_select_data").data("options")[attend_case]+" ";
							
							if (grade_arr.length>0)
							{
								for (var number in grade_arr)
								{
									Cell_Text+=((grade_arr[number].grade=="0")?"отр":grade_arr[number].grade)+'/';
								}
								Cell_Text=Cell_Text.substring(0,Cell_Text.length-1);
							}
							
							
							$('td[id*="GradeCell '+options.student+'_les_'+options.lesson+'"]').html(Cell_Text);
							//NeedToUpdateJournalList=true;
						}
					},
					error: function(obj,error) {
						$("div#grade_status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data: 
					{ 'lesson':options.lesson,'student':options.student,'att_case':attend_case,'grade_arr':grade_arr,'status':'edit_grades'}
			});    
}

function save_attest() {
	
	disc_and_prepod_arr=[];
	disc_and_prepod_arr.push({'disc':$("#select_disc option:selected").val(),'prepod':$("#select_prepod option:selected").val() });

	$.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#att_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							$("div#att_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);
							NeedToUpdateJournalList=true;
						}
					},
					error: function(obj,error) {
						$("div#att_status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data: 
					{ 'lesson_date':$("#edit_form_attest_date").val(), 'lesson_theme':'Аттестация',
					'homework':'', 'group':$("#select_group option:selected").val(), 'disc_and_prepod':disc_and_prepod_arr, 'status':'save_attest','lesson_id':$(".att_form_lesson_id").attr("id"),'is_att':1}
			});
}

function save_att_grades() {
	
	var att_label; var label_type; var att_grade;
	options=$("#Attest_Grade_Edit_form #attest_lesson_student_data").data('options');
	if ($("#attest_label_select").val()=='grade')
		{
			att_label=$("#attest_grade_select").val();
			label_type='grade';
			att_grade=$("#attest_grade_select").val();
		}
	else 
		{
			att_label=$("#attest_label_select").val();
			label_type=$("#attest_label_select").val();
			att_grade='0';
		}
     $.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#attest_grade_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							$("div#attest_grade_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);
							
							//ставим данные
							$('td[id*="AttCell '+options.student+'_les_'+options.lesson+'"]').data('attest',{'type':label_type,'grade':att_grade});
							
							//ставим текст в ячейку
							Cell_Text=att_label;
							
							if (label_type=='grade')
								{
									Cell_Text=att_label;
								}
							else
								{
									if (att_label=='zach')
										Cell_Text='зач';
									else if (att_label=='nzach')
										Cell_Text='н/з';
									else if (att_label=='att')
										Cell_Text='а';
									else if (att_label=='na')
										Cell_Text='н/а';
								}
							
							
							$('td[id*="AttCell '+options.student+'_les_'+options.lesson+'"]').html(Cell_Text);
							//NeedToUpdateJournalList=true;
						}
					},
					error: function(obj,error) {
						$("div#attest_grade_status").html('Ошибка при записи данных. Попробуйте позже').css({color:"green"}).fadeIn(1000);
					},
					data: 
					{ 'lesson':options.lesson,'student':options.student,'att_label':att_label,'status':'edit_attest'}
			});    
}

function fill_grade_cell_by_data(data_arr)
{
	//посещаемость
	if (data_arr.attend!="0")
	{
		$("#Grades_Edit_form input#is_attend").prop("checked",true);
		$("#Grades_Edit_form select#attend_case").show();
		$("#Grades_Edit_form #case_label").show();
		
		objSel=document.getElementById('attend_case');
		for (var i=1;i<=objSel.options.length;i++)
			{
				if (objSel.options[i-1].value==data_arr.attend)
				objSel.options[i-1].selected=true;
				
			}
	}
	else
	{
		$("#Grades_Edit_form input#is_attend").prop("checked",false);
		$("#Grades_Edit_form select#attend_case").hide();
		$("#Grades_Edit_form #case_label").hide();
	}
		
	//успеваемость
	if (data_arr.grade.length>0)
	{
		
		for (var number in data_arr.grade)
			{
				$("#grade_container").append("<div id='grade_str'><div style='float:left;'><span><b>Отработка</b></span><input type='checkbox' id='grade_otr'><span id='grade_label'><b>Оценка</b></span><select name='grade' class='grade_inp' id='grade_inp'><option val='2'>2</option><option val='3'>3</option><option val='4'>4</option><option val='5'>5</option></select></div><div id='grade_del' style='float:left'><span><b>Удалить</b></span></div><div class='clearfix'></div></div>");
			}
			$("div#grade_str").each(function(){
				ind=$("#grade_container>div#grade_str").index($(this));
				if (data_arr.grade[ind].grade=="0")
				{
					$(this).find("input#grade_otr").prop("checked",true);
					$(this).find("span#grade_label").hide();
					$(this).find("select#grade_inp").hide();
				}
				else
				{
					$(this).find("input#grade_otr").prop("checked",false);
					$(this).find("span#grade_label").show();
					$(this).find("select#grade_inp").show();
					
					objSel=$(this).find("select#grade_inp").get(0);
					for (var i=1;i<=objSel.options.length;i++)
						{
							if (objSel.options[i-1].value==data_arr.grade[ind].grade)
							objSel.options[i-1].selected=true;
							
						}
				}
				
			}
			)
	}
}


function fill_att_cell_by_data(data_att)
{
	if (data_att.type=="0") // если аттестация вообще не установлена
				{
					objSel=$("#attest_label_select").get(0);
					for (var i=1;i<=objSel.options.length;i++)
						{
							if (i==1)
							{ objSel.options[i-1].selected=true; break; }
							
						}
					objSel=$("#attest_grade_select").get(0);
					for (var i=1;i<=objSel.options.length;i++)
						{
							if (i==1)
							{ objSel.options[i-1].selected=true; break; }
							
						}	
					$("#attest_grade_select").hide();	
				}
	else if (data_att.type=="grade") // если оценка
				{
					objSel=$("#attest_label_select").get(0);
					for (var i=1;i<=objSel.options.length;i++)
						{
							if (objSel.options[i-1].value=='grade')
							objSel.options[i-1].selected=true;
							
						}
					objSel=$("#attest_grade_select").get(0);
					for (var i=1;i<=objSel.options.length;i++)
						{
							if (objSel.options[i-1].value==data_att.grade)
							{ objSel.options[i-1].selected=true; break; }
							
						}	
					$("#attest_grade_select").show();	
				}
	else  //если символьное обозачение
				{
					objSel=$("#attest_label_select").get(0);
					for (var i=1;i<=objSel.options.length;i++)
						{
							if (objSel.options[i-1].value==data_att.type)
							objSel.options[i-1].selected=true;
							
						}
					$("#attest_grade_select").hide();	
				}			
	
}

function change_disc_in_edit_form(num) {
   $.ajax({
			 success: function(data) {
			 
				$("#edit_form_prepod_"+num).empty();
				for (var number in data.prepod)
					{
						$("#edit_form_prepod_"+num).append( $('<option value="'+data.prepod [number] ['prepod_id']+'">'+data.prepod [number] ['prep_FIO']+'</option>'));
					}
				$("#edit_form_prepod_"+num+" :nth-child(1)").attr("selected", "selected");
				
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перезагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), disc:$("#edit_form_disc_"+num+" option:selected").val(), month:$("#select_month option:selected").val(), status:'change_month'}
		 });
}

function set_Journal_Area_Granted(){
	$("#Overlay_Access_Denied").removeClass("Overlay_Access_Denied");
	$("#Overlay_Access_Denied").css({height:0})

}

function set_Journal_Area_Denied(){
	Journal_Area_Top=$("#Journal_Page").offset().top; Journal_Area_Height=$("#Journal_Page").outerHeight();
	$("#Overlay_Access_Denied").css({top:Journal_Area_Top, height:Journal_Area_Height}).addClass("Overlay_Access_Denied");
}

$(document).ready(function() {
	
	
	//инициализация диалоговой формы меню редактирования занятий
   $("div#Lesson_Edit_form").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: true, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 600, // Ширина окна
      height: "auto", // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { // Описание кнопок
         "Сохранить": save_lesson,
		 "Закрыть": function() {
			clear_edit_lesson_form_without_select();
            $(this).dialog("close"); // Закрыть окно
			if (NeedToUpdateJournalList) reload_journal_lesson();
			NeedToUpdateJournalList=false;
         },
      }
   });
   //инициализация диалоговой формы меню редактирования занятий

   //инициализация диалоговой формы меню редактирования оценок
   $("div#Grades_Edit_form").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: true, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 430, // Ширина окна
      height: "auto", // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { // Описание кнопок
         "Сохранить": save_grades,
		 "Закрыть": function() {
			clear_edit_grades_form();
            $(this).dialog("close"); // Закрыть окно
			//if (NeedToUpdateJournalList) reload_journal_lesson();
			//NeedToUpdateJournalList=false;
         },
      }
   });
   //инициализация диалоговой формы меню редактирования оценок
   
    //инициализация диалоговой формы меню редактирования отметок об аттестации
   $("div#Attest_Grade_Edit_form").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: true, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 600, // Ширина окна
      height: "auto", // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { // Описание кнопок
         "Сохранить": save_att_grades,
		 "Закрыть": function() {
			$("#attest_grade_status").hide();
            $(this).dialog("close"); // Закрыть окно
			//if (NeedToUpdateJournalList) reload_journal_lesson();
			//NeedToUpdateJournalList=false;
         },
      }
   });
   //инициализация диалоговой формы меню редактирования отметок об аттестации
   
   //инициализация диалоговой формы меню редактирования аттестаций
   $("div#Attest_Edit_form").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: true, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 400, // Ширина окна
      height: "auto", // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { // Описание кнопок
         "Сохранить": save_attest,
		 "Закрыть": function() {
			clear_edit_attest_form();
            $(this).dialog("close"); // Закрыть окно
			if (NeedToUpdateJournalList) reload_journal_lesson();
			NeedToUpdateJournalList=false;
         },
      }
   });
   //инициализация диалоговой формы меню редактирования аттестаций
   
   $("div#ajax_status").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: false, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 200, // Ширина окна
      height: 80, // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { }
   });
   
   	//сокрытие верхней панели диалоговых окон
	$("#Lesson_Edit_form").parent().find(".ui-dialog-titlebar-close").hide();
	$("#Attest_Edit_form").parent().find(".ui-dialog-titlebar-close").hide();
	$("#Grades_Edit_form").parent().find(".ui-dialog-titlebar-close").hide();
	$("#Attest_Grade_Edit_form").parent().find(".ui-dialog-titlebar-close").hide();
   
   $("#ajax_status").parent().find(".ui-dialog-titlebar").hide();
   $("#ajax_status").parent().find(".ui-dialog-titlebar-close").hide();
   
   //инициализация календаря для диалогового окна урока
   $("#edit_form_lesson_date").datepicker({
	  monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь',
	  'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	  numberOfMonths: 1, 
	  dateFormat:"yy-mm-dd" 
	  });
	// инициализация календаря для диалогового окна урока
	
	//инициализация календаря для диалогового окна аттестации
   $("#edit_form_attest_date").datepicker({
	  monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь',
	  'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	  numberOfMonths: 1, 
	  dateFormat:"yy-mm-dd" 
	  });
	// инициализация календаря для диалогового окна аттестации
	
	$.ajaxSetup({
			 url: "J_AJAX_Server.php",
			 type:"POST",
			 dataType: "json",
			 cache: false,
			 global:true
		  });
 
  //обработчики изменения элементов управления верхнего меню ... начало
   $("#select_group").change(function() {
   
		Journal_Page_area_is_granted=false;
		set_Journal_Area_Denied();
		 $.ajax({
			 success: function(data) {
				set_disc_select(data); 
				set_month_select(data);
				set_prepod_select(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), status:'change_groups'}
		  });
	  
   });
   
   $("#select_disc").change(function() {
   
	  Journal_Page_area_is_granted=false;
	  set_Journal_Area_Denied();
      $.ajax({
			 success: function(data) {
				set_month_select(data);
				set_prepod_select(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), disc:$("#select_disc option:selected").val(), status:'change_disc'}
		  });
	  
   });
   
   $("#select_month").change(function() {
   
	  Journal_Page_area_is_granted=false;
	  set_Journal_Area_Denied();
      $.ajax({
			 success: function(data) {
				set_prepod_select(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), disc:$("#select_disc option:selected").val(),
				month:$("#select_month option:selected").val(), status:'change_month'}
		  });
	  
   });
   
   $("#select_prepod").change(function() {
   
	  Journal_Page_area_is_granted=false;
	  set_Journal_Area_Denied();
   });
   
   $("#select_subgroup").change(function() {
   
	  Journal_Page_area_is_granted=false;
	  set_Journal_Area_Denied();
   });
   //обработчики изменения элементов управления верхнего меню ... конец
   
   
   //загрузка страницы журнала, а также взаимосвязанных данных... начало
   $("#load_journal").click(function() {
   
		clear_edit_lesson_form(); //подготовили форму для редактирования
		clear_edit_attest_form(); //подготовили форму атт для редактирования
		set_Calendar_Date_range();//установили интервал дат
		
		reload_journal_lesson();//загрузили страницу журнала
		   
		  // $.ajax({
			 // success: function(data) {
			// //это список дисциплин
				// if (data!='No data')
				// {	
					// for (var number in data.disc)
						// {
							// $("#edit_form_disc_1").append( $('<option value="'+data.disc [number] ['disc_id']+'">'+data.disc [number] ['title']+'</option>'));
							// $("#edit_form_disc_2").append( $('<option value="'+data.disc [number] ['disc_id']+'">'+data.disc [number] ['title']+'</option>'));
						// }
						// $("#edit_form_disc_1 :nth-child(1)").attr("selected", "selected");
						// $("#edit_form_disc_2 :nth-child(1)").attr("selected", "selected");
						
				// //а теперь преподов(вернее - препод-то один... не-а. может быть и не одиня. когда идет деление по подгруппам)
					// for (var number in data.prepod)
						// {
							// $("#edit_form_prepod_1").append( $('<option value="'+data.prepod [number] ['prepod_id']+'">'+data.prepod [number] ['prep_FIO']+'</option>'));
							// $("#edit_form_prepod_2").append( $('<option value="'+data.prepod [number] ['prepod_id']+'">'+data.prepod [number] ['prep_FIO']+'</option>'));
						// }
						// $("#edit_form_prepod_1 :nth-child(1)").attr("selected", "selected");
						// $("#edit_form_prepod_2 :nth-child(1)").attr("selected", "selected");
				// }	
			 // },
			 // error: function(obj,error) {
				// alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 // },
			 // data: { group:$("#select_group option:selected").val(), 
				// month:$("#select_month option:selected").val(), disc:$("#select_disc option:selected").val(), status:"get_disc_list_with_prepod"}
			// });
	  
   });
  //загрузка страницы журнала, а также взаимосвязанных данных ... конец
   
   
   //обработчик нажатия на клетку таблицы при заполнении сведений о уроке (оценка, посещаемость) ..начало
   $("#Journal_Page").on('click','td[id*="GradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$("#Grades_Edit_form #lesson_student_data").data('options',{student:$(this).data("options").student_id,lesson:$(this).data("options").lesson});
			fill_grade_cell_by_data($(this).data('gr_att'));
			//alert($(this).data('gr_att').grade[0].grade+" "+$(this).data('gr_att').attend);
			$("div#Grades_Edit_form").dialog("open");
		}
   });
	//обработчик нажатия на клетку таблицы при заполнении сведений о уроке (оценка, посещаемость) ..конец
   
    $("#Journal_Page").on('mouseenter','td[id*="GradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("lesson_column");
			$(this).stop(true,true).addClass("grade_cell_hover",500);
			//$(this).animate({opacity:0.5},1000);
		}
   });
   
   $("#Journal_Page").on('mouseleave','td[id*="GradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("grade_cell_hover");
			$(this).stop(true,true).addClass("lesson_column",500);
		}
   });
   
    $("#Journal_Page").on('mouseenter','td[id*="AttCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("att_column");
			$(this).stop(true,true).addClass("grade_cell_hover",500);
		}
   });
   
   $("#Journal_Page").on('mouseleave','td[id*="AttCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("grade_cell_hover",500);
			$(this).stop(true,true).addClass("att_column");
		}
   });
   
   //обработчик нажатия на клетку таблицы при заполнении сведений об аттестации ..начало
   $("#Journal_Page").on('click','td[id*="AttCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$("#Attest_Grade_Edit_form #attest_lesson_student_data").data('options',{student:$(this).data("options").student_id,lesson:$(this).data("options").lesson});
			fill_att_cell_by_data($(this).data('attest'));
			//alert($(this).data('gr_att').grade[0].grade+" "+$(this).data('gr_att').attend);
			$("div#Attest_Grade_Edit_form").dialog("open");
		}
   });
	//обработчик нажатия на клетку таблицы при заполнении сведений об фттестации ..конец
   
   //обработчики кнопок редактирования списка уроков .... начало//
   $("#Journal_Page").on('click','div#add_lesson',function() {
		if (Journal_Page_area_is_granted)
		{
			$("div#Lesson_Edit_form").dialog("open");
		}
   });
 
   $("#Journal_Page").on('click','div[id*="edit_lesson"]',function() {
		if (Journal_Page_area_is_granted)
		{
			l_id=$(this).attr('id');
			l_id=l_id.replace(/edit_lesson_/g,"");
			
			$.ajax({
			 success: function(data) {
				
				var count=0;
				for (var number in data.lesson_param)
						{
						
							$("div.lesson_id").attr("id",data.lesson_param [number] ['lesson_id']);
							$("#edit_form_lesson_date").val(data.lesson_param [number] ['lesson_date']);
							$("#edit_form_theme").val(data.lesson_param [number] ['theme']);
							$("#edit_form_homework").val(data.lesson_param [number] ['homework']);
							
							//устанавливаем значение для селекта типа занятия
							objSel=document.getElementById('select_lesson_type');
								for (var i=1;i<=objSel.options.length;i++)
								{
									if (objSel.options[i-1].value==data.lesson_param [number] ['lesson_type'])
									objSel.options[i-1].selected=true;
									
								}
							
							//все, что ниже, было написано для случая интеграции 2 и более дисциплин на одном уроке, отказался от этой байды - здесь оно нафиг не надо
							// if (data.lesson_param [number] ['discipline_id']==$("#select_disc option:selected").val())
								// continue;
							// else
								// count++;
								
							// if (count==1)
							// {
								
								// document.getElementById('edit_form_integr').checked=true;
								// $("#edit_form_table").show();
								// document.getElementById('check_row_table_1').checked=true;
							
								// objSel=document.getElementById('edit_form_disc_1');
								// for (var i=1;i<=objSel.options.length;i++)
								// {
									// if (objSel.options[i-1].value==data.lesson_param [number] ['discipline_id'])
									// objSel.options[i-1].selected=true;
									
								// }
								
								// objSel=document.getElementById('edit_form_prepod_1');
								// for (var i=1;i<=objSel.options.length;i++)
								// {
									// if (objSel.options[i-1].value==data.lesson_param [number] ['prepod_id'])
									// objSel.options[i-1].selected=true;
									
								// }
								
							// }
							// if (count==2)
							// {
								// document.getElementById('check_row_table_2').checked=true;
								
								// objSel=document.getElementById('edit_form_disc_2');
								// for (var i=1;i<=objSel.options.length;i++)
								// {
									// if (objSel.options[i-1].value==data.lesson_param [number] ['discipline_id'])
									// objSel.options[i-1].selected=true;
									
								// }
								
								// objSel=document.getElementById('edit_form_prepod_2');
								// for (var i=1;i<=objSel.options.length;i++)
								// {
									// if (objSel.options[i-1].value==data.lesson_param [number] ['prepod_id'])
									// objSel.options[i-1].selected=true;
									
								// }
							
							// }
						}
				$("div#Lesson_Edit_form").dialog("open");		
				
			 },
			 error: function(obj,error) {
				alert("Jшибка: "+error);
			 },
			 data: { lesson_id:l_id, status:'get_lesson_param'}
		  });
	  
		}
   });
   
   $("#Journal_Page").on('click','div[id*="delete_lesson"]',function() {
		if (Journal_Page_area_is_granted)
		{
			if (!confirm('Вы действительно хотите удалить занятие вместе со связанной информацией?'))
			return;
			
			//l_id=$(this).attr('id');
			//l_id=l_id.replace(/delete_lesson_/g,"");
			l_id=$(this).data('options').lesson;
		
			$.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					success: function(data) {
						if (data=="Success")
						{
							reload_journal_lesson();
						}
					},
					error: function(obj,error) {
						alert("Ошибка связи с сервером, попробуйте чуть позже ");
					},
					data: 
					{'status':'del_lesson','lesson_id':l_id}
			});
		}
   });
   //обработчики кнопок редактирования списка уроков...конец//
	  
	//обработчики кнопок редактирования списка аттестаций .... начало//
	$("#Journal_Page").on('click','div#add_att',function() {
		if (Journal_Page_area_is_granted)
		{
			$("div#Attest_Edit_form").dialog("open");
		}
   });
   
   $("#Journal_Page").on('click','div[id*="edit_att"]',function() {
		if (Journal_Page_area_is_granted)
		{
			// l_id=$(this).attr('id');
			// l_id=l_id.replace(/edit_att_/g,"");
			l_id=$(this).data('options').lesson;
			l_date=$(this).data('options').date;
			
			$("div.att_form_lesson_id").attr("id",l_id);
			$("#edit_form_attest_date").val(l_date);
			
			$("div#Attest_Edit_form").dialog("open");
		}
   }
   );
	//обработчики кнопок редактирования списка аттестаций .... конец//
	  
	//Обработчики элементов управления формы редактирования урока...начало//  
   // $("#edit_form_integr").click(function() {
		// if ($(this).is(":checked"))
		// {
			// $("table#edit_form_table").show();
		// }
		// else
		// {
			// $("table#edit_form_table").hide();
		// }
   // });
   
   // $("#edit_form_disc_1").change(function() {
       // change_disc_in_edit_form(1);
   // });
   
   // $("#edit_form_disc_2").change(function() {
      // change_disc_in_edit_form(2);
   // });
   //Обработчики элементов управления формы редактирования урока...конец//

   //Заложим возможность добавлять несколько оценок, на всякий
   //Обработчики элементов управления формы редактирования оценок...начало// 
   $("#add_grade").click(function(){
		grade_str_count=$('#grade_container #grade_str').size()+1;
		 if (grade_str_count>1)
		 {
			 alert('Может быть выставлена только одна оценка');
			 return;
		 }	
		$("#grade_container").append("<div id='grade_str'><div style='float:left;'><span><b>Отработка</b></span><input type='checkbox' id='grade_otr'><span id='grade_label'><b>Оценка</b></span><select name='grade' class='grade_inp' id='grade_inp'><option val='2'>2</option><option val='3'>3</option><option val='4'>4</option><option val='5'>5</option></select></div><div id='grade_del' style='float:left;'><b>Удалить</b></div><div class='clearfix'></div></div>");
   }
   )
   
   $("#is_attend").click(function(){
	if ($(this).is(":checked") )
		{
			$("select#attend_case").show();
			$("#case_label").show();
		}
		else
		{
			$("select#attend_case").hide();
			$("#case_label").hide();
			
			$("div#grade_str").each(function(){
				$(this).children("span#grade_label").show();
				$(this).children("select#grade_inp").show();
				$(this).children("input#grade_otr").attr("checked",false);
			}
			)
			
		}
   }
   )
   
   $("#grade_container").on('click','div[id*="grade_del"]',function() {
		 //str_number=$(this).attr('id');
		 //str_number=str_number.replace(/grade_del_/g,"");
		 ind=$("#grade_container>div#grade_str").index($(this).parent());
		 $("div#grade_str").eq(ind).remove();
   });
   
   //распечатка листа журнала 
   $(".print_container").on('click','div[id*="journal_print"]',function() {
		 //str_number=$(this).attr('id');
		 //str_number=str_number.replace(/grade_del_/g,"");
		  
		  $.ajax({
			 success: function(data) {
				alert(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), subgroup:$("#select_subgroup option:selected").val(), month:$("#select_month option:selected").val(), disc:$("#select_disc option:selected").val(), prepod:$("#select_prepod option:selected").val(), status:'print_journal_list'}
		  });
   });
   
   
   $("#grade_container").on('click','input[id*="grade_otr"]',function() {
		if ($("#is_attend").is(":checked")==false)
			{
				alert('Пропуск не установлен');
				return false;
			}
		ind=$("#grade_container>div#grade_str").index($(this).parent().parent());	
		if ($(this).is(":checked"))
			{
				$("div#grade_str").eq(ind).find("select#grade_inp").hide();
				$("div#grade_str").eq(ind).find("span#grade_label").hide();
			}
		else
			{
				$("div#grade_str").eq(ind).find("select#grade_inp").show();
				$("div#grade_str").eq(ind).find("span#grade_label").show();
			}
   });

   // $("#grade_container").on('change','select[id*="grade_inp"]',function() {
		// ind=$("#grade_container>div#grade_str").index($(this).parent());
		// $("div#grade_str").eq(ind).children("input#grade_otr").attr("checked",false);
   // });
    //Обработчики элементов управления формы редактирования оценок...конец//
	
	//Обработчики элементов управления формы редактирования оценок...начало//
	$("#attest_label_select").change(function(){
		
		var grade_select=$("#attest_grade_select");
		
		if ($(this).val()=='grade')
			grade_select.show();
		else grade_select.hide();
	}
	)
	//Обработчики элементов управления формы редактирования оценок...конец//	

});