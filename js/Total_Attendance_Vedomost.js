var Vedomost_Page_area_is_granted=false;
var first_sem_month = [ '9','10','11','12' ];
var second_sem_month = [ '1','2','3','4','5' ];
var semestrs_ar=[{id:1,title:'1 семестр'},{id:2,title:'2 семестр'}];
var month_ar=[{id:9,title:'Сентябрь'},{id:10,title:'Октябрь'},{id:11,title:'Ноябрь'},{id:12,title:'Декабрь'},{id:1,title:'Январь'},{id:2,title:'Февраль'},{id:3,title:'Март'},{id:4,title:'Апрель'},{id:5,title:'Май'}];

$(document).ready(function() {

	$("div#ajax_status").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: false, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 200, // Ширина окна
      height: 80, // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { }
   });
   
   	//сокрытие верхней панели диалоговых окон   
   $("#ajax_status").parent().find(".ui-dialog-titlebar").hide();
   $("#ajax_status").parent().find(".ui-dialog-titlebar-close").hide();

	$.ajaxSetup({
			 url: "J_AJAX_Server.php",
			 type:"POST",
			 dataType: "json",
			 cache: false
		  });

  //обработчики изменения элементов управления верхнего меню ... начало
   $("#select_group").change(function() {
		Vedomost_Page_area_is_granted=false;
   });
   //обработчики изменения элементов управления верхнего меню ... конец

   

   //загрузка страницы журнала
   $("#load_vedomost").click(function() {
	
		
		data_arr={ group:$("#select_group option:selected").val(), status:'get_total_attendance_vedomost'}
		
		$.ajax({
			beforeSend:function(){
				$("div#ajax_status").dialog("open");
			},
			 success: function(data) {
				$("div#ajax_status").dialog("close");
				$("div#Vedomost_Page").html(data);
				Vedomost_Page_area_is_granted=true;
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, попробуйте перезагрузить страницу: "+error);
			 },
			 data: data_arr
		  });
		
   });
  //загрузка страницы журнала

});
