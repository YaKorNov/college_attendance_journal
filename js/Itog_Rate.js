var Journal_Page_area_is_granted=false;
var first_sem_month = [ '9','10','11','12' ];
var second_sem_month = [ '1','2','3','4','5' ];
var semestr=0;
var year=0;
var NeedToUpdateJournalList=false;



function set_disc_select(data) {
	$("#select_disc").empty();
	for (var number in data.disc)
	$("#select_disc").append( $('<option value="'+data.disc [number] ['discipline_id']+'">'+data.disc [number] ['title']+'</option>'));
	$("#select_disc :nth-child(1)").attr("selected", "selected");
}

function set_semestr_select(data) {
	$("#select_semestr").empty();
	for (var number in data.semestr)
		{
			$("#select_semestr").append( $('<option value="'+data.semestr [number] ['semestr_id']+'">'+data.semestr [number] ['title']+'</option>'));
		}
	$("#select_semestr :nth-child(1)").attr("selected", "selected");
}

function set_prepod_div(data) {
	var prepods_string='';
	for (var number in data.prepod)
		{	
			prepods_string=prepods_string+data.prepod [number]['prep_FIO']+", ";
			//$("div.prepod_FIO").attr('id',data.prepod [number]['prepod_id']);
		}
		//prepods_string
		$("div.prepod_FIO").html(prepods_string.substr(0,prepods_string.length-2));
}


function reload_journal_lesson() {
       $.ajax({
			beforeSend:function(){
				$("div#ajax_status").dialog("open");
			},
			 success: function(data) {
				$("div#ajax_status").dialog("close");
				$("div#Journal_Page").html(data);
				Journal_Page_area_is_granted=true;
				set_Journal_Area_Granted();
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, попробуйте перезагрузить страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), subgroup:$("#select_subgroup option:selected").val(), semestr:$("#select_semestr option:selected").val(), disc:$("#select_disc option:selected").val(), status:'total_grades_get_journal_list'}
		  });
}



function save_grades() {

	options=$("#Total_Grade_form #total_grade_data").data('options');

     $.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#total_grade_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							 
							$("div#total_grade_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);
							 Cell_Text="";
							 if ($("#total_grade_select option:selected").val()=='grade' || $("#total_grade_select option:selected").val()=='dif_zach')
								 {
								  grade=$("#total_grade_rate_select").val(); zach='0';  Cell_Text+=$("#total_grade_rate_select").val();
								 }
							 else if ($("#total_grade_select option:selected").val()=='zach')
								 {
									grade='0'; zach=$("input#total_grade_zach").is(":checked");
									if ($("input#total_grade_zach").is(":checked"))
										Cell_Text+='зач';
									else
										Cell_Text+='н/з';	
								 }
							 
							 rate_opt={"rate_type":$("#total_grade_select option:selected").val(),"grade":grade,"zach":zach};
							 
							 $('td[id*="ExamsGradeCell '+options.student+'"]').data('options',{'student':options.student,'variant':options.variant,'rate_opt':rate_opt});
							 
							 $('td[id*="ExamsGradeCell '+options.student+'"]').html(Cell_Text);
							
						}
					},
					error: function(obj,error) {
						$("div#total_grade_status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data:
					{ 'variant':options.variant,'student':options.student,'group':$("#select_group option:selected").val(),'disc':$("#select_disc option:selected").val(),'semestr':$("#select_semestr option:selected").val(),'rate':$("#total_grade_rate_select").val(),'zach':$("input#total_grade_zach").is(":checked"),'rate_type':$("#total_grade_select option:selected").val(),status:'update_final_grades'}
			});
}

function save_semestr_grades() {

	options=$("#Semestr_Grade_form #semestr_grade_data").data('options');

     $.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#semestr_grade_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							 $("div#semestr_grade_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);

							 Cell_Text="";
							 if ($("#semestr_grade_select option:selected").val()=='grade')
								 {
								  grade=$("#semestr_grade_rate_select").val(); zach='0';  Cell_Text+=$("#semestr_grade_rate_select").val();
								 }
							 else if ($("#semestr_grade_select option:selected").val()=='zach')
								 {
									grade='0'; zach=$("input#semestr_grade_zach").is(":checked");
									if ($("input#semestr_grade_zach").is(":checked"))
										Cell_Text+='зач';
									else
										Cell_Text+='н/з';	
								 }
							 
							 rate_opt={"rate_type":$("#semestr_grade_select option:selected").val(),"grade":grade,"zach":zach};
							 
							 $('td[id*="TotalGradeCell '+options.student+' '+options.variant+'"]').data('options',{'student':options.student,'variant':options.variant,'rate_opt':rate_opt});
							 
							 $('td[id*="TotalGradeCell '+options.student+' '+options.variant+'"]').html(Cell_Text);
							
						}
					},
					error: function(obj,error) {
						$("div#semestr_grade_status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data:
					{ 'variant':options.variant,'student':options.student,'group':$("#select_group option:selected").val(),'disc':$("#select_disc option:selected").val(),'semestr':$("#select_semestr option:selected").val(),'rate':$("#semestr_grade_rate_select").val(),'zach':$("input#semestr_grade_zach").is(":checked"),'rate_type':$("#semestr_grade_select option:selected").val(),status:'update_final_grades'}
			});
}

function delete_grades() {

options=$("#Total_Grade_form #total_grade_data").data('options');

     $.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#total_grade_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							 $("div#total_grade_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);

							 $('td[id*="ExamsGradeCell '+options.student+'"]').data('options',{'student':options.student,'variant':options.variant,'rate_opt':{}});
							 
							 $('td[id*="ExamsGradeCell '+options.student+'"]').html("");
							
						}
					},
					error: function(obj,error) {
						$("div#total_grade_status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data:
					{ 'variant':options.variant,'student':options.student,'group':$("#select_group option:selected").val(),'disc':$("#select_disc option:selected").val(),'semestr':$("#select_semestr option:selected").val(),status:'delete_final_grades'}
			});

}

function delete_semestr_grades() {

options=$("#Semestr_Grade_form #semestr_grade_data").data('options');

     $.ajax({
					url: "J_AJAX_Lesson_Op_Server.php",
					beforeSend:function(){
						$("div#semestr_grade_status").hide();
					},
					success: function(data) {
						if (data=="Success")
						{
							 $("div#semestr_grade_status").html('Данные успешно записаны').css({color:"green"}).fadeIn(1000);

							 $('td[id*="TotalGradeCell '+options.student+' '+options.variant+'"]').data('options',{'student':options.student,'variant':options.variant,'rate_opt':{}});
							 
							 $('td[id*="TotalGradeCell '+options.student+' '+options.variant+'"]').html("");
							
						}
					},
					error: function(obj,error) {
						$("div#semestr_grade_status").html('Ошибка при записи данных. Повторите операцию чуть позже').css({color:"red"}).fadeIn(1000);
					},
					data:
					{ 'variant':options.variant,'student':options.student,'group':$("#select_group option:selected").val(),'disc':$("#select_disc option:selected").val(),'semestr':$("#select_semestr option:selected").val(),status:'delete_final_grades'}
			});

}

function fill_grade_cell_by_data(data_arr)
{
	if (data_arr['rate_type'] != undefined) //оценка проставлена
	{
		objSel=document.getElementById('total_grade_select');
		for (var i=1;i<=objSel.options.length;i++)
			{
				if (objSel.options[i-1].value==data_arr['rate_type'])
				objSel.options[i-1].selected=true;
			}
			
		if (data_arr['rate_type']=='grade' || data_arr['rate_type']=='dif_zach')
		{
			$("#total_grade_rate_label").show();
			$("#total_grade_rate_select").show();
			$("#total_grade_zach_label").hide();
			$("#total_grade_zach").hide();
			
			objSel=document.getElementById('total_grade_rate_select');
			for (var i=1;i<=objSel.options.length;i++)
				{
					if (objSel.options[i-1].value==data_arr['grade'])
					objSel.options[i-1].selected=true;
				}
		}
			else //если зачет 
		{
			$("#total_grade_rate_label").hide();
			$("#total_grade_rate_select").hide();
			$("#total_grade_zach_label").show();
			$("#total_grade_zach").show();
			
			if (data_arr['zach']==1)
			$("#total_grade_zach").prop("checked",true);
				else
			$("#total_grade_zach").prop("checked",false);
		}	
	}
	else //оценка не проставлена,
	{
		
		//в зависимости от селекта вида контроля - устанавливаем дефолтное значение переключателя
		objSel=document.getElementById('total_grade_select');
		for (var i=1;i<=objSel.options.length;i++)
			{
				if (objSel.options[i-1].value==$("#vid_control_select").val())
				objSel.options[i-1].selected=true;
			}
		//alert($("#vid_kontrol_select").val());
		if ($("#vid_control_select").val()=='zach')
		{	
			$("#total_grade_rate_label").hide();
			$("#total_grade_rate_select").hide();
			$("#total_grade_zach_label").show();
			$("#total_grade_zach").show();
			$("#total_grade_zach").prop("checked",false);
		}
			else //если экзамен или диф зачет
		{
			$("#total_grade_rate_label").show();
			$("#total_grade_rate_select").show();
			$("#total_grade_zach_label").hide();
			$("#total_grade_zach").hide();
		}		
			
	}


}

function fill_semestr_grade_cell_by_data(data_arr)
{
	if (data_arr['rate_type'] != undefined) //оценка проставлена
	{
		objSel=document.getElementById('semestr_grade_select');
		for (var i=1;i<=objSel.options.length;i++)
			{
				if (objSel.options[i-1].value==data_arr['rate_type'])
				objSel.options[i-1].selected=true;
			}
			
		if (data_arr['rate_type']=='grade')
		{
			$("#semestr_grade_rate_label").show();
			$("#semestr_grade_rate_select").show();
			$("#semestr_grade_zach_label").hide();
			$("#semestr_grade_zach").hide();
			
			objSel=document.getElementById('semestr_grade_rate_select');
			for (var i=1;i<=objSel.options.length;i++)
				{
					if (objSel.options[i-1].value==data_arr['grade'])
					objSel.options[i-1].selected=true;
				}
		}
			else //если зачет 
		{
			$("#semestr_grade_rate_label").hide();
			$("#semestr_grade_rate_select").hide();
			$("#semestr_grade_zach_label").show();
			$("#semestr_grade_zach").show();
			
			if (data_arr['zach']==1)
			$("#semestr_grade_zach").prop("checked",true);
				else
			$("#semestr_grade_zach").prop("checked",false);
		}	
	}
	else //оценка не проставлена,
	{
		//в зависимости от селекта вида контроля - устанавливаем дефолтное значение переключателя
		objSel=document.getElementById('semestr_grade_select');
		checked_value=$("#vid_control_select").val();
		
		if (checked_value=='dif_zach') //если выбрали диф.зачет, который по понятным причинам для семестровой оценки бессмысленен
			checked_value='grade';
		
		for (var i=1;i<=objSel.options.length;i++)
			{
				if (objSel.options[i-1].value==checked_value)
				objSel.options[i-1].selected=true;	
			}
		//alert($("#vid_kontrol_select").val());
		if ($("#vid_control_select").val()=='zach')
		{	
			$("#semestr_grade_rate_label").hide();
			$("#semestr_grade_rate_select").hide();
			$("#semestr_grade_zach_label").show();
			$("#semestr_grade_zach").show();
			$("#semestr_grade_zach").prop("checked",false);
		}
			else //если экзамен
		{
			$("#semestr_grade_rate_label").show();
			$("#semestr_grade_rate_select").show();
			$("#semestr_grade_zach_label").hide();
			$("#semestr_grade_zach").hide();
		}	
	}


}

function set_Journal_Area_Granted(){
	$("#Overlay_Access_Denied").removeClass("Overlay_Access_Denied");
	$("#Overlay_Access_Denied").css({height:0})

}

function set_Journal_Area_Denied(){
	Journal_Area_Top=$("#Journal_Page").offset().top; Journal_Area_Height=$("#Journal_Page").outerHeight();
	$("#Overlay_Access_Denied").css({top:Journal_Area_Top, height:Journal_Area_Height}).addClass("Overlay_Access_Denied");
}

$(document).ready(function() {

	//инициализация диалоговой формы меню редактирования занятий
   $("div#Total_Grade_form").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: true, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 500, // Ширина окна
      height: "auto", // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { // Описание кнопок
         "Сохранить": save_grades,
		 "Удалить": delete_grades,
		 "Отмена": function() {
			$("#total_grade_status").hide();
            $(this).dialog("close"); // Закрыть окно
         },
      }
   });
   
   $("div#Semestr_Grade_form").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: true, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 500, // Ширина окна
      height: "auto", // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { // Описание кнопок
         "Сохранить": save_semestr_grades,
		 "Удалить": delete_semestr_grades,
		 "Отмена": function() {
			$("#semestr_grade_status").hide();
            $(this).dialog("close"); // Закрыть окно
         },
      }
   });
   
   //инициализация диалоговой формы меню редактирования занятий
	$("div#ajax_status").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: false, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 200, // Ширина окна
      height: 80, // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { }
   });
   
     	//сокрытие верхней панели диалоговых окон
	$("#Total_Grade_form").parent().find(".ui-dialog-titlebar-close").hide();
	$("#Semestr_Grade_form").parent().find(".ui-dialog-titlebar-close").hide();
	
	$("#ajax_status").parent().find(".ui-dialog-titlebar").hide();
	$("#ajax_status").parent().find(".ui-dialog-titlebar-close").hide();

	$.ajaxSetup({
			 url: "J_AJAX_Server.php",
			 type:"POST",
			 dataType: "json",
			 cache: false
		  });

  //обработчики изменения элементов управления верхнего меню ... начало
   $("#select_group").change(function() {

		Journal_Page_area_is_granted=false;
		set_Journal_Area_Denied();
		 $.ajax({
			 success: function(data) {
				set_disc_select(data);
				set_semestr_select(data);
				set_prepod_div(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), status:'total_grades_change_groups'}
		  });

   });

   $("#select_disc").change(function() {

	  Journal_Page_area_is_granted=false;
	  set_Journal_Area_Denied();
      $.ajax({
			 success: function(data) {
				set_semestr_select(data);
				set_prepod_div(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), disc:$("#select_disc option:selected").val(), status:'total_grades_change_disc'}
		  });

   });

   $("#select_semestr").change(function() {

	  Journal_Page_area_is_granted=false;
	  set_Journal_Area_Denied();
      $.ajax({
			 success: function(data) {
				set_prepod_div(data);
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), disc:$("#select_disc option:selected").val(),
				semestr:$("#select_semestr option:selected").val(), status:'total_grades_change_semestr'}
		  });

   });

   //обработчики изменения элементов управления верхнего меню ... конец


   //загрузка страницы журнала
   $("#load_journal").click(function() {
		reload_journal_lesson();//загрузили страницу журнала
   });
  //загрузка страницы журнала


   //обработчик нажатия на клетку таблицы  ..начало
   $("body").on('click','td[id*="ExamsGradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			 $("#Total_Grade_form #total_grade_data").data('options',{student:$(this).data("options").student,variant:$(this).data("options").variant});
			 fill_grade_cell_by_data($(this).data('options').rate_opt);
			 $("div#Total_Grade_form").dialog("open");
		}
   });
   
    $("body").on('click','td[id*="TotalGradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			 $("#Semestr_Grade_form #semestr_grade_data").data('options',{student:$(this).data("options").student,variant:$(this).data("options").variant});
			 fill_semestr_grade_cell_by_data($(this).data('options').rate_opt);
			 $("div#Semestr_Grade_form").dialog("open");
		}
   });
	//обработчик нажатия на клетку таблицы  ..конец

	//Обработчики элементов управления формы редактирования оценок...начало//
	$("#total_grade_select").change(function(){

		if ($(this).val()=='grade' || $(this).val()=='dif_zach')
		{
			$("#total_grade_rate_label").show();
			$("#total_grade_rate_select").show();
			$("#total_grade_zach_label").hide();
			$("#total_grade_zach").hide();
		}
			else //если зачет 
		{
			$("#total_grade_rate_label").hide();
			$("#total_grade_rate_select").hide();
			$("#total_grade_zach_label").show();
			$("#total_grade_zach").show();
		}
	}
	)
	
	$("#semestr_grade_select").change(function(){

		if ($(this).val()=='grade')
		{
			$("#semestr_grade_rate_label").show();
			$("#semestr_grade_rate_select").show();
			$("#semestr_grade_zach_label").hide();
			$("#semestr_grade_zach").hide();
		}
			else //если зачет 
		{
			$("#semestr_grade_rate_label").hide();
			$("#semestr_grade_rate_select").hide();
			$("#semestr_grade_zach_label").show();
			$("#semestr_grade_zach").show();
		}
	}
	)
	//Обработчики элементов управления формы редактирования оценок...конец//

	
	$("#Journal_Page").on('mouseenter','td[id*="ExamsGradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("ekz_zach");
			$(this).stop(true,true).addClass("grade_cell_hover",500);
			//$(this).animate({opacity:0.5},1000);
		}
   });
   
   $("#Journal_Page").on('mouseleave','td[id*="ExamsGradeCell"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("grade_cell_hover");
			$(this).stop(true,true).addClass("ekz_zach",500);
		}
   });
   
    $("#Journal_Page").on('mouseenter','td[id*="sem_itog"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("sem_itog");
			$(this).stop(true,true).addClass("grade_cell_hover",500);
		}
   });
   
   $("#Journal_Page").on('mouseleave','td[id*="sem_itog"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("grade_cell_hover",500);
			$(this).stop(true,true).addClass("sem_itog");
		}
   });
   
     $("#Journal_Page").on('mouseenter','td[id*="year_itog"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("year_itog");
			$(this).stop(true,true).addClass("grade_cell_hover",500);
		}
   });
   
   $("#Journal_Page").on('mouseleave','td[id*="year_itog"]',function() {
		if (Journal_Page_area_is_granted)
		{
			$(this).stop(true,true).removeClass("grade_cell_hover",500);
			$(this).stop(true,true).addClass("year_itog");
		}
   });
});
