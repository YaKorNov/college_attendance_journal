var Student_List_area_is_granted=false;
var first_sem_month = [ '9','10','11','12' ];
var second_sem_month = [ '1','2','3','4','5' ];
var semestrs_ar=[{id:1,title:'1 семестр'},{id:2,title:'2 семестр'}];
var month_ar=[{id:9,title:'Сентябрь'},{id:10,title:'Октябрь'},{id:11,title:'Ноябрь'},{id:12,title:'Декабрь'},{id:1,title:'Январь'},{id:2,title:'Февраль'},{id:3,title:'Март'},{id:4,title:'Апрель'},{id:5,title:'Май'}];

function set_disc_select(data) {
	$("#select_disc").empty();
	for (var number in data.disc)
	$("#select_disc").append( $('<option value="'+data.disc [number] ['discipline_id']+'">'+data.disc [number] ['title']+'</option>'));
	$("#select_disc :nth-child(1)").attr("selected", "selected");
}

function set_Journal_Area_Granted(){
	$("#Overlay_Access_Denied").removeClass("Overlay_Access_Denied");
	$("#Overlay_Access_Denied").css({height:0})

}

function set_Journal_Area_Denied(){
	Journal_Area_Top=$("#Subgroup_Page").offset().top; Journal_Area_Height=$("#Subgroup_Page").outerHeight();
	$("#Overlay_Access_Denied").css({top:Journal_Area_Top, height:Journal_Area_Height}).addClass("Overlay_Access_Denied");
}

function compare_student_lists()
{
	$("table.subgroup_list tr").each(function(i){
		$(this).css({'background':'none'});
	});

	$("table.group_list tr").each(function(i){
		$(this).css({'background':'none'});
	});
	
	$("table.subgroup_list tr").each(function(i){
				if (i>0)
				{	
					sub_tr_id=$(this).attr('id');
					sub_tr=$(this);
					
					$("table.group_list tr").each(function(n){
						if (n>0)
						{	
							if (sub_tr_id==$(this).attr('id'))
							{
								sub_tr.css({'background':'#f6d0d9'});
								$(this).css({'background':'#f6d0d9'});
								
							}
						}
						
					});
				
				}
				
			});
}

$(document).ready(function() {
	
	$("div#ajax_status").dialog({
      autoOpen: false, // Открывать ли окно сразу
      closeOnEscape: false, // Закрывать ли при нажатии Esc
      title: "", // Заголовок
      position: ["center", 50], // Местоположение окна
      width: 200, // Ширина окна
      height: 80, // Высота окна
      draggable: true, // Перемещение
      resizable: false, // Изменение размера
      modal: true, // Модальное окно или нет
      show: null, // Эффект при открытии окна
      hide: null, // Эффект при закрытии окна
      buttons: { }
   });
   
   //инициализация календаря для диалогового окна урока
   $("#list_date").datepicker({
	  monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь',
	  'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'], 
	  dateFormat:"yy-mm-dd" 
	  });
	// инициализация календаря для диалогового окна урока
   
    //сокрытие верхней панели диалоговых окон   
   $("#ajax_status").parent().find(".ui-dialog-titlebar").hide();
   $("#ajax_status").parent().find(".ui-dialog-titlebar-close").hide();
	
	$.ajaxSetup({
			 url: "J_AJAX_Server.php",
			 type:"POST",
			 dataType: "json",
			 cache: false
		  });

  //обработчики изменения элементов управления верхнего меню ... начало
   $("#select_group").change(function() {
		Student_List_area_is_granted=false;
		set_Journal_Area_Denied();
		 $.ajax({
			 success: function(data) {
				set_disc_select(data); 
			 },
			 error: function(obj,error) {
				alert("Ошибка связи с сервером, перзагрузите страницу: "+error);
			 },
			 data: { group:$("#select_group option:selected").val(), status:'change_groups'}
		  });
   });
   
    $("#select_disc").change(function() {
		Student_List_area_is_granted=false;
		set_Journal_Area_Denied();
   });
   
    $("#select_subgroup").change(function() {
		Student_List_area_is_granted=false;
		set_Journal_Area_Denied();
   });
   //обработчики изменения элементов управления верхнего меню ... конец

   //обработчики кнопок добавления - удаления студентов из списка подгрупп
   $("#Subgroup_Page").on('click','div[id*="del_st_subgroup"]',function() {
		if (Student_List_area_is_granted)
		{
			// ind=$("#grade_container>div#grade_str").index($(this).parent().parent());
			// $("div#grade_str").eq(ind).remove();
			var sub_tr_id=$(this).parent().parent().attr('id');
			$("table.group_list tr").each(function(i){
				if (i>0)
				{	
					if (sub_tr_id==$(this).attr('id'))
					{
						$(this).css({'background':'none'});
						
					}
				}
				
			});
			
			$(this).parent().parent().remove();
		}
   });
   
   $("#Subgroup_Page").on('click','div[id*="move_st_group"]',function() {
		if (Student_List_area_is_granted)
		{
			var st_id=$(this).data("st_group_options").st_id;
			var st_FIO=$(this).data("st_group_options").st_FIO;
		
			var st_already_added=false;
			$("table.subgroup_list tr").each(function(i){
				if (i>0)
				{				
					//alert(st_id+" "+"st_"+$(this).attr('id'));
					if (("st_"+$(this).attr('id'))==st_id)
						{
							st_already_added=true;
							//break;
						}
				}
				
			});
			
			if (!st_already_added)
			{
				table_row="<tr id='"+st_id.replace(/st_/g,"")+"' style='background:#f6d0d9;'><td class='st_FIO' id='"+st_id+"'>"+st_FIO+"</td><td class='del_st_subgroup'><div class='del_st_subgroup' id='del_st_subgroup' data-st_subgroup_options='{\"st_id\":\""+st_id.replace(/st_/g,"")+"\",\"st_FIO\":\""+st_FIO+"\"}'><img src='/img/red-cross.png'></div></td></tr>";
				$("table.subgroup_list").append(table_row);
				$(this).parent().parent().css({'background':'#f6d0d9'});
			}
		}
   });
   //обработчики кнопок добавления - удаления студентов из списка подгрупп

   
   
   //загрузка 
   $("#load_lists_subgroups").click(function() {
	
		
		 data_arr={ group:$("#select_group option:selected").val(), subgroup:$("#select_subgroup option:selected").val(),disc:$("#select_disc option:selected").val(), day_of_month:$("#list_date").val(), status:'get_subgroups_lists'}
		
			$.ajax({
				beforeSend:function(){
					$("div#ajax_status").dialog("open");
				},
				 success: function(data) {
					$("div#ajax_status").dialog("close");
					$("div#Subgroup_Page").html(data);
					Student_List_area_is_granted=true;
					set_Journal_Area_Granted();
					compare_student_lists();
				 },
				 error: function(obj,error) {
					alert("Ошибка связи с сервером, попробуйте перезагрузить страницу: "+error);
				 },
				 data: data_arr
			  });
		
   });
  //загрузка 

   //сохранение
   $("#Subgroup_Page").on('click','button[id*="save_subgroup"]',function() {
		if (Student_List_area_is_granted)
		{
			
			var st_array=new Array();
			
			$("table.subgroup_list tr").each(function(i){
				if (i>0)
				{				
					//alert(st_id+" "+"st_"+$(this).attr('id'));	
					st_array.push($(this).attr('id'));
				}
				
			});
			
			data_arr={ student_array:st_array, group:$("#select_group option:selected").val(), subgroup:$("#select_subgroup option:selected").val(), disc:$("#select_disc option:selected").val(), status:'save_subgroup_list'};
			
			$.ajax({
				url: "J_AJAX_Lesson_Op_Server.php",
				beforeSend:function(){
					$("div#ajax_status").dialog("open");
				},
				 success: function(data) {
					$("div#ajax_status").dialog("close");
				 },
				 error: function(obj,error) {
					alert("Ошибка связи с сервером, попробуйте перезагрузить страницу: "+error);
				 },
				 data: data_arr
			  });
		}
   });
   //сохранение
});
