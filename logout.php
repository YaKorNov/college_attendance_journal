<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
require_once("common_functions.php");
checkLoggedIn();
destroyUserSession();
header("Location: /login.php");
?>
