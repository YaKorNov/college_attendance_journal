<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
require_once("bd_config.php");
require_once("bd_functions.php");
require_once("common_functions.php");
checkLoggedIn();
$conn = mysql_connect($dbhost, $dbuser, $dbpassword)or die("Connection Error: " . mysql_error());
mysql_select_db($db) or die("Error conecting to db.");
mysql_query("SET NAMES 'utf8'");

$title="Электронный журнал ОГБОУ СПО БПК";

function check_post_params()
{
	global $encrypt_salt; //определено в config-е
	if(isset($_POST["submit"])) {
	   $messages="";
	   $login_field_message=field_validator("логин", $_POST["login"], "alphanumeric", 4, 15);
	   $pass_field_message=field_validator("пароль", $_POST["password"], "alphanumeric", 4, 15);
	   if($login_field_message || $pass_field_message){
		  return $login_field_message." ".$pass_field_message;
	    }
	   
	   $user_data = check_pass_and_get_user_params($_POST["login"], $_POST["password"], $encrypt_salt);
		if( $user_data===false ) {
			 $messages="<b>Неверная пара логин/пароль</b>";
		}

	   if(!$messages)
	   {
		   createUserSession($user_data);
		   user_login_router();
		   exit();
	   }
	   else
		   return $messages;
   
	}
	else 
		return false;
}


$messages=check_post_params();
?>
 <html>
 <head>
 <title><?php echo $title; ?></title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 </head>
 <link rel="stylesheet" type="text/css" media="screen" href="/css/style.css" />
 <body>
	<div class="logo_and_text_title align_right">
		<div class="text_title"><h4><?php echo $title; ?></h4></div>
		<div class="image_title"><img src="/img/emblema.jpg" ></div>
		<div class="clearfix"></div>
	</div>
	<div class="clearfix"></div>
	<br/>
	<div class="author_form align_center">
		 <form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="POST">
			 <table>
				<tr>
					<td colspan="2"><b><div class="auth_title">Пожалуйста, авторизуйтесь</div></b></td>
				 </tr>
				 <tr>
					<td>Логин:</td>
					<td><input type="text" name="login" value="<?php echo isset($_POST["login"]) ? $_POST["login"] : "" ; ?>"
					maxlength="15"></td>
				 </tr>
				 <tr>
					<td>Пароль:</td>
					<td><input type="password" name="password" value="" maxlength="15"></td>
				</tr>
				 <tr>
					<td colspan="2"><div><div class="enter_to_system"><input name="submit" type="submit" value="Войти"></div></div></td>
				 </tr>
			 </table>
		 </form>
	 </div>
	 <br/>
	 <br/>
	 <br/>
	 <div class="error_string">
	 <?php
		if($messages!==false) { echo $messages; }
	  ?>
	 </div>
 </body>
 </html>