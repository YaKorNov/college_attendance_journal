<?

function get_nagr_strings()
{
	// $SQL=
			// "SELECT groups.group_id group_id, groups.literal literal, SUM( IF( IFNULL( rup.discipline_id, 0 ) , 1, 0 ) ) count
			// FROM groups
			// LEFT JOIN rup ON groups.spec_id = rup.spec_id
			// GROUP BY groups.group_id, groups.literal
			// HAVING count >0";
			
		$SQL=
			"SELECT groups.group_id group_id, groups.literal literal, SUM( IF( IFNULL( nagr.discipline_id, 0 ) , 1, 0 ) ) count
			FROM groups
			LEFT JOIN nagr ON groups.group_id = nagr.group_id
			GROUP BY groups.group_id, groups.literal
			HAVING count >0
			ORDER BY groups.literal ASC";
			
			$result=mysql_query($SQL) or die(mysql_error().'Error');
		
			$result_array=array();
				while($r=mysql_fetch_assoc($result)){
					foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
					$i++;
				}
	return $result_array;	
}

function check_refs_filling()
{
		$nagr_strings=get_nagr_strings();
		if (count($nagr_strings)==0) $nagr_filled=false; else $nagr_filled=true;
				
		$edu_periods=get_education_periods();
		if (count($edu_periods)==0) $edu_periods_filled=false; else $edu_periods_filled=true;
			
		if (!$nagr_filled || !$edu_periods_filled)
			{
				$message_text='';
				if (!$nagr_filled)
				$message_text.='Нагрузка по преподавтелям не заполнена! ';
				if (!$edu_periods_filled)
				$message_text.='Учебные периоды не указаны! ';
				$message_text.='Работа с журналом не может быть продолжена';
			}
		else
			{
				$message_text='Success';
			}
		
			
		return ($message_text);		
}


/********************* Функции для фомирования выборок из БД по РУП, оставлено на всякий сл **********************/
function get_disciplines_by_group_old($group)
{
$SQL=
				"SELECT rup.discipline_id discipline_id, disciplines.title title
				FROM rup
				LEFT JOIN disciplines ON rup.discipline_id = disciplines.discipline_id
				WHERE rup.spec_id = (
				SELECT spec_id
				FROM groups
				WHERE group_id =$group
				)
				GROUP BY rup.discipline_id, disciplines.title";

				$i=1;
				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				
				$result_array=array();
				while($r=mysql_fetch_assoc($result)){
					foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
					$i++;
				}
	return $result_array;			
}

function get_month_by_disc_old($group,$first_disc_in_list)
{

$SQL=
				"SELECT rup.semestr semestr
				FROM rup
				LEFT JOIN disciplines ON rup.discipline_id = disciplines.discipline_id
				WHERE rup.spec_id = (
				SELECT spec_id
				FROM groups
				WHERE group_id ='$group'
				) AND rup.discipline_id ='$first_disc_in_list'";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$month_array=array();
				$semestrs_array=array();
				while($r=mysql_fetch_assoc($result)){
					$semestrs_array[]=$r['semestr'];
				}
				
				//значения по дефолту
				$summer_month=array(6,7,8);
				if (in_array(1,$semestrs_array) && in_array(2,$semestrs_array))
					{
					 $start_month=1; $end_month=12; 
					}
				elseif (in_array(1,$semestrs_array))
					{
					 $start_month=9; $end_month=12;
					}
				elseif (in_array(2,$semestrs_array))
					{
						$start_month=1; $end_month=5;
					}
				
				
				
				//$i=1;
				$result_array=array();
				for($i=$start_month;$i<=$end_month;$i++)
				{
					if (in_array($i,$summer_month)) continue;
						$result_array[$i]['month_id']=$i;
						$result_array[$i]['title']=date('F',mktime(0,0,0,$i));	
				}
				
	return $result_array;			
}
/********************* Функции для фомирования выборок из БД по РУП, оставлено на всякий сл **********************/

/********************* Функции для фомирования выборок из БД по нагрузке, рабочий вариант **********************/
function get_disciplines_by_group($group)
{
$SQL=
				"SELECT nagr.discipline_id discipline_id, disciplines.title title
				FROM nagr
				LEFT JOIN disciplines ON nagr.discipline_id = disciplines.discipline_id
				WHERE nagr.group_id = $group
				GROUP BY nagr.discipline_id, disciplines.title
				ORDER BY disciplines.title ASC";

				$i=1;
				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				
				$result_array=array();
				while($r=mysql_fetch_assoc($result)){
					
					foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
					$i++;
				}
	return $result_array;			
}

function get_month_by_disc($group,$disc)
{

$SQL=
				"SELECT nagr.semestr semestr
				FROM nagr
				LEFT JOIN disciplines ON nagr.discipline_id = disciplines.discipline_id
				WHERE nagr.group_id ='$group'
				 AND nagr.discipline_id ='$disc'
				 GROUP BY nagr.semestr";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$semestrs_array=array();
				while($r=mysql_fetch_assoc($result)){
					$semestrs_array[]=$r['semestr'];
				}
				
				$summer_month=array(6,7,8);
				if (in_array(1,$semestrs_array) && in_array(2,$semestrs_array))
					{
					 $start_month=1; $end_month=12; 
					}
				elseif (in_array(1,$semestrs_array))
					{
					 $start_month=9; $end_month=12;
					}
				elseif (in_array(2,$semestrs_array))
					{
						$start_month=1; $end_month=5;
					}
				
				//$i=1;
				$result_array=array();
				for($i=$start_month;$i<=$end_month;$i++)
				{
					if (in_array($i,$summer_month)) continue;
						$result_array[$i]['month_id']=$i;
						$result_array[$i]['title']=date('F',mktime(0,0,0,$i,10));	
						//$result_array[$i]['title']=$i; //баг
				}
				
	return $result_array;			
}

function get_semestr_by_disc($group,$disc)
{

$SQL=
				"SELECT nagr.semestr semestr
				FROM nagr
				LEFT JOIN disciplines ON nagr.discipline_id = disciplines.discipline_id
				WHERE nagr.group_id ='$group'
				 AND nagr.discipline_id ='$disc'
				 GROUP BY nagr.semestr";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$semestrs_array=array();
				while($r=mysql_fetch_assoc($result)){
					$semestrs_array[]=$r['semestr'];
				}
				
	return $semestrs_array;			
}

function get_prepod_by_month($group,$disc,$month)
{

				$month_and_sem=array("1"=>"2","2"=>"2","3"=>"2","4"=>"2","5"=>"2","6"=>"2","9"=>"1","10"=>"1","11"=>"1","12"=>"1");
				$semestr=$month_and_sem[$month];
				
	return get_prepod_by_semestr($group,$disc,$semestr);			
}

function get_prepod_by_semestr($group,$disc,$semestr)
{

$SQL=
				"SELECT nagr.prepod_id prepod_id, CONCAT(prepods.surname,' ',prepods.name,' ',prepods.patronymic) prep_FIO
				FROM nagr
				LEFT JOIN prepods ON nagr.prepod_id = prepods.prepod_id
				WHERE nagr.group_id ='$group'
				 AND nagr.discipline_id ='$disc'
				 AND nagr.semestr ='$semestr'
				 ORDER BY prepods.surname ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$i=1;
				$result_array=array();
				while($r=mysql_fetch_assoc($result)){
					foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
					$i++;
				}
				
	return $result_array;			
}

function get_disc_by_group_month($group,$month)
{

				$month_and_sem=array("1"=>"2","2"=>"2","3"=>"2","4"=>"2","5"=>"2","6"=>"2","9"=>"1","10"=>"1","11"=>"1","12"=>"1");
				$semestr=$month_and_sem[$month];

$SQL=
				"SELECT nagr.discipline_id disc_id, disciplines.title title
				FROM nagr
				LEFT JOIN disciplines ON nagr.discipline_id = disciplines.discipline_id
				WHERE nagr.group_id ='$group'
				 AND nagr.semestr ='$semestr'
				 GROUP BY nagr.discipline_id,disciplines.title
				 ORDER BY disciplines.title ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$i=1;
				$result_array=array();
				while($r=mysql_fetch_assoc($result)){
					foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
					$i++;
				}
				
	return $result_array;			
}

/********************* Функции для фомирования выборок из БД по нагрузке, рабочий вариант **********************/

/*************** Функции для фомирования выборок из БД по для отображения одного листа журнала **********************/
function get_list_of_students($day_of_month,$group)
{

$sql = "SELECT * FROM kontingent_motion WHERE student_id IN (SELECT student_id FROM kontingent_motion WHERE `group_id` ='$group' AND motion_date<='$day_of_month') AND motion_date<='$day_of_month' ORDER BY student_id ASC, motion_date DESC";
$query=mysql_query($sql) or die("error");

$current_student_id='';
$student_array=array();

while($r=mysql_fetch_assoc($query))
	{
	if ($current_student_id!=$r['student_id'])
		{
		$current_student_id=$r['student_id'];
			if ($r['group_id']==$group)
			$student_array[]=$r['student_id'];
		}

	}

	If   (count($student_array)>0)
	{
		$student_array=implode(',',$student_array);
		$sql = "SELECT * FROM students WHERE student_id IN ($student_array)";
		$student_array=array();
		$query=mysql_query($sql) or die("error");
		while($r=mysql_fetch_assoc($query))
			{
				$student_array['st_'.$r['student_id']]=$r['surname']." ".$r{'name'}." ".$r['patronymic'];
			}
	}
	
return $student_array;
}

function get_one_journal_list_lessons($month,$disc,$group)
{

		$edu_periods=get_education_periods();
		if (count($edu_periods)==0)
		{
			return array();
		}

				if (is_array($month))
				{
					if (in_array(9,$month))  //1 семестр
							{
								$year=$edu_periods[0]['year']; //первая строка - первый семестр
								$begin_date=$year."-".$month[0]."-1"; // первый месяц 
								$last_date=date("t",strtotime($year."-".$month[count($month)-1]."-1"));// последний месяц
								$end_date=$year."-".$month[count($month)-1]."-".$last_date; 
							} 
								else //2 семестр
							{
								$year=$edu_periods[1]['year'];
								$begin_date=$year."-".$month[0]."-1"; // первый месяц 
								$last_date=date("t",strtotime($year."-".$month[count($month)-1]."-1"));// последний месяц
								$end_date=$year."-".$month[count($month)-1]."-".$last_date;
							}
					
				}
				else
				{
					if (in_array($month,array("9","10","11","12")))  //1 семестр
							{
								//$year=date("Y", mktime())-1;
								$year=$edu_periods[0]['year']; //первая строка - первый семестр
							} 
								else //2 семестр
							{
								$year=$edu_periods[1]['year'];
							}
					
							$begin_date=$year."-".$month."-1";
							$last_date=date("t",strtotime($year."-".$month."-1"));
							$end_date=$year."-".$month."-".$last_date;
				}
				
				
$SQL=
				"SELECT lessons.lesson_id lesson_id, lessons.lesson_date lesson_date,lessons.theme theme, lessons.homework homework, lessons.att att, lessons_types.title lesson_type 
				FROM `lessons`
				LEFT JOIN `lessons_types` ON lessons.lesson_type=lessons_types.lessons_type_id
				WHERE `lesson_date` BETWEEN '$begin_date' AND '$end_date'
				AND lessons.discipline_id='$disc' AND lessons.group_id='$group'
				ORDER BY lesson_date ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;			
}

function get_one_journal_list_lessons_by_prepod($month,$disc,$group,$prepod)
{

		$edu_periods=get_education_periods();
		if (count($edu_periods)==0)
		{
			return array();
		}

				if (is_array($month))
				{
					if (in_array(9,$month))  //1 семестр
							{
								$year=$edu_periods[0]['year']; //первая строка - первый семестр
								$begin_date=$year."-".$month[0]."-1"; // первый месяц 
								$last_date=date("t",strtotime($year."-".$month[count($month)-1]."-1"));// последний месяц
								$end_date=$year."-".$month[count($month)-1]."-".$last_date; 
							} 
								else //2 семестр
							{
								$year=$edu_periods[1]['year'];
								$begin_date=$year."-".$month[0]."-1"; // первый месяц 
								$last_date=date("t",strtotime($year."-".$month[count($month)-1]."-1"));// последний месяц
								$end_date=$year."-".$month[count($month)-1]."-".$last_date;
							}
					
				}
				else
				{
					if (in_array($month,array("9","10","11","12")))  //1 семестр
							{
								//$year=date("Y", mktime())-1;
								$year=$edu_periods[0]['year']; //первая строка - первый семестр
							} 
								else //2 семестр
							{
								$year=$edu_periods[1]['year'];
							}
					
							$begin_date=$year."-".$month."-1";
							$last_date=date("t",strtotime($year."-".$month."-1"));
							$end_date=$year."-".$month."-".$last_date;
				}
				
				//запрос формируем следующим способом: выбираем для аттестаций все занятия, для обычных занятий- только по укзанному преподу
				
				$SQL=
				"(
				SELECT lessons.lesson_id lesson_id, lessons.lesson_date lesson_date, lessons.theme theme, lessons.homework homework, lessons.att att, lessons_types.title lesson_type
				FROM `lessons` 
				LEFT JOIN `lessons_types` ON lessons.lesson_type = lessons_types.lessons_type_id
				INNER JOIN `prepods_lessons` ON lessons.lesson_id = prepods_lessons.lesson_id
				WHERE `lesson_date` BETWEEN '$begin_date' AND '$end_date'
				AND lessons.discipline_id = '$disc'
				AND lessons.group_id = '$group'
				AND lessons.att = '0'
				AND prepods_lessons.prepod_id = '$prepod'
				)
				UNION (

				SELECT lessons.lesson_id lesson_id, lessons.lesson_date lesson_date, lessons.theme theme, lessons.homework homework, lessons.att att, NULL lesson_type
				FROM `lessons` 
				WHERE `lesson_date` 
				BETWEEN '$begin_date' AND '$end_date'
				AND lessons.discipline_id = '$disc'
				AND lessons.group_id = '$group'
				AND lessons.att = '1'
				)
				ORDER BY lesson_date ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;			
}


function get_total_attendance($begin_date,$end_date,$group,$group_level)
{
	$SQL=
				"SELECT ".($group_level=='group_by_date'?"lessons.lesson_date lesson_date ,":"")." attendance.student_id student_id, COUNT(attendance.attend_case) count, SUM(attend_case.respectable) resp,
				(COUNT(attendance.attend_case)-SUM(attend_case.respectable)) unresp FROM `lessons`
				INNER JOIN `attendance` ON lessons.lesson_id=attendance.lesson_id
				INNER JOIN `attend_case` ON attendance.attend_case=attend_case.att_case_id
				WHERE `lesson_date` BETWEEN '$begin_date' AND '$end_date' AND lessons.att='0'
				AND lessons.group_id='$group'
				GROUP BY ".($group_level=='group_by_date'?"lessons.lesson_date,":"")." attendance.student_id
				ORDER BY lesson_date ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;	
}

function get_total_grades($group)
{
	$SQL=
				"SELECT `student_id` , `semestr` , `disc_id` , `variant` , 
				CASE `rate_type` 
				WHEN 'dif_zach' THEN `rate` 
				WHEN 'zach' THEN `zach` 
				WHEN 'grade' THEN `rate` 
				END grade,
				CASE `rate_type` 
				WHEN 'dif_zach' THEN 'dif_zach' 
				WHEN 'zach' THEN 'zach' 
				WHEN 'grade' THEN 'grade' 
				END rate_type
				FROM `final_grades` 
				WHERE group_id = $group";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;	
}

function get_lessons_dates($begin_date,$end_date,$group)
{
	$SQL=
				"SELECT lessons.lesson_date lesson_date FROM `lessons`
				WHERE `lesson_date` BETWEEN '$begin_date' AND '$end_date' AND lessons.att='0'
				AND lessons.group_id='$group'
				GROUP BY lessons.lesson_date
				ORDER BY lesson_date ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;	
}

function get_one_journal_list_grades($lessons_list)
{
$SQL=
			"SELECT lessons.lesson_id lesson_id, grades.student_id student_id, grades.grade_number grade_number, grades.rate rate, grades.rate_description rate_description
			FROM lessons
			LEFT JOIN `grades` ON lessons.lesson_id=grades.lesson_id
			WHERE lessons.lesson_id IN ($lessons_list)  AND lessons.att=0
			ORDER BY lesson_id, student_id, grades.grade_number ASC";
			//print_r($SQL);
			$result=mysql_query($SQL) or die(mysql_error().'Error');
				
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						if ($r['student_id']==NULL) continue; //пропсукаем занятия, где оценки не зафиксированы 
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;
}

function get_one_journal_list_attend($lessons_list)
{
$SQL=
			"SELECT lessons.lesson_id lesson_id, attendance.student_id student_id, attend_case.att_case_id case_id, attend_case.title case_title
			FROM lessons
			LEFT JOIN  `attendance` ON lessons.lesson_id = attendance.lesson_id
			LEFT JOIN  `attend_case` ON attendance.attend_case = attend_case.att_case_id
			WHERE lessons.lesson_id
			IN ($lessons_list) AND lessons.att=0
			ORDER BY lesson_id, student_id";
			
			$result=mysql_query($SQL) or die(mysql_error().'Error');
			//print_r (mysql_num_rows($result));	
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						if ($r['student_id']==NULL) continue; //пропсукаем занятия, где пропуски не зафиксированы 
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
			//print_r ($result_array);	
	return $result_array;
}

function get_one_journal_list_attest($lessons_list)
{
$SQL=
			"SELECT lessons.lesson_id lesson_id, attest.student_id student_id, attest.att_rate att_rate
			FROM lessons
			LEFT JOIN  `attest` ON lessons.lesson_id = attest.lesson_id
			WHERE lessons.lesson_id
			IN ($lessons_list) AND lessons.att>0
			ORDER BY lesson_id, student_id";
			
			$result=mysql_query($SQL) or die(mysql_error().$lessons_list);
			//print_r (mysql_num_rows($result));	
			
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						if ($r['student_id']==NULL) continue; //пропускаем аттестации, где аттестация не зафиксирована 
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
			//print_r ($result_array);	
	return $result_array;
}
/*************** Функции для фомирования выборок из БД по для отображения одного листа журнала **********************/

function get_attend_case_list()
{
$SQL=
				"SELECT att_case_id, title, full_case_name full_title FROM `attend_case`";
				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;			
}

function get_lessons_types()
{
$SQL=
				"SELECT lessons_type_id id, title FROM `lessons_types`";
				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				
	return $result_array;			
}

function get_lesson_param($lesson_id)
{
$SQL=
				"SELECT lesson_id, MONTH(lessons.lesson_date) month, lessons.group_id group_id, lessons.lesson_date lesson_date,lessons.theme theme, lessons.homework homework, lessons.lesson_type lesson_type,lessons.discipline_id discipline_id FROM `lessons` 
				WHERE lessons.lesson_id='$lesson_id' ";
				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$result_array=array();
				$i=1;
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r_val;
						}	
						$i++;
				}
				
				//все, что ниже, было написано для случая интеграции 2 и более дисциплин на одном уроке, отказался от этой байды - здесь оно нафиг не надо
				// while($r=mysql_fetch_assoc($result))
				// {
						// $month=$r['date'];	
						// $group_id=$r['group_id'];
						// $lesson_date=$r['lesson_date'];
						// $lesson_theme=$r['theme'];
						// $lesson_homework=$r['homework'];
						// $lesson_type=$r['lesson_type'];
						// $discipline_ids[]="'".$r['discipline_id']."'";
				// }
				// $discipline_ids=implode(',',$discipline_ids);
				
				// $month_and_sem=array("1"=>"2","2"=>"2","3"=>"2","4"=>"2","5"=>"2","9"=>"1","10"=>"1","11"=>"1","12"=>"1");
				// $semestr=$month_and_sem[$month];
// $SQL=
				// "SELECT '$lesson_id' lesson_id, '$lesson_date' lesson_date, '$lesson_theme' theme, '$lesson_homework' homework, $lesson_type lesson_type, nagr.discipline_id discipline_id, disciplines.title discipline_title, nagr.prepod_id prepod_id, CONCAT(prepods.surname,' ',prepods.name,' ',prepods.patronymic) prepod FROM nagr 
				// INNER JOIN prepods ON nagr.prepod_id=prepods.prepod_id
				// INNER JOIN disciplines ON nagr.discipline_id=disciplines.discipline_id
				// WHERE nagr.group_id='$group_id' AND  nagr.discipline_id IN ($discipline_ids) AND nagr.semestr=$semestr
				// ORDER BY discipline_id, prepod_id";

				// $result=mysql_query($SQL) or die(mysql_error().'Error2');
				
			
				// $result_array=array();
				// $i=1;
				// $disc_and_prep_array=array();
				// $row_array=array();
				// while($r=mysql_fetch_assoc($result))
				// {
					// $disc_and_prep_array[$r['discipline_id']][]=$r['prepod_id'];
					// foreach ($r as $r_key=>$r_val)
						// {
							// $row_array[$r_key]=$r_val;
						// }				
				// }
				
				// $i=1;
				
				// foreach ($disc_and_prep_array as $disc=>$prep_array)
					// {
						// foreach ($row_array as $r_key=>$r_val)
						// {
							// $result_array[$i][$r_key]=$r_val;
						// }	
						// $result_array[$i]['discipline_id']=$disc;
						// $result_array[$i]['prepod_id']=$prep_array;
						// $i++;
					// }
					
				
				
	return $result_array;			
}

function add_new_lesson($insert_array,$lesson_or_attest)
{
	//вот в итоге пришли к такому варианту запроса, т.к. интегрированные уроки я убрал, т.е. урок жестко привязан к одной дисциплине
$SQL=
				"INSERT INTO lessons( `lesson_date` , `theme` , `homework` , `group_id`,`att`, `lesson_type`,`discipline_id` ) 
				VALUES (
				'$insert_array[lesson_date]', '$insert_array[lesson_theme]', '$insert_array[homework]', '$insert_array[group]', '$insert_array[is_att]',". ($insert_array['is_att']=='1'?'NULL':$insert_array['lesson_type']) .", '".$insert_array['disc_and_prepod'][0]['disc']."')";

				$result=mysql_query($SQL) or die(mysql_error());
				$lesson_id = mysql_insert_id(); 
	
				$inserted_pl_strings=$inserted_dl_strings=array();
	
				foreach ($insert_array['disc_and_prepod'] as $str)
				{
					$inserted_pl_strings[]=" ('".$str['prepod']."','".$lesson_id."') ";
					$inserted_dl_strings[]=" ('".$str['disc']."','".$lesson_id."') ";
				}
	
	if ($lesson_or_attest=='lesson')
	{
		$SQL=
				 "INSERT INTO prepods_lessons( `prepod_id` , `lesson_id`  ) 
				VALUES ".implode(",",$inserted_pl_strings);

				 $result=mysql_query($SQL) or die(mysql_error());				
	}
	
	// $SQL=
				// "INSERT INTO discipline_lessons( `discipline_id` , `lesson_id` ) 
				// VALUES ".implode(",",$inserted_dl_strings);

				// $result=mysql_query($SQL) or die(mysql_error());
				
	return 1;			
}


function edit_lesson($edit_array,$lesson_or_attest)
{
$SQL=
				"UPDATE lessons SET `lesson_date`='$edit_array[lesson_date]' , `theme`= '$edit_array[lesson_theme]', `homework`= '$edit_array[homework]', `group_id`='$edit_array[group]', `lesson_type`=". ($edit_array['is_att']=='1'?'NULL':$edit_array['lesson_type']) .", `discipline_id`= '".$edit_array['disc_and_prepod'][0]['disc']."' WHERE lesson_id='$edit_array[lesson_id]'";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
	
				$inserted_pl_strings=$inserted_dl_strings=array();
	
				foreach ($edit_array['disc_and_prepod'] as $str)
				{
					$inserted_pl_strings[]=" ('".$str['prepod']."','".$edit_array['lesson_id']."') ";
					$inserted_dl_strings[]=" ('".$str['disc']."','".$edit_array['lesson_id']."') ";
				}
	
// $SQL=
				// "DELETE FROM discipline_lessons WHERE lesson_id='$edit_array[lesson_id]'";

				// $result=mysql_query($SQL) or die(mysql_error());	
	
// $SQL=
				// "INSERT INTO discipline_lessons( `discipline_id` , `lesson_id` ) 
				// VALUES ".implode(",",$inserted_dl_strings);

				// $result=mysql_query($SQL) or die(mysql_error());
	
	
	if ($lesson_or_attest=='lesson')
	{
		$SQL=
						"DELETE FROM prepods_lessons WHERE lesson_id='$edit_array[lesson_id]'";

						$result=mysql_query($SQL) or die(mysql_error());	
			
		$SQL=
						"INSERT INTO prepods_lessons( `prepod_id` , `lesson_id` ) 
						VALUES ".implode(",",$inserted_pl_strings);

						$result=mysql_query($SQL) or die(mysql_error());
	}
	return 1;			
}

function delete_lesson($del_array)
{
$SQL=
				"DELETE FROM lessons  WHERE lesson_id='$del_array[lesson_id]'";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
			
	return 1;			
}

function edit_grade_and_attend($edit_array)
{

$SQL[]=
		"DELETE FROM grades WHERE lesson_id='$edit_array[lesson]' AND student_id='$edit_array[student]'";
$SQL[]=
		"DELETE FROM attendance WHERE lesson_id='$edit_array[lesson]' AND student_id='$edit_array[student]'";
		
		if (count($edit_array['grade_arr'])>0)
		{
			$inserted_values=array();
			//$buffer="";
			foreach ($edit_array['grade_arr'] as $str)
			{
				$inserted_values[]=" ('".$edit_array['lesson']."','".$edit_array['student']."','".$str['number']."','".$str['grade']."') ";
				//$buffer.=print_r($str);
			}				
			
			$SQL[]=
			"INSERT INTO grades (`lesson_id` , `student_id`,`grade_number`, `rate`) VALUES ". implode(",",$inserted_values);
		}
			
		if ($edit_array['att_case'])
		{	
				$SQL[]=
				"INSERT INTO attendance (`lesson_id` , `student_id`,`attend_case`) VALUES ('$edit_array[lesson]', '$edit_array[student]','$edit_array[att_case]' ) ";

				
		}		
		
		foreach ($SQL as $single_query) 
		{
			if (!mysql_query($single_query)) die(mysql_error().'Error');
		}
		
	return 1;			
}

function edit_attest($edit_array)
{

$SQL[]=
		"DELETE FROM attest WHERE lesson_id='$edit_array[lesson]' AND student_id='$edit_array[student]'";
		
$SQL[]=
		"INSERT INTO attest (`lesson_id` , `student_id`,`att_rate`) VALUES ('$edit_array[lesson]', '$edit_array[student]','$edit_array[att_label]' ) ";

		foreach ($SQL as $single_query) 
		{
			if (!mysql_query($single_query)) die(mysql_error().'Error');
		}
		
	return 1;			
}

function update_final_grades($edit_array)
{

$SQL[]=
		"DELETE FROM final_grades WHERE group_id='$edit_array[group]' AND student_id='$edit_array[student]' AND semestr='$edit_array[semestr]' AND disc_id='$edit_array[disc]' AND variant='$edit_array[variant]'";

		
		if ($edit_array['rate_type']=='zach')
		{
			
			$edit_array['zach']=($edit_array['zach']=='true'?"'1'":"'0'");
			$SQL[]=
			"INSERT INTO final_grades(`group_id`,`student_id`,`semestr`,`disc_id`,`variant`,`zach`,`rate_type`) VALUES ('$edit_array[group]' , '$edit_array[student]','$edit_array[semestr]', '$edit_array[disc]', '$edit_array[variant]',  $edit_array[zach], '$edit_array[rate_type]')";
		}
			else
		{
		
			$SQL[]=
			"INSERT INTO final_grades(`group_id`,`student_id`,`semestr`,`disc_id`,`variant`,`rate`,`rate_type`) VALUES ('$edit_array[group]' , '$edit_array[student]','$edit_array[semestr]', '$edit_array[disc]', '$edit_array[variant]', '$edit_array[rate]',  '$edit_array[rate_type]')";
		}
			
		
			
		
		foreach ($SQL as $single_query) 
		{
			if (!mysql_query($single_query)) die(mysql_error().$single_query);
		}
		//die $edit_array['zach'];
	return 1;			
}

function delete_final_grades($edit_array)
{

$SQL[]=
		"DELETE FROM final_grades WHERE group_id='$edit_array[group]' AND student_id='$edit_array[student]' AND semestr='$edit_array[semestr]' AND disc_id='$edit_array[disc]' AND variant='$edit_array[variant]'";


		foreach ($SQL as $single_query) 
		{
			if (!mysql_query($single_query)) die(mysql_error().$single_query);
		}
	return 1;			
}

function save_subgroup_student_list($save_array)
{
	$SQL=
		"DELETE FROM subgroups WHERE group_id='$save_array[group]' AND subgroup_number='$save_array[subgroup]' AND discipline_id='$save_array[disc]' ";
		$result=mysql_query($SQL) or die(mysql_error());
	$SQL=
		"INSERT INTO subgroups(group_id,subgroup_number,discipline_id) VALUES ('$save_array[group]','$save_array[subgroup]','$save_array[disc]') ";
		$result=mysql_query($SQL) or die(mysql_error());
		$subgroup_id = mysql_insert_id(); 
	
	if (isset($save_array['student_array']))
	{
		$inserted_values=array();
		foreach ($save_array['student_array'] as $st_id)
		{
			$inserted_values[]="('$subgroup_id','$st_id')";
		}
		$inserted_values=implode(",",$inserted_values);
			
		$SQL=
			"INSERT INTO subgroup_kontingent(subgroup_id,student_id) VALUES $inserted_values ";
			$result=mysql_query($SQL) or die(mysql_error());	
	}	
	return 1;
}

function get_education_periods()
{
	$SQL=
				"SELECT semestr_number, year FROM `education_periods` ORDER BY semestr_number ASC";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				if (mysql_num_rows($result)<2)
				{
					return array(); //если в БД не указаны семестры и учебный год, то возвращать нечего
				}
				
				$i=0;
				$year_and_semestr_array=array();
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$year_and_semestr_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				return $year_and_semestr_array;
				
}

function get_subgroup_list($group,$subgroup,$disc)
{
$SQL=
				"SELECT subgroup_kontingent.student_id student_id, students.surname surname, students.name name, students.patronymic patronymic  FROM subgroup_kontingent INNER JOIN `students` ON subgroup_kontingent.student_id=students.student_id
				WHERE subgroup_id IN
				(
					SELECT subgroup_id
					FROM subgroups 
					WHERE subgroups.group_id = $group AND subgroups.subgroup_number = $subgroup AND subgroups.discipline_id = $disc
				)
				";

				$i=1;
				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				
				$result_array=array();
				while($r=mysql_fetch_assoc($result)){
					
					$result_array[$r['student_id']]=$r['surname']." ".$r['name']." ".$r['patronymic'];
					$i++;
				}
	return $result_array;			
}

/*************** Функции для фомирования выборок из БД по для отображения одного листа журнала **********************/
function get_total_grades_by_group_disc_sem($groups,$discs,$semestrs)
{
	if (is_array($groups))
				{
					foreach ($groups as $index=>$group)
					{
						$groups[$index]='"'.$group.'"';
					}
					$groups=implode(',',$groups);
				}
				else
				{
					$groups='"'.$groups.'"';
				}
	
	if (is_array($discs))
				{
					foreach ($discs as $index=>$disc)
					{
						$discs[$index]='"'.$disc.'"';
					}
					$discs=implode(',',$discs);
				}
				else
				{
					$discs='"'.$discs.'"';
				}
	
	if (is_array($semestrs))
				{
					foreach ($semestrs as $index=>$semestr)
					{
						$semestrs[$index]='"'.$semestr.'"';
					}
					$semestrs=implode(',',$semestrs);
				}
				else
				{
					$semestrs='"'.$semestrs.'"';
				}
	
	$SQL=
				"SELECT student_id, group_id, disc_id, semestr, variant, rate, zach, rate_type FROM final_grades WHERE group_id IN ($groups) AND disc_id IN ($discs) AND semestr IN ($semestrs)
				ORDER BY student_id,group_id, disc_id, semestr";

				$result=mysql_query($SQL) or die(mysql_error().'Error');
				
				$i=1;
				$result_array=array();
				while($r=mysql_fetch_assoc($result))
				{
						foreach ($r as $r_key=>$r_val)
						{
							$result_array[$i][$r_key]=$r[$r_key];
						}	
						$i++;
				}
				return $result_array;
				
		
}
/*************** Функции для фомирования выборок из БД по для отображения одного листа журнала **********************/

/*************** Функции для проверок авторизаций пользователя ******************************************************/
function check_pass_and_get_user_params($login, $password,$encrypt_salt) {
   
   $query="SELECT login,user_role FROM users WHERE login='$login' and AES_DECRYPT(pass,'$encrypt_salt')='$password'";
   $result=mysql_query($query) or die("The error was happened when checking password: ".mysql_error());

   if(mysql_num_rows($result)>0) {
     $row=mysql_fetch_array($result);
     return array('user_role'=>$row['user_role'],'login'=>$row['login']); 
   }
   
   $query="SELECT login,surname,name,patronymic FROM prepods WHERE login='$login' and AES_DECRYPT(pass,'$encrypt_salt')='$password'";
   $result=mysql_query($query) or die("The error was happened when checking password: ".mysql_error());

   if(mysql_num_rows($result)>0) {
     $row=mysql_fetch_array($result);
     return array('user_role'=>'prepod','FIO'=>$row['surname'].' '.$row['name'].' '.$row['patronymic']); 
   }
   
   return false;
}
/*************** Функции для проверок авторизаций пользователя *****************************************************/

?>