<?php
class Blogmodel extends Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::Model();
    }
    
    function get_last_three_entries()
    {
        $query = $this->db->get('url', 3);
        return $query->result();
    }


}
?>