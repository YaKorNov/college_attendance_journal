<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<!--студенты-->
<div class="admin_header_text">Списки студентов</div>
<br/>
<table id="rowed3"></table>
<div id="prowed3"></div>
<br />
<br />
<div class="admin_header_text">Приказы учебной части по студентам</div>
<br/>
<!--движение контингента-->
<table id="rowed3_d"></table>
<div id="prowed3_d"></div>
<br />

<?php
$MasterTable="students";
$colNamesMasterTable="'id','Фамилия','Имя','Отчество'";
$colModelMasterTable="
{name:'student_id',index:'student_id', width:90, editable:false},
{name:'surname',index:'surname', width:90, editable:true},
{name:'name',index:'name', width:90, editable:true},
{name:'patronymic',index:'patronymic', width:90, editable:true}";
$sortNameMasterTable="'student_id'";

$Detail_Table="kontingent_motion";
$SQL = "SELECT group_id, literal FROM groups ORDER BY literal ASC";
$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
$group_op=array();
while($r=mysql_fetch_assoc($result)){
	$group_op[]=$r['group_id'].":".$r['literal'];	
}	
$group_op[]="NULL:выбыл";
$group_options=implode(';',$group_op);


$colNamesDetail_Table="'Действия','Учебная группа/Статус студента','Дата приказа','Номер приказа'";
$colModelDetail_Table="
{name:'act',index:'act', width:75,sortable:false},
{name:'group_id',index:'group_id', width:90, editable:true, edittype:'select',editoptions:{value:'$group_options'}},
{name:'motion_date',index:'motion_date', width:90, editable:true},
{name:'order_number',index:'order_number', width:90, editable:true}";
$sortNameDetail_Table="'kont_id'";
?>

<!-- далее идет костыль для добавления возможности добавлять новые записи в движуху, стандартные свойства плагина это то ли не позволяют. то ли не нашел все-таки в документации-->
<div id="addrowed3_d" style="display:none;">
			<table id="TblGrid_rowed3_d" class="EditTable" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr id="tr_group">
						<td class="CaptionTD">Учебная группа/Статус студента</td>
						<td class="DataTD">
							<select name="groups_range" id="groups_range">
							<?php
								$i=1;
								foreach($group_op as $go)
									{
										$key_and_value=explode(":",$go);
										$g_id=$key_and_value[0];
										$g_name=$key_and_value[1];
										
										echo '<option value="'.$g_id.'" >'.$g_name.''."\n";
										$i++;
									}
							?>	
							</select>
						</td>
						</tr>
					<tr rowpos="2" id="tr_motion_date">
						<td class="CaptionTD">Дата приказа</td><td>
							<input type="text" id="motion_date" name="motion_date">
						</td>
					</tr>
					<tr rowpos="2" id="tr_order_number">
						<td class="CaptionTD">Номер приказа</td><td>
							<input type="text" id="order_number" name="order_number">
						</td>
					</tr>
				</tbody>
			</table>
</div>

<script>
var lastsel_Master;
var lastsel_Detail;
var MasterRecord;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/JQ_Grid_Server_Refs_Show.php?table=<?php echo $MasterTable; ?>',
	width:800,
	height:300,
	datatype: "json",
   	colNames:[<?php echo $colNamesMasterTable; ?>],
   	colModel:[
   		<?php echo $colModelMasterTable; ?>	
   	],
   	rowNum:10,
   	rowList:[10,20,30],
   	pager: '#prowed3',
   	sortname: <?php echo $sortNameMasterTable; ?>,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id == null) 
			{ 
			id=0; 
			if(jQuery("#rowed3_d").jqGrid('getGridParam','records')>0 ) 
				{ 
				jQuery("#rowed3_d").jqGrid('setGridParam',{url:"JS_Grid_server/Server_St_Motion_Show.php?q=1&id="+id,page:1}); 
				//jQuery("#rowed3_d").jqGrid('setCaption',"Студент: "+id).trigger('reloadGrid'); 
				jQuery("#rowed3_d").trigger('reloadGrid'); 
				} 
			}
		else 
			{ 
			jQuery("#rowed3_d").jqGrid('setGridParam',{url:"JS_Grid_server/Server_St_Motion_Show.php?q=1&id="+id,page:1}); 
			//jQuery("#rowed3_d").jqGrid('setCaption',"Студент") .trigger('reloadGrid'); 
			jQuery("#rowed3_d").trigger('reloadGrid'); 
			}
		if(id && id!==lastsel_Master){
			lastsel_Master=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=<?php echo $MasterTable; ?>",
	caption: "Студенты"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

jQuery("#rowed3_d").jqGrid({
   	url:'JS_Grid_server/Server_St_Motion_Show.php?q=1&id=0',
	width:800,
	height:300,
	datatype: "json",
   	colNames:[<?php echo $colNamesDetail_Table; ?>],
   	colModel:[
   		<?php echo $colModelDetail_Table; ?>	
   	],
   	rowNum:10,
	
   	rowList:[10,20,30],
   	pager: '#prowed3_d',
   	sortname: <?php echo $sortNameDetail_Table; ?>,
	viewrecords: true,
	sortorder: "desc",
	gridComplete: function(){
		var ids = jQuery("#rowed3_d").jqGrid('getDataIDs');
		for(var i=0;i < ids.length;i++){
			var cl = ids[i];
			be = "<input style='height:22px;width:40px;' type='button' value='Ред' onclick=\"jQuery('#rowed3_d').editRow('"+cl+"');  \" />"; 
			se = "<input style='height:22px;width:40px;' type='button' value='Сохр' onclick=\"jQuery('#rowed3_d').saveRow('"+cl+"');\"  />"; 
			ce = "<input style='height:22px;width:40px;' type='button' value='Отм' onclick=\"jQuery('#rowed3_d').restoreRow('"+cl+"');     \" />"; 
			jQuery("#rowed3_d").jqGrid('setRowData',ids[i],{act:be+se+ce});
			
		}	
	},
	onSelectRow: function(id){
		if(id && id!==lastsel_Detail){
			//MasterRecord=jQuery("#rowed3").jqGrid('getGridParam','selrow');
			jQuery('#rowed3_d').jqGrid('restoreRow',lastsel_Detail);
			jQuery('#rowed3_d').jqGrid('editRow',id,
				{
					keys:true,
					oneditfunc:pickdates,
					url:'JS_Grid_server/Server_St_Motion_Edit.php?table=kontingent_motion&student_id='+lastsel_Master
				}
			);
			lastsel_Detail=id;
			
		}
	},
	editurl: 'JS_Grid_server/Server_St_Motion_Edit.php?table=kontingent_motion',
	caption: "Движение контингента"
});
jQuery("#rowed3_d").jqGrid('navGrid',"#prowed3_d",{edit:false,add:false,del:true}).navButtonAdd("#prowed3_d",{caption:"Добавить новую запись",buttonicon:"ui-icon-add",onClickButton:function(){$("div#addrowed3_d").dialog("open");},position:"first"});;

function pickdates(id)
{
	jQuery("#"+id+"_motion_date","#rowed3_d").datepicker({dateFormat:"yy-mm-dd"});
}

function save_student_motion()
{
	MasterRecord=jQuery("#rowed3").jqGrid('getGridParam','selrow');
	if (MasterRecord==undefined)
		{
			alert('Выберите студента');
			return;
		}
	md=$("#motion_date").val();
	group=$("#groups_range").val();
	order_numb=$("#order_number").val();
	
	$.ajax({
		url:'JS_Grid_server/Server_St_Motion_Edit.php',
		dataType:"html",
		data:{
			table:'<?php echo $Detail_Table; ?>',
			motion_date:md,
			student_id:MasterRecord,
			group_id:group,
			order_number:order_numb,
			oper:'add'
		}
	});
	
	jQuery("#rowed3_d").trigger('reloadGrid');
	//$("#motion_date").val('');
}

$(document).ready(function()
	{
	
		$("div#addrowed3_d").dialog({
		  autoOpen: false, // Открывать ли окно сразу
		  closeOnEscape: true, // Закрывать ли при нажатии Esc
		  title: "Добавить движение", // Заголовок
		  position: ["center", 50], // Местоположение окна
		  width: 300, // Ширина окна
		  height: "auto", // Высота окна
		  draggable: true, // Перемещение
		  resizable: false, // Изменение размера
		  modal: true, // Модальное окно или нет
		  show: null, // Эффект при открытии окна
		  hide: null, // Эффект при закрытии окна
		  buttons: { // Описание кнопок
			 "Сохранить": save_student_motion,
			 "Закрыть": function() {
				$(this).dialog("close"); // Закрыть окно
				}
				}
		});
	
		$("#motion_date").datepicker({
		monthNames:['Январь','Февраль','Март','Апрель','Май','Июнь',
		'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
		dateFormat:"yy-mm-dd" });
	
	}
)

</script>