<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<div class="admin_header_text">Справочники</div>
<br/>
<form name="myform" method="POST" action="index.php?action=ref_edit">
		<span>Выберите вид справочника из списка</span>
		<select name="table_refs" onchange="document.forms[0].submit();">'
	
			<?php
			$tables_ref_array=array('specs'=>'Специальности','students'=>'Студенты','disciplines'=>'Дисциплины','attend_case'=>'Причины пропусков занятий','lessons_types'=>'Типы занятий');
			$i=1;
			foreach($tables_ref_array as $table_name=>$table_desc)
				{
				if (isset($table))
					{
						if ($table==$table_name) $selected='selected'; else $selected='';
					}
					else 
					{
						if ($i==1) 
						{
						$selected='selected';
						$table=$table_name;
						} 
						else 
						{$selected='';
						}						
					}
				
					
				echo '<option '.$selected.' value="'.$table_name.'" >'.$table_desc.''."\n";
				$i++;
				}
				
			$SQL = "describe $table";
			$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
			$fields_array=array();
			
			while($r=mysql_fetch_assoc($result)){
				$fields_array[]=$r;	
			}	
			?>	
		
		</select>
</form>
<br>
<table id="rowed3"></table>
<div id="prowed3"></div>
<br />

<?php

$colModel=$sortName=$colNames=''; 
$i=1;
//print_r($fields_array);
foreach ($fields_array as $field)
	{
			$colNames.=field_synonym($field['Field'],$table);
			if ($field['Key']=='PRI')
				{
				  $editable='false'; $sortName="'".$field['Field']."'";
				}
			else
				{
				  $editable='true'; 
				}
				
			
			$colStr="{name:'".$field['Field']."',index:'".$field['Field']."', width:90, editable:$editable}";
			if ($table=='attend_case' && $field['Field']=='respectable')
			{			
				$options="{value:'y:n'}";
				$colStr="{name:'".$field['Field']."',index:'".$field['Field']."', width:90, edittype:'checkbox', editable:$editable, editoptions:".$options."}";				
			}
			
			$colModel.=$colStr;
			
			if ($i!=count($fields_array)) 
			{
			$colNames.=',';
			$colModel.=',';
			}
			$i++;
	}
	
echo <<<HTML
<script language="javascript">
var lastsel;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/JQ_Grid_Server_Refs_Show.php?table=$table',
	width:800,
	height:400,
	datatype: "json",
   	colNames:[$colNames],
   	colModel:[
   		$colModel	
   	],
   	rowNum:10,
   	rowList:[10,30,50],
   	pager: '#prowed3',
   	sortname: $sortName,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id && id!==lastsel){
			jQuery('#rowed3').jqGrid('restoreRow',lastsel);
			jQuery('#rowed3').jqGrid('editRow',id,
			{
			keys:true
			}
			);
			lastsel=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=$table",
	caption: "Справочники"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

</script>
HTML;


function field_synonym($field,$table)
{
	
	$synonym_array=array('students'=>array('student_id'=>'id','name'=>'Имя','surname'=>'Фамилия','patronymic'=>'Отчество'),'disciplines'=>array('discipline_id'=>'id','title'=>'Дисциплина'),'specs'=>array('spec_id'=>'id','kod_spec'=>'Код специальности','title'=>'Название специальности'),'prepods'=>array('prepod_id'=>'id','name'=>'Имя','surname'=>'Фамилия','patronymic'=>'Отчество','login'=>'Логин','pass'=>'Пароль'),'attend_case'=>array('att_case_id'=>'id','title'=>'Краткое наименование','respectable'=>'Уважительная','full_case_name'=>'Полное наименование'),'lessons_types'=>array('lessons_type_id'=>'id','title'=>'Наименование'));
	
	
		if (isset($synonym_array[$table][$field]))
		{
			return "'".$synonym_array[$table][$field]."'";
		}		
		else
		{
			return "";
		}
	
	
}

?>