<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
require_once("../bd_config.php");
require_once("../bd_functions.php");
require_once("../common_functions.php");
checkLoggedIn();
// The name of THIS file - для ограничения доступа на включаемые скрипты
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
$conn = mysql_connect($dbhost, $dbuser, $dbpassword)or die("Connection Error: " . mysql_error());
mysql_select_db($db) or die("Error conecting to db.");
mysql_query("SET NAMES 'utf8'");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Админка</title>

<link href="style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="journal.js"></script>

<!-- Подключение JQ_Grid -->
<link rel="stylesheet" type="text/css" media="screen" href="../css/JQ_Grid/ui-lightness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../css/JQ_Grid/ui.jqgrid.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../css/admin_style.css" />
<script src="../js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="../js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="../js/JQ_Grid/i18n/grid.locale-ru.js" type="text/javascript"></script>
<script src="../js/JQ_Grid/jquery.jqGrid.min.js" type="text/javascript"></script>
<!-- Подключение JQ_Grid -->

<body>
<div class="header_admin">
	<div class="header_title"><h1>Административный раздел</h1></div>
	
	<div class="logo_and_text_title align_right">
		<div class="text_title"><h4>Электронный журнал ОГБОУ СПО БПК</h4></div>
		<div class="image_title"><img src="/img/emblema.jpg" ></div>
		<div class="clearfix"></div>
		<div class="header_user_block"><?php echo User_Menu_By_The_Rights(); ?></div>
	</div>
	
	<div class="clearfix"></div>
</div>


<div class="content-wrap">
	<div class="menu_column">
		<div class="menu_wrap">
			<div><a href="index.php?action=education_period">Учебный период</a></div>
			<div><a href="index.php?action=ref_edit">Первичные справочники</a></div>
			<div><a href="index.php?action=groups_edit">Учебные группы</a></div>
			<div><a href="index.php?action=student_motion">Студенты, приказы учебной части по студентам</a></div>
			<!--div><a href="index.php?action=spec_rup">Специальности, учебные планы</a></div-->
			<div><a href="index.php?action=prepod_nagr">Преподаватели, учебная нагрузка преподавателей</a></div>
		</div>
	</div>


	<div class="content_column">
			<?php
			
			$action=@$_REQUEST['action'];
			$table=@$_REQUEST['table_refs'];
			$spec_select=@$_REQUEST['spec_select'];
			
			if($action=='ref_edit') //часть 1...... Если смотрим справочники
				{
					 require_once "action_with_refs.php";
				} 
			elseif ($action=='groups_edit') //часть 2...... Если смотрим группы
				{
					require_once "groups_edit.php";
				}
			elseif ($action=='student_motion') //часть 3...... Если смотрим движуху
				{
					require_once "student_motion.php";
				}
			elseif ($action=='spec_rup') //часть 3...... Если смотрим спец-ти и уч.план
				{
					require_once "spec_with_rup.php";
				}	
			elseif ($action=='prepod_nagr') //часть 4...... Если смотрим преподов и нагрузку
				{
					require_once "prepods_with_nagr.php";
				}	
			elseif ($action=='education_period') //часть 6, она же первая ...... Если смотрим учебный период
				{
					require_once "education_period.php";
				}
			elseif ($action=='users') //юзеры
				{
					if (Admin_SubMenu_By_The_Rights())
					{
						require_once "users_area.php";
					}	
				}	
			elseif ($action=='prepods_login') //преподы
				{
					if (Admin_SubMenu_By_The_Rights())
					{
						require_once "prepods_login.php";
					}
				}
			elseif ($action==NULL) 
				{
					require_once "education_period.php";
				}	
		
			?>

	</div>

	<?php
	if (Admin_SubMenu_By_The_Rights())
	{
	?>
	<div class="menu_column">
		<div class="menu_wrap">
			<div><a href="index.php?action=users">Управление учетками администраторов</a></div>
			<div><a href="index.php?action=prepods_login">Управление учетками преподавателей</a></div>
		</div>
	</div>
	<?php
	}
	?>

<div class="clearfix"></div>
</div>

</body>
</html>
