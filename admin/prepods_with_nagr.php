<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<!--преподаватели-->
<div class="admin_header_text">Преподаватели</div>
<br/>
<table id="rowed3"></table>
<div id="prowed3"></div>
<br/>
<br/>
<div class="admin_header_text">Учебная нагрузка преподавателей</div>
<br />
<!--нагрузка-->
<table id="rowed3_d"></table>
<div id="prowed3_d"></div>
<br />

<?php
$sem_op_for_table="1:1 семестр;2:2 семестр";

$SQL = "SELECT discipline_id, title FROM disciplines ORDER BY title ASC";
$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
$disciplines_op=array();
while($r=mysql_fetch_assoc($result)){
	$disciplines_op[]=$r['discipline_id'].":".$r['title'];	
}	
$disciplines_op_for_table=implode(';',$disciplines_op);

$MasterTable="prepods";
$colNamesMasterTable="'id','Фамилия','Имя','Отчество'";
$colModelMasterTable="
{name:'prepod_id',index:'prepod_id', width:90, editable:false},
{name:'surname',index:'surname', width:90, editable:true},
{name:'name',index:'anme', width:90, editable:true},
{name:'patronymic',index:'patronymic', width:90, editable:true}";
$sortNameMasterTable="'prepod_id'";

$Detail_Table="nagr";
								
$SQL = "SELECT group_id, literal FROM groups ORDER BY literal ASC";
$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
$groups_op=array();
while($r=mysql_fetch_assoc($result)){
	$groups_op[]=$r['group_id'].":".$r['literal'];	
}	
$groups_op=implode(';',$groups_op);

$colNamesDetail_Table="'Действия','Учебная группа','Дисциплина','Семестр'";
$colModelDetail_Table="
{name:'act',index:'act', width:75,sortable:false},
{name:'group_id',index:'group_id', width:90, editable:true, edittype:'select',editoptions:{value:'$groups_op'}},
{name:'discipline_id',index:'discipline_id', width:90, editable:true, edittype:'select',editoptions:{value:'$disciplines_op_for_table'}},
{name:'semestr',index:'semestr', width:90, editable:true, edittype:'select',editoptions:{value:'$sem_op_for_table'}}";
$sortNameDetail_Table="'nag_id'";
														
?>

<!-- далее идет костыль для добавления возможности добавлять новые записи в нагрузку, стандартные свойства плагина это то ли не позволяют. то ли не нашел все-таки в документации-->
<div id="addrowed3_d" style="display:none;">
			<table id="TblGrid_rowed3_d" class="EditTable" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr id="tr_group">
						<td class="CaptionTD">Семестр</td>
						<td class="DataTD">
							<select name="semesters" id="semesters">
							<?php
								$i=1;
								$sem_op=explode(';',$sem_op_for_table);
								foreach($sem_op as $sem)
									{
										$key_and_value=explode(":",$sem);
										$sem_id=$key_and_value[0];
										$sem_name=$key_and_value[1];
										
										echo '<option value="'.$sem_id.'" >'.$sem_name.''."\n";
										$i++;
									}
							?>	
							</select>
						</td>
					</tr>
					<tr id="tr_motion_date">
							<td class="CaptionTD">Дисциплина</td>
							<td class="DataTD">
								<select name="disciplines" id="disciplines">
								<?php
									$i=1;
									$disciplines_op=explode(';',$disciplines_op_for_table);
									foreach($disciplines_op as $disc)
										{
											$key_and_value=explode(":",$disc);
											$disc_id=$key_and_value[0];
											$disc_name=$key_and_value[1];
											
											echo '<option value="'.$disc_id.'" >'.$disc_name.''."\n";
											$i++;
									}
								?>	
								</select>
							</td>
					</tr>
					<tr id="tr_motion_date">
							<td class="CaptionTD">Группа</td>
							<td class="DataTD">
								<select name="groups" id="groups">
								<?php
									$i=1;
									$groups_op=explode(';',$groups_op);
									foreach($groups_op as $group)
										{
											$key_and_value=explode(":",$group);
											$group_id=$key_and_value[0];
											$group_literal=$key_and_value[1];
											
											echo '<option value="'.$group_id.'" >'.$group_literal.''."\n";
											$i++;
									}
								?>	
								</select>
							</td>
					</tr>
				</tbody>
			</table>
</div>

<script language="javascript">
var lastsel_Master;
var lastsel_Detail;
var MasterRecord;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/JQ_Grid_Server_Refs_Show.php?table=<?php echo $MasterTable; ?>',
	width:800,
	height:300,
	datatype: "json",
   	colNames:[<?php echo $colNamesMasterTable; ?>],
   	colModel:[
   		<?php echo $colModelMasterTable; ?>	
   	],
   	rowNum:10,
   	rowList:[10,20,30],
   	pager: '#prowed3',
   	sortname: <?php echo $sortNameMasterTable; ?>,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id == null) 
			{ 
			id=0; 
			if(jQuery("#rowed3_d").jqGrid('getGridParam','records')>0 ) 
				{ 
				jQuery("#rowed3_d").jqGrid('setGridParam',{url:"JS_Grid_server/Server_Nagr_Show.php?q=1&id="+id,page:1}); 
				//jQuery("#rowed3_d").jqGrid('setCaption',"Преподаватель:").trigger('reloadGrid'); 
				jQuery("#rowed3_d").trigger('reloadGrid'); 
				} 
			}
		else 
			{ 
			jQuery("#rowed3_d").jqGrid('setGridParam',{url:"JS_Grid_server/Server_Nagr_Show.php?q=1&id="+id,page:1}); 
			//jQuery("#rowed3_d").jqGrid('setCaption',"Преподаватель: ") .trigger('reloadGrid'); 
			jQuery("#rowed3_d").trigger('reloadGrid'); 
			}
		if(id && id!==lastsel_Master){
			lastsel_Master=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=<?php echo $MasterTable; ?>",
	caption: "Преподаватели"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

jQuery("#rowed3_d").jqGrid({
   	url:'JS_Grid_server/Server_Nagr_Show.php?q=1&id=0',
	width:800,
	height:200,
	datatype: "json",
   	colNames:[<?php echo $colNamesDetail_Table; ?>],
   	colModel:[
   		<?php echo $colModelDetail_Table; ?>	
   	],
   	rowNum:10,
	
   	rowList:[10,20,30],
   	pager: '#prowed3_d',
   	sortname: <?php echo $sortNameDetail_Table; ?>,
	viewrecords: true,
	sortorder: "desc",
	gridComplete: function(){
		var ids = jQuery("#rowed3_d").jqGrid('getDataIDs');
		for(var i=0;i < ids.length;i++){
			var cl = ids[i];
			be = "<input style='height:22px;width:40px;' type='button' value='Ред' onclick=\"jQuery('#rowed3_d').editRow('"+cl+"');  \" />"; 
			se = "<input style='height:22px;width:40px;' type='button' value='Сохр' onclick=\"jQuery('#rowed3_d').saveRow('"+cl+"');\"  />"; 
			ce = "<input style='height:22px;width:40px;' type='button' value='Отм' onclick=\"jQuery('#rowed3_d').restoreRow('"+cl+"');     \" />"; 
			jQuery("#rowed3_d").jqGrid('setRowData',ids[i],{act:be+se+ce});
			
		}	
	},
	onSelectRow: function(id){
		if(id && id!==lastsel_Detail){
			//MasterRecord=jQuery("#rowed3").jqGrid('getGridParam','selrow');
			jQuery('#rowed3_d').jqGrid('restoreRow',lastsel_Detail);
			jQuery('#rowed3_d').jqGrid('editRow',id,
				{
					keys:true,
					url:'JS_Grid_server/Server_Nagr_Edit.php?table=<?php echo $Detail_Table; ?>&prepod_id='+lastsel_Master
				}
			);
			lastsel_Detail=id;
			
		}
	},
	editurl: 'JS_Grid_server/Server_Nagr_Edit.php?table=<?php echo $Detail_Table; ?>',
	caption: "Строки нагрузки"
});
jQuery("#rowed3_d").jqGrid('navGrid',"#prowed3_d",{edit:false,add:false,del:true}).navButtonAdd("#prowed3_d",{caption:"Добавить новую запись",buttonicon:"ui-icon-add",onClickButton:function(){$("div#addrowed3_d").dialog("open");},position:"first"});

function save_nagr(){
	MasterRecord=jQuery("#rowed3").jqGrid('getGridParam','selrow');
	if (MasterRecord==undefined)
	{
		alert('Выберите преподавателя');
		return;
	}
	sem=$("#semesters").val();
	disc=$("#disciplines").val();
	group=$("#groups").val();
	$.ajax({
		url:'JS_Grid_server/Server_RUP_Edit.php',
		dataType:"html",
		data:{
			table:'<?php echo $Detail_Table; ?>',
			semestr:sem,
			discipline_id:disc,
			group_id:group,
			prepod_id:MasterRecord,
			oper:'add'
		}
	});
	jQuery("#rowed3_d").trigger('reloadGrid');
}

$(document).ready(function()
	{
		$("div#addrowed3_d").dialog({
		  autoOpen: false, // Открывать ли окно сразу
		  closeOnEscape: true, // Закрывать ли при нажатии Esc
		  title: "Добавить строку нагрузки", // Заголовок
		  position: ["center", 50], // Местоположение окна
		  width: 300, // Ширина окна
		  height: "auto", // Высота окна
		  draggable: true, // Перемещение
		  resizable: false, // Изменение размера
		  modal: true, // Модальное окно или нет
		  show: null, // Эффект при открытии окна
		  hide: null, // Эффект при закрытии окна
		  buttons: { // Описание кнопок
			 "Сохранить": save_nagr,
			 "Закрыть": function() {
				$(this).dialog("close"); // Закрыть окно
				}
				}
		});
	
	}
)

</script>