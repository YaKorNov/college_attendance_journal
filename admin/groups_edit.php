<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<div class="admin_header_text">Учебные группы</div>
<br/>
<form name="myform" method="POST" action="index.php?action=groups_edit">
		<span><b>Отбор по специальности</b></span>
		<select name="spec_select" onchange="document.forms[0].submit();">'
	
			<?php
			
			$table="specs";
			$SQL = "SELECT spec_id, CONCAT(kod_spec,'/',title) c_title FROM $table";
			$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
			
			$spec_array=array();
			while($r=mysql_fetch_assoc($result)){
				$spec_array[]=array('spec_id'=>$r['spec_id'],'title'=>$r['c_title']);
			}
			$spec_array[]=array('spec_id'=>'0','title'=>'Нет отбора');
			
			foreach($spec_array as $spec){
				if (isset($spec_select))
					{
						if ($spec_select==$spec{'spec_id'}) $selected='selected'; else $selected='';
					}
					else 
					{
						if ($spec{'spec_id'}=="0") $selected='selected'; else $selected=''; 
						$spec_select="0";
					}
				
					
					echo '<option '.$selected.' value="'.$spec{'spec_id'}.'" >'.$spec{'title'}.''."\n";
					$i++;	
			}	
			?>	
		</select>
</form>
<br>

<table id="rowed3"></table>
<div id="prowed3"></div>
<br />


<?php

$colModel=$sortName=$colNames=''; 
$table="groups";

array_pop($spec_array);
$strings_spec=array();
foreach($spec_array as $spec){
	$strings_spec[]=$spec['spec_id'].':'.$spec['title'];	
}	
$edit_options=implode(';',$strings_spec); 
			
$colNames="'id','Литера','Специальность'";
$colModel="
{name:'group_id',index:'group_id', width:90, editable:false},
{name:'literal',index:'literal', width:90, editable:true},
{name:'spec_id',index:'spec_id', width:90, editable:true, edittype:'select',editoptions:{value:'$edit_options'}}";
$sortName="'group_id'";
	
	
echo <<<HTML
<script language="javascript">
var lastsel;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/Server_Groups_Show.php?table=$table&spec_select=$spec_select',
	width:800,
	height:400,
	datatype: "json",
   	colNames:[$colNames],
   	colModel:[
   		$colModel	
   	],
   	rowNum:10,
   	rowList:[10,20,30],
   	pager: '#prowed3',
   	sortname: $sortName,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id && id!==lastsel){
			jQuery('#rowed3').jqGrid('restoreRow',lastsel);
			jQuery('#rowed3').jqGrid('editRow',id,true);
			lastsel=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=$table",
	caption: "Группы"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

</script>
HTML;

?>