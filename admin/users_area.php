<?php  if ( ! defined('SELF')) exit('No direct script access allowed');

$Table="users";
$colNames="'id','Логин','Пароль','Роль'";
$colModel="
{name:'user_id',index:'user_id', width:90, editable:false},
{name:'login',index:'login', width:90, editable:true},
{name:'pass',index:'pass', width:90, editable:true},
{name:'user_role',index:'user_role', width:90, editable:true, edittype:'select', editoptions:{value:'SA:суперадминистратор;A:администратор;ZavUch:завуч'}}";
$sortName="'user_id'";
?>	
<br/>
<div class="admin_header_text">Администраторы системы</div>
<br/>
<table id="rowed3"></table>
<div id="prowed3"></div>
<br />

<?php

	
echo <<<HTML
<script language="javascript">
var lastsel;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/JQ_Grid_Server_Show_Users.php',
	width:800,
	height:400,
	datatype: "json",
   	colNames:[$colNames],
   	colModel:[
   		$colModel	
   	],
   	rowNum:10,
   	rowList:[10,30,50],
   	pager: '#prowed3',
   	sortname: $sortName,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id && id!==lastsel){
			jQuery('#rowed3').jqGrid('restoreRow',lastsel);
			jQuery('#rowed3').jqGrid('editRow',id,
			{
			keys:true
			}
			);
			lastsel=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=$Table",
	caption: "Администраторы"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

</script>
HTML;


?>