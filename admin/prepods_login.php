<?php  if ( ! defined('SELF')) exit('No direct script access allowed');

		$Table="prepods";
		$colNames="'id','Фамилия','Имя','Отчество','Логин','Пароль'";
		$colModel="
		{name:'prepod_id',index:'prepod_id', width:90, editable:false},
		{name:'surname',index:'surname', width:90, editable:true},
		{name:'name',index:'name', width:90, editable:true},
		{name:'patronymic',index:'patronymic', width:90, editable:true},
		{name:'login',index:'login', width:90, editable:true},
		{name:'pass',index:'pass', width:90, editable:true}";
		$sortName="'prepod_id'";
?>	
<br/>
<div class="admin_header_text">Преподаватели колледжа. Логины и пароли</div>
<br/>
<table id="rowed3"></table>
<div id="prowed3"></div>
<br />

<?php

	
echo <<<HTML
<script language="javascript">
var lastsel;
jQuery("#rowed3").jqGrid({
   	url:"JS_Grid_server/JQ_Grid_Server_Refs_Show.php?table=$Table",
	width:800,
	height:400,
	datatype: "json",
   	colNames:[$colNames],
   	colModel:[
   		$colModel	
   	],
   	rowNum:10,
   	rowList:[10,30,50],
   	pager: '#prowed3',
   	sortname: $sortName,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id && id!==lastsel){
			jQuery('#rowed3').jqGrid('restoreRow',lastsel);
			jQuery('#rowed3').jqGrid('editRow',id,
			{
			keys:true
			}
			);
			lastsel=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=$Table",
	caption: "Преподаватели"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

</script>
HTML;


?>