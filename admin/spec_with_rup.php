<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
Cпециальности
<table id="rowed3"></table>
<div id="prowed3"></div>
<br />
Учебный план
<table id="rowed3_d"></table>
<div id="prowed3_d"></div>
<br />


<div class="ui-widget ui-widget-content ui-corner-all ui-jqdialog jqmID1" id="addrowed3_d" style="width: 300px; height: auto; z-index: 950; overflow: hidden; display:none;" tabindex="-1">
	<div class="ui-jqdialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" id="edithdrowed3_d" style="cursor: move;">
		<span class="ui-jqdialog-title" style="float: left;">Добавить запись</span>
	</div>
	<div class="ui-jqdialog-content ui-widget-content" id="editcntrowed3">
		<div  id="FrmGrid_rowed3_d" class="FormGrid" style="width:auto;overflow:auto;position:relative;height:auto;">
			<table id="TblGrid_rowed3_d" class="EditTable" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr rowpos="1" class="FormData" id="tr_group">
						<td class="CaptionTD">Семестр</td>
						<td class="DataTD">
							<select name="semesters" id="semesters" class="FormElement ui-widget-content ui-corner-all">
							<?php
							$sem_op="1:1 семестр;2:2 семестр";
							
							$i=1;
							$sem_op=explode(';',$sem_op);
							foreach($sem_op as $sem)
								{
									$key_and_value=explode(":",$sem);
									$sem_id=$key_and_value[0];
									$sem_name=$key_and_value[1];
									
									echo '<option value="'.$sem_id.'" >'.$sem_name.''."\n";
									$i++;
								}
				
							?>	
							</select>
						</td>
						</tr>
						<tr rowpos="2" class="FormData" id="tr_motion_date">
							<td class="CaptionTD">Дисциплины</td>
							<td class="DataTD">
								<select name="disciplines" id="disciplines" class="FormElement ui-widget-content ui-corner-all">
								<?php
			
								$MasterTable="specs";
								$colNamesMasterTable="'id','Код специальности','Наименование специальности'";
								$colModelMasterTable="
								{name:'spec_id',index:'spec_id', width:90, editable:false},
								{name:'kod_spec',index:'kod_spec', width:90, editable:true},
								{name:'title',index:'title', width:90, editable:true}";
								$sortNameMasterTable="'spec_id'";
				
								$Detail_Table="rup";
								$sem_op="1:1 семестр;2:2 семестр";
								
								$SQL = "SELECT discipline_id, title FROM disciplines";
								$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
								$disciplines_op=array();
								while($r=mysql_fetch_assoc($result)){
									$disciplines_op[]=$r['discipline_id'].":".$r['title'];	
								}	
								$disciplines_op=implode(';',$disciplines_op);
						
								$colNamesDetail_Table="'Дисциплина','Семестр'";
								$colModelDetail_Table="
								{name:'discipline_id',index:'discipline_id', width:90, editable:true, edittype:'select',editoptions:{value:'$disciplines_op'}},
								{name:'semestr',index:'semestr', width:90, editable:true, edittype:'select',editoptions:{value:'$sem_op'}}";
								$sortNameDetail_Table="'RUP_id'";
										
								
								$i=1;
								$disciplines_op=explode(';',$disciplines_op);
								foreach($disciplines_op as $disc)
									{
										$key_and_value=explode(":",$disc);
										$disc_id=$key_and_value[0];
										$disc_name=$key_and_value[1];
										
										echo '<option value="'.$disc_id.'" >'.$disc_name.''."\n";
										$i++;
								}
				
								?>	
								<select>
							</td>
						</tr>
				</tbody>
			</table>
		</div>
		<table border="0" cellspacing="0" cellpadding="0" class="EditTable" id="TblGrid_rowed3_d">
			<tbody>
				<tr>
					<td colspan="2"><hr class="ui-widget-content" style="margin:1px"></td>
				</tr>
				<tr id="Act_Buttons">
					<td class="EditButton">
						<a href="javascript:void(0)" id="addrowed3_d_sData" class="fm-button ui-state-default ui-corner-all fm-button-icon-left">Сохранить<span class="ui-icon ui-icon-disk"></span></a>
						<a href="javascript:void(0)" id="addrowed3_d_cData" class="fm-button ui-state-default ui-corner-all fm-button-icon-left">Отмена<span class="ui-icon ui-icon-close"></span></a>
				</td>
				</tr>
			</tbody>
		</table>
	</div>
<div class="jqResize ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se"></div>
</div>

<script language="javascript">
var lastsel_Master;
var lastsel_Detail;
var MasterRecord;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/JQ_Grid_Server_Refs_Show.php?table=<?php echo $MasterTable; ?>',
	width:600,
	datatype: "json",
   	colNames:[<?php echo $colNamesMasterTable; ?>],
   	colModel:[
   		<?php echo $colModelMasterTable; ?>	
   	],
   	rowNum:10,
   	rowList:[10,20,30],
   	pager: '#prowed3',
   	sortname: <?php echo $sortNameMasterTable; ?>,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id == null) 
			{ 
			id=0; 
			if(jQuery("#rowed3_d").jqGrid('getGridParam','records')>0 ) 
				{ 
				jQuery("#rowed3_d").jqGrid('setGridParam',{url:"JS_Grid_server/Server_RUP_Show.php?q=1&id="+id,page:1}); 
				jQuery("#rowed3_d").jqGrid('setCaption',"Специальность: ").trigger('reloadGrid'); 
				} 
			}
		else 
			{ 
			jQuery("#rowed3_d").jqGrid('setGridParam',{url:"JS_Grid_server/Server_RUP_Show.php?q=1&id="+id,page:1}); 
			jQuery("#rowed3_d").jqGrid('setCaption',"Студент") .trigger('reloadGrid'); 
			}
		if(id && id!==lastsel_Master){
			lastsel_Master=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=<?php echo $MasterTable; ?>",
	caption: "Специальности"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

jQuery("#rowed3_d").jqGrid({
   	url:'JS_Grid_server/Server_RUP_Show.php?q=1&id=0',
	width:600,
	datatype: "json",
   	colNames:[<?php echo $colNamesDetail_Table; ?>],
   	colModel:[
   		<?php echo $colModelDetail_Table; ?>	
   	],
   	rowNum:10,
	
   	rowList:[10,20,30],
   	pager: '#prowed3_d',
   	sortname: <?php echo $sortNameDetail_Table; ?>,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id && id!==lastsel_Detail){
			//MasterRecord=jQuery("#rowed3").jqGrid('getGridParam','selrow');
			jQuery('#rowed3_d').jqGrid('restoreRow',lastsel_Detail);
			jQuery('#rowed3_d').jqGrid('editRow',id,
				{
					keys:true,
					url:'JS_Grid_server/Server_RUP_Edit.php?table=<?php echo $Detail_Table; ?>&spec_id='+lastsel_Master
				}
			);
			lastsel_Detail=id;
			
		}
	},
	editurl: 'JS_Grid_server/Server_RUP_Edit.php?table=<?php echo $Detail_Table; ?>',
	caption: "Справочники"
});
jQuery("#rowed3_d").jqGrid('navGrid',"#prowed3_d",{edit:false,add:false,del:true});



$(document).ready(function()
	{
		$("#add_motion").click(
		function(){
			$("#addrowed3_d").show();
			$("#addrowed3_d").dialog();
		});
		
		
		$("#addrowed3_d_sData").click(
		function(){
			MasterRecord=jQuery("#rowed3").jqGrid('getGridParam','selrow');
			if (MasterRecord==undefined)
			{
				alert('Выберите специальность');
				return;
			}
			sem=$("#semesters").val();
			disc=$("#disciplines").val();
			$.ajax({
				url:'JS_Grid_server/Server_RUP_Edit.php',
				dataType:"html",
				data:{
					table:'<?php echo $Detail_Table; ?>',
					semestr:sem,
					discipline_id:disc,
					spec_id:MasterRecord,
					oper:'add'
				}
			});
			jQuery("#rowed3_d").trigger('reloadGrid');
		});
		
		$("#addrowed3_d_cData").click(
		function(){
			$("#addrowed3_d").hide();
		});
	}
)

</script>

<button id="add_motion">Добавить строку РУП</button>