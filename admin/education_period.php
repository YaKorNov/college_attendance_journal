<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<div class="admin_header_text">Учебные периоды</div>
<br/>
<table id="rowed3"></table>
<div id="prowed3"></div>
<br />
<br />

<?php
$table='education_periods';
$colModel=$sortName=$colNames=''; 
$i=1;

$SQL = "describe $table";
$result = mysql_query( $SQL ) or die("Couldn t execute query.".mysql_error());
$rows_count=mysql_num_rows($result);

// $month_array=array('1'=>'Январь','2'=>'Февраль','3'=>'Март','4'=>'Апрель','5'=>'Май','6'=>'Июнь','7'=>'Июль','8'=>'Август','9'=>'Сентябрь','10'=>'Октябрь','11'=>'Ноябрь','12'=>'Декабрь');
			
			// foreach ($month_array as $number=>$month_name){
					// $month_op[]=$number.":".$month_name;	
			// }		
			// $month_options=implode(';',$month_op);

while($field=mysql_fetch_assoc($result)){
			$colNames.=field_synonym($field['Field'],$table);
			if ($field['Key']=='PRI')
				{
				  $editable='false'; $sortName="'".$field['Field']."'";
				}
			else
				{
				  $editable='true'; 
				}
			
			// if ($field['Field']=='start_month')
				// {
					// $colStr="{name:'".$field['Field']."',index:'".$field['Field']."', width:90, edittype:'select', editable:$editable, editoptions:{value:'".$month_options."'}}";
				// }
			// elseif ($field['Field']=='end_month')
				// {
					// $colStr="{name:'".$field['Field']."',index:'".$field['Field']."', width:90, edittype:'select', editable:$editable, editoptions:{value:'".$month_options."'}}";
				// }
			if ($field['Field']=='semestr_number')
				{
					$colStr="{name:'".$field['Field']."',index:'".$field['Field']."', width:90, edittype:'select', editable:$editable, editoptions:{value:'1:1 семестр;2:2 семестр'}}";
				}
			else
				{	
					$colStr="{name:'".$field['Field']."',index:'".$field['Field']."', width:90, edittype:'text', editable:$editable}";		
				}
			
							
			
			$colModel.=$colStr;
			
			if ($i!=$rows_count) 
			{
			$colNames.=',';
			$colModel.=',';
			}
			$i++;
	}


	
	
echo <<<HTML
<script language="javascript">
var lastsel;
jQuery("#rowed3").jqGrid({
   	url:'JS_Grid_server/JQ_Grid_Server_Edu_Period_Show.php?table=$table',
	width:800,
	datatype: "json",
   	colNames:[$colNames],
   	colModel:[
   		$colModel	
   	],
   	rowNum:10,
   	rowList:[10,20,30],
   	pager: '#prowed3',
   	sortname: $sortName,
	viewrecords: true,
	sortorder: "desc",
	onSelectRow: function(id){
		if(id && id!==lastsel){
			jQuery('#rowed3').jqGrid('restoreRow',lastsel);
			jQuery('#rowed3').jqGrid('editRow',id,
			{
			keys:true
			}
			);
			lastsel=id;
		}
	},
	editurl: "JS_Grid_server/JQ_Grid_Server_Refs_Edit.php?table=$table",
	caption: "Учебные периоды"
});
jQuery("#rowed3").jqGrid('navGrid',"#prowed3",{edit:true,add:true,del:true});

</script>
HTML;


function field_synonym($field,$table)
{
	
	$synonym_array=array('education_periods'=>array('education_period_id'=>'id','semestr_number'=>'Номер семестра','year'=>'Год','start_month'=>'Месяц начала семестра','end_month'=>'Месяц конца семестра'));
	
	
		if (isset($synonym_array[$table][$field]))
		{
			return "'".$synonym_array[$table][$field]."'";
		}		
		else
		{
			return "";
		}
	
	
}

?>