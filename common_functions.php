<?

function get_rus_names_for_month($month_str) //находит в строке подстроку с англоязычеым названеим месяцв и заменяет
{

	$assoc_array=array('September'=>'Сентябрь','October'=>'Октябрь','November'=>'Ноябрь','December'=>'Декабрь','January'=>'Январь','February'=>'Февраль','March'=>'Март','April'=>'Апрель','May'=>'Май');	
	
	$month_str=str_replace(array_keys($assoc_array),array_values($assoc_array),$month_str);
	
	return $month_str;
}

function get_rus_names_for_attest_label($att_string)
{

	$assoc_array=array('2'=>'2','3'=>'3','4'=>'4','5'=>'5','att'=>'а','na'=>'н/а','zach'=>'зач','nzach'=>'н/з');	
	return $assoc_array[$att_string];
}

function get_attest_data($attest_info)
{
	
	switch($attest_info)
	{
		case 'аtt': $data_attest='{"type":"att","grade":"0"}';
			break;
		case 'na': $data_attest='{"type":"na","grade":"0"}';
			break;
		case 'zach': $data_attest='{"type":"zach","grade":"0"}';
			break;
		case 'nzach': $data_attest='{"type":"nzach","grade":"0"}';
			break;
		case '2': $data_attest='{"type":"grade","grade":"2"}';
			break;
		case '3': $data_attest='{"type":"grade","grade":"3"}';
			break;
		case '4': $data_attest='{"type":"grade","grade":"4"}';
			break;
		case '5': $data_attest='{"type":"grade","grade":"5"}';
			break;
		default: $data_attest='{"type":"0","grade":"0"}';
			break;
	}
	
	return $data_attest;
	
}

function sort_students_by_the_family($common_student_list)
{
	$sortable_st_array=array();
	foreach ($common_student_list as $st_id=>$st_full_name)
			{
				$st_FIO=explode(' ',$st_full_name);
				$sortable_st_array[$st_id]=$st_FIO[0];
			}	
	asort($sortable_st_array);
	
	
	foreach ($sortable_st_array as $st_id=>$st_fam)
	{
		$sortable_st_array[$st_id]=$common_student_list[$st_id];
	}
	return $sortable_st_array;
}

function get_common_student_list_by_crossing($common_student_list,$subgroup_list)
{
	global $common_student_list,$subgroup_list;
	$buffer_array=array();
	foreach ($subgroup_list as $st_id=>$st_FIO)
	{
		$buffer_array["st_".$st_id]=$st_FIO;
	}
	$subgroup_list=$buffer_array;

	//выделим студентов, которые действительно есть в группе на этот момент
	$subgroup_list=sort_students_by_the_family(array_intersect_key($common_student_list, $subgroup_list));
	$non_subgroup_list=sort_students_by_the_family(array_diff_key($common_student_list, $subgroup_list));
	$common_student_list=array_merge($subgroup_list,$non_subgroup_list);
	
	return 1;
}

function create_journal_tables_for_zero_lessons()
{
	global $common_student_list,$subgroup_list,$subgroup,$table,$table_description;
	
	$table.="<table id='grades_table'>";
	$table.='<tr class="table_grades_header"><td>Студенты/Даты занятий</td></tr>';
	$student_number=1;
	foreach ($common_student_list as $student_id=>$student)
		{
			
			//смотрим, входит ли студент в подгруппу и выставляем ему класс
			if ($subgroup!='group')
			{
				if (isset($subgroup_list[$student_id]))
				{
					$student_ids=array_keys($subgroup_list);
					//оформление подчеркиванием последней строка
					$class_border_td="";
					if (array_search($student_id,$student_ids)==(count($student_ids)-1))
						$class_border_td="last_subgroup_st";
					$table.="<tr><td class='student_column rows subgroup_st $class_border_td' id='$student_id'>".$student_number." ".$student."<span class='star_mark'>*</span></td></tr>";
				}
				else
					$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td></tr>";
			}
			else
				$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td></tr>";
			
			$student_number++;
		} 
	$table.='</table>';
	
	$table_description="<table id='description_table'>";
	$table_description.="<tr class='table_description_header'><td>Дата занятия</td><td>Тема занятия</td><td>Задание для СР</td><td>Тип занятия</td></tr>";
	$table_description.="</table>";
}

//функции для работы с сессиями

function checkLoggedIn(){
    if(!isset($_SESSION["loggedIn"]))
		{	
			if ($_SERVER['PHP_SELF']!='/login.php')
			header("Location: /login.php");
		}
	elseif (isset($_SESSION["loggedIn"]) && $_SESSION["loggedIn"] === true)	
		{
			user_router();
		}
	return true;
} 

function user_login_router()
{
	switch($_SESSION['user_role']){
     case "SA":
	     $requested_path='/admin/index.php';
		 break;
     case "A":
         $requested_path='/admin/index.php';
		 break;
     case "ZavUch": //для завучей пока нет подсистемы
         $requested_path='/zavuch.php';
		 break;
	 case "prepod":
         $requested_path='/teachers_area/index.php';
		 break;	 
   }
   
   if ($_SERVER['PHP_SELF']!=$requested_path)
   header("Location: $requested_path");
   
   return true;
}

function user_router()
{
	$granted_pages=array();
	$granted_pages=array_keys($_SESSION['granted_pages']);
  
   
   // if ($_SERVER['PHP_SELF']=='/login.php')
	// {
		// header("Location: $granted_pages[0]");
	// }
	//подумал, пока нет смысла делать какие-то доп сообщения при переходе чела на запрещенную страницу, пусть и для логина и для него будет просто редирект
   if (!in_array($_SERVER['PHP_SELF'],$granted_pages)) //если страница, которая не разрешена по уровню доступа, тогда делаем редирект на дефолтную первую разрешенную
		header("Location: $granted_pages[0]");
   
   return true;
}


function createUserSession($user_params) {
	foreach ($user_params as $key=>$val)
	{
		 $_SESSION[$key]=$val;
		 if ($key=='user_role')
		 {
			switch($val){
			 case "SA":
				 $granted_pages=array('/admin/index.php'=>'Административный раздел','/teachers_area/index.php'=>'Работа с журналом','/logout.php'=>'Выйти');
				 break;
			 case "A":
				 $granted_pages=array('/admin/index.php'=>'Административный раздел','/teachers_area/index.php'=>'Работа с журналом','/logout.php'=>'Выйти');
				 break;
			 case "ZavUch": //для завучей пока нет подсистемы
				 $granted_pages=array('/zavuch.php'=>'Подсистема зав. отделением','/logout.php'=>'Выйти');
				 break;
			 case "prepod":
				 $granted_pages=array('/teachers_area/index.php'=>'Работа с журналом','/logout.php'=>'Выйти');
				 break;	 
			}
			$_SESSION['granted_pages']=$granted_pages;
		 }
	}
	$_SESSION["loggedIn"]=true;
	return true;
} 


function destroyUserSession() {
	foreach ($_SESSION as $key=>$val)
	{
		 unset($_SESSION[$key]);
	}
	session_destroy();
	return true;
} 

function User_Menu_By_The_Rights(){
	$current_page=$_SERVER['PHP_SELF'];
	$granted_pages=$_SESSION['granted_pages'];
	$menu_items='';
	foreach ($granted_pages as $page_addr=>$page_name)
	{
		if ($current_page==$page_addr) continue;
		$menu_items.="<div><a href='$page_addr'>$page_name</a></div>";
	}
	
	if ($_SESSION['user_role']=='prepod')
		$User_Nik=$_SESSION['FIO'];
	else
		$User_Nik=$_SESSION['login'];
		
	return "<div class='User_Meeting'>Здравствуйте, $User_Nik</div>
			<div class='User_Menu'>
				".$menu_items."
				<div class='clearfix'></div>
			</div>
		    ";
	
}

//выделим пока в отдельную функцию, если будет необходимость, в дальнейшем будем разделять права по страницам
function Admin_SubMenu_By_The_Rights(){
	switch($_SESSION['user_role']){
			 case "SA":
				return true;
			 case "A":
				 return false;
			 case "ZavUch":
				 return false;
			 case "prepod":
				 return false;	 
			}
}

//функции для работы с сессиями

function field_validator($field_descr, $field_data, $field_type, $min_length="", $max_length="", $field_required=1) {

	$message='';
	
   if(!$field_data && !$field_required){ return; }

   $field_ok=false;

   $email_regexp="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|";
   $email_regexp.="(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

   $data_types=array(
     "email"=>$email_regexp,
     "digit"=>"^[0-9]$",
     "number"=>"^[0-9]+$",
     "alpha"=>"^[a-zA-Z]+$",
     "alpha_space"=>"^[a-zA-Z ]+$",
     "alphanumeric"=>"^[a-zA-Z0-9]+$",
     "alphanumeric_space"=>"^[a-zA-Z0-9 ]+$",
     "string"=>""
   );

   if ($field_required && empty($field_data)) {
     $message = "Поле $field_descr обязательно для заполнения";
     return $message;
   }

   // if ($field_type == "string") {
     // $field_ok = true;
   // } else {
     // $field_ok = ereg($data_types[$field_type], $field_data);
   // }

   // if (!$field_ok) {
     // $message = "Пожалуйста введите корректный $field_descr";
     // return $message;
   // }

   if ($field_ok && ($min_length > 0)) {
     if (strlen($field_data) < $min_length) {
       $message = "$field_descr должен быть не короче $min_length символов.";
       return $message;
     }
   }

   if ($field_ok && ($max_length > 0)) {
     if (strlen($field_data) > $max_length) {
       $message = "$field_descr не должен быть длиннее $max_length символов.";
       return $message;
     }
   }
}
	
?>