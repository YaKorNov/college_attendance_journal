
$(document).ready(function() {
  
	
	$(".testSlider").hrzAccordion({containerClass     : "containerSlider",
			listItemClass      		: "listItemSlider",					
			containerClass     		: "containerSlider",
			contentContainerClass  	: "contentContainerSlider",
			contentWrapper     		: "contentWrapperSlider",
			contentInnerWrapper		: "contentInnerWrapperSlider",
			handleClass        		: "handleSlider",
			handleClassOver    		: "handleOverSlider",
			handleClassSelected		: "handleSelectedSlider",
			handlePosition     		: "left",
			closeEaseAction    		: "easeInQuart",
			closeSpeed     			: 500,
			openEaseAction     		: "easeInQuart",
			openSpeed      			: 500,
			openOnLoad		   		: 1,
							  });

	
 });