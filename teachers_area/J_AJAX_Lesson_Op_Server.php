<?
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
header('Content-Type: text/html; charset=utf-8');

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) 
// Здесь обрабатываем данные, если отключен JavaScript
exit('Don\'t sent by AJAX');


	require_once("../bd_config.php");
	require_once("../bd_functions.php");
	require_once("../common_functions.php");
	foreach ($_POST as $key=>$val)
	{
		if (isset($_POST[$key]))
		$post[$key]=$_POST[$key];
		else $post[$key]='';
	}
	if (isset($_POST['disc_and_prepod'])) $post['disc_and_prepod'] = $_POST['disc_and_prepod'];
	else $post['disc_and_prepod'] = array();
	if (isset($_POST['grade_arr'])) $post['grade_arr'] = $_POST['grade_arr'];
	else $post['grade_arr'] = array();

	$conn = mysql_connect($dbhost, $dbuser, $dbpassword) or die("Connection Error: " . mysql_error());
	mysql_select_db($db) or die("Error conecting to db.");
	mysql_query("SET NAMES 'utf8'");

	if (($post['status']=="save_lesson"))
		{
			if ($post['lesson_id'])
			 {
				$execution_result=edit_lesson($post,"lesson");
			 }
			 else
			 {
				$execution_result=add_new_lesson($post,"lesson");
			 }
			
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}
	elseif (($post['status']=="save_attest"))
		{
			if ($post['lesson_id'])
			 {
				$execution_result=edit_lesson($post,"attest");
			 }
			 else
			 {
				$execution_result=add_new_lesson($post,"attest");
			 }
			
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}	
	elseif (($post['status']=="del_lesson"))
		{
			
			$execution_result=delete_lesson($post);
			 
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}
	elseif (($post['status']=="edit_grades"))
		{
			$post['student']=str_replace("st_","",$post['student']);
			$execution_result=edit_grade_and_attend($post);
			
			 
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}
	elseif (($post['status']=="edit_attest"))
		{
			$post['student']=str_replace("st_","",$post['student']);
			$execution_result=edit_attest($post);
			
			 
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}
	elseif (($post['status']=="update_final_grades"))
		{
			$post['student']=str_replace("st_","",$post['student']);
			$execution_result=update_final_grades($post);
			 
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}
	elseif (($post['status']=="delete_final_grades"))
		{
			$post['student']=str_replace("st_","",$post['student']);
			$execution_result=delete_final_grades($post);
			 
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}
	elseif (($post['status']=="save_subgroup_list"))
		{
			//$st_arr=array();
			// foreach ($post['student_array'] as $st_id)
				// {
					// $st_arr[]=str_replace("st_","",$st_id);
				// }
			// $post['student_array']=$st_arr;	
			$execution_result=save_subgroup_student_list($post);
			 
			 if ($execution_result)			
				 echo json_encode('Success');
			 else
				 echo json_encode('Fail');
				
		}	


?>