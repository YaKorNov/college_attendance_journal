<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<script src="../js/Journal_Month_List.js" type="text/javascript"></script>
<h2>Текущая успеваемость и посещаемость</h2>
<br>
<?php
			$selected_group=$selected_disc=$selected_month="";
			
			
			$MsgText=check_refs_filling();
			if ($MsgText=='Success')
				{
					$refs_filled=1;
				}
			else
				{
					$refs_filled=0;
					echo $MsgText;
				}			
	
			if ($refs_filled)
				{
?>			
<div id="Toolbar_Panel">
	<table class="Group_UI_Tools">
		<tr>
			<td>
				<span id="label_group"><b>Группа</b></span>
			</td>
			<td>
				<select name="group" id="select_group">
					<?php
						$result_array=get_nagr_strings();
						$i=1;
						foreach($result_array as $d){
							if ($i==1)
								{
									$where=$d['group_id'];
								}

							if ($d['group_id']==$where)
								{
									$selected='selected';
									$selected_group=$d['group_id'];
								}
							else
								{
									$selected='';
								}

							echo '<option class="save" '.$selected.' value="'.$d['group_id'].'" >'.$d['literal'].'</option>'."\n";
							$i++;
						}
					?>
				</select>
			</td>
			<td>
				<span id="label_subgroup"><b>Подгруппа</b></span>
			</td>
			<td>
				<select name="subgroup" id="select_subgroup">
					<option class="save" selected value='group'>Группа полным составом</option>
					<option class="save" value='1'>1</option>
					<option class="save" value='2'>2</option>
					<option class="save" value='3'>3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span id="label_disc"><b>Дисциплина</b></span>
			</td>
			<td colspan="3">
				<select name="disc" id="select_disc">
					<?php
							$result_array=get_disciplines_by_group($selected_group);
							$i=1;
						
							foreach ($result_array as $d){
								if ($i==1)
									{
										$where=$d['discipline_id'];
									}

								if ($d['discipline_id']==$where)
									{
										$selected='selected';
										$selected_disc=$d['discipline_id'];
									}
								else
									{
										$selected='';
									}

								echo '<option class="save" '.$selected.' value="'.$d['discipline_id'].'" >'.$d['title'].'</option>'."\n";
								$i++;
							}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span id="label_disc"><b>Месяц</b></span>
			</td>
			<td colspan="3">
				<select name="month" id="select_month">
					<?php
					
							$result_array=get_month_by_disc($selected_group,$selected_disc);
							$i=1;
							foreach ($result_array as $d)
							{
								if ($i==1)
										{
											$where=$d['month_id'];
										}
									
									if ($d['month_id']==$where)
										{
										$selected='selected';
										$selected_month=$d['month_id'];
										}
									else
										{
										$selected='';
										}
									echo '<option class="save" '.$selected.' value="'.$d['month_id'].'" >'.get_rus_names_for_month($d['title']).'</option>'."\n";
								$i++;
							}			
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<span id="label_prepod"><b>Преподаватель</b></span>
			</td>
			<td colspan="3">
				<select name="prepod" id="select_prepod">
					<?php
						
								$result_array=get_prepod_by_month($selected_group,$selected_disc,$selected_month);
								$i=1;
								foreach ($result_array as $d)
								{
									if ($i==1)
											{
												$where=$d['prepod_id'];
											}
										
										if ($d['prepod_id']==$where)
											{
											$selected='selected';
											$selected_prepod=$d['prepod_id'];
											}
										else
											{
											$selected='';
											}
										echo '<option class="save" '.$selected.' value="'.$d['prepod_id'].'" >'.$d['prep_FIO'].'</option>'."\n";
									$i++;
								}			
						?>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<button id="load_journal">Открыть лист журнала</button>
			</td>
		</tr>
	</table>		
</div>
<div class="print_container"><div class="journal_print" id="journal_print">Распечатать журнал</div></div>


<div id="ajax_status"><div class="loading_progress"><img src="/img/ico-loading.gif"></div><div class="loading_label">Идет загрузка...</div></div>
<div id="Journal_Page" class="Area_IS_Granted"></div>
<div id="Overlay_Access_Denied"></div>
<div id="Lesson_Edit_form" style="display:none;">
	<form>
		<table class="hidden_lines">
			<tr>
				<td><b>Дата занятия</b></td>
				<td><input type="text" value="" name="edit_form_lesson_date" id="edit_form_lesson_date"></input></td>
				<td><b>Тип занятия</b></td>
				<td>
					<select name="select_lesson_type" id="select_lesson_type">
					<?php
					
							$result_array=get_lessons_types();
							$i=1;
							foreach ($result_array as $d)
							{
								if ($i==1)
										{
											$where=$d['id'];
										}
									
									if ($d['id']==$where)
										{
										$selected='selected';						
										}
									else
										{
										$selected='';
										}
									echo '<option class="save" '.$selected.' value="'.$d['id'].'" >'.$d['title'].'</option>'."\n";
								$i++;
							}			
					?>
					</select>
				</td>
			</tr>
		</table>
		<div>
			<div id="lesson_theme_label"><b>Тема занятия</b></div>
			<textarea value="" name="edit_form_theme" id="edit_form_theme" rows="2" cols="78"></textarea>
		</div>
		<div>
			<div id="lesson_SR_label"><b>Задание для СР</b></div>
			<textarea value="" name="edit_form_homework" id="edit_form_homework" rows="4" cols="78"></textarea>
		</div>
		<!--table id="integr_widget" class="Group_UI_Tools">
			<tr>
				<td><b>Интегрированное занятие</b></td>
				<td><input type="checkbox" name="edit_form_integr" id="edit_form_integr"></input></td>
			</tr>
		</table>	
		<table name="edit_form_table" id="edit_form_table" class="hidden_lines" style="display:none;">
			<tr>
				<td><input type="checkbox" name="check_row_table_1" id="check_row_table_1"></input></td>
				<td><b>Смежная дисциплина</b></td>
				<td><select name="edit_form_disc_1" class="edit_form_disc_1" id="edit_form_disc_1"></select></td>
				<td><b>Преподаватель</b></td>
				<td><select name="edit_form_prepod_1" class="edit_form_prepod_1" id="edit_form_prepod_1"></select></td>
			</tr>
			<tr>
				<td><input type="checkbox" name="check_row_table_2" id="check_row_table_2"></input></td>
				<td><b>Смежная дисциплина</b></td>
				<td><select name="edit_form_disc_2" class="edit_form_disc_2" id="edit_form_disc_2"></select></td>
				<td><b>Преподаватель</b></td>
				<td><select name="edit_form_prepod_2" class="edit_form_prepod_2" id="edit_form_prepod_2"></select></td>
			</tr>
		</table-->
	</form>
	<div id="status"></div>
	<div class="lesson_id" id="" style="display:none;"></div>
</div>

<div id="Attest_Edit_form" style="display:none;">
	<form>
		<table class="hidden_lines">
			<tr>
				<td><b>Дата аттестации</b></td>
				<td><input type="text" value="" name="edit_form_attest_date" id="edit_form_attest_date"></input></td>
			</tr>
		</table>
	</form>
	<div id="att_status"></div>
	<div class="att_form_lesson_id" id="att_form_lesson_id" style="display:none;"></div>
</div>

<div id="Grades_Edit_form" style="display:none;">
	<form>
		<table class="hidden_lines">
			<tr>
				<td><b>Пропуск</b></td>
				<td><input type="checkbox" name="is_attend" id="is_attend"></input></td>
				<td><span id="case_label" style="display:none;"><b>Причина</b></span></td>
				<td>
					<select name="attend_case" class="attend_case" id="attend_case" style="display:none;">
					<?php
						$result_array=get_attend_case_list();
						$i=1;
					
						$data_opt_array=array();
					
						foreach ($result_array as $d){
							if ($i==1)
								{
									$where=$d['att_case_id'];
								}

							if ($d['att_case_id']==$where)
								$selected='selected';
							else
								$selected='';
							
						echo '<option class="save" '.$selected.' value="'.$d['att_case_id'].'" >'.$d['full_title'].''."\n";
						
						$data_opt_array[]="'$d[att_case_id]':'$d[title]'";
						
						$i++;
						}
						
						$data_options=implode(',',$data_opt_array);
						$data_options=str_replace('\'','"',$data_options);
						$data_options="data-options={".$data_options."}";
					?>
					</select>
				</td>
			</tr>
		</table>
		<br>
		<div id="add_grade"><b>Добавить оценку(отметку об отработке)</b></div>
		<div id="grade_container"></div>
	</form>
	<div id="grade_status"></div>
	<div id="lesson_student_data" style="display:none;"></div>
	<div id="attendance_select_data" style="display:none;" <? echo $data_options; ?>></div>
</div>

<div id="Attest_Grade_Edit_form" style="display:none;">
	<form>
		<table class="hidden_lines">
			<tr>
				<td><span><b>Отметка об аттестации</b></span></td>
				<td>
					<select name="attest_label_select" class="attest_label_select" id="attest_label_select">
						<option value="att" selected>Аттестован(а)</option>
						<option value="na" >Неаттестован(а)</option>
						<option value="zach" >Зачтено</option>
						<option value="nzach" >Незачтено</option>
						<option value="grade" >Оценка</option>
					</select>
				</td>
				<td><span id="Attest_Edit_form_grade_label" style="display:none;"><b>Оценка</b></span></td>
				<td>
					<select name="attest_grade_select" class="attest_grade_select" id="attest_grade_select" style="display:none;">
						<option value="3" selected>3</option>
						<option value="4" >4</option>
						<option value="5" >5</option>
					</select>
				</td>
			</tr>
		</table>
	</form>
	<div id="attest_grade_status"></div>
	<div id="attest_lesson_student_data" style="display:none;"></div>
</div>
<?php
			}
?>