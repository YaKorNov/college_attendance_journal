<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<script src="../js/Total_Attendance_List.js" type="text/javascript"></script>
<h2>Сведения о количестве занятий, пропущенных обучающимися</h2>
<br>
<?php
			$selected_group=$selected_disc=$selected_semestr="";


			$MsgText=check_refs_filling();
			if ($MsgText=='Success')
				{
					$refs_filled=1;
				}
			else
				{
					$refs_filled=0;
					echo $MsgText;
				}

			if ($refs_filled)
				{
?>
<div id="Toolbar_Panel">
	<table class="Group_UI_Tools">
		<tr>
			<td><span><b>Группа</b></span></td>
			<td>
				<select name="group" id="select_group">
				<?php
					$result_array=get_nagr_strings();
					$i=1;
					foreach($result_array as $d){
						if ($i==1)
							{
								$where=$d['group_id'];
							}

						if ($d['group_id']==$where)
							{
								$selected='selected';
								$selected_group=$d['group_id'];
							}
						else
							{
								$selected='';
							}

						echo '<option class="save" '.$selected.' value="'.$d['group_id'].'" >'.$d['literal'].''."\n";
						$i++;
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td><span><b>Вид периода</b></span></td>
			<td>
				<select name="vid_perioda" id="select_vid_perioda">
					<option class="save"  value='year' selected>За весь учебный год</option>
					<option class="save"  value='semestr' >За один семестр</option>
					<option class="save"  value='month' >За один месяц</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><span id="period_label" style="display:none;"><b>Период</b></span></td>
			<td>
				<select name="period" id="select_period" style="display:none;">
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<button id="load_vedomost">Открыть ведомость</button>
			</td>
		</tr>
	</table>
</div>
<br/>
<div id="ajax_status"><div class="loading_progress"><img src="/img/ico-loading.gif"></div><div class="loading_label">Идет загрузка...</div></div>
<div id="Vedomost_Page"></div>
<?php
			}
?>