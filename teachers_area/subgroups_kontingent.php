<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<script src="../js/Subgroups_kontingent.js" type="text/javascript"></script>
<h2>Формирование списков студентов по подгруппам</h2>
<br>
<?php
			$selected_group="";
			$MsgText=check_refs_filling();
			if ($MsgText=='Success')
				{
					$refs_filled=1;
				}
			else
				{
					$refs_filled=0;
					echo $MsgText;
				}

			if ($refs_filled)
				{
?>
<div id="Toolbar_Panel">
	<table class="Group_UI_Tools">
		<tr>
			<td><span><b>Группа</b></span></td>
			<td>
				<select name="group" id="select_group">
				<?php
					$result_array=get_nagr_strings();
					$i=1;
					foreach($result_array as $d){
						if ($i==1)
							{
								$where=$d['group_id'];
							}

						if ($d['group_id']==$where)
							{
								$selected='selected';
								$selected_group=$d['group_id'];
							}
						else
							{
								$selected='';
							}

						echo '<option class="save" '.$selected.' value="'.$d['group_id'].'" >'.$d['literal'].''."\n";
						$i++;
					}
				?>
				</select>
			</td>
			<td><span><b>Список группы на дату:</b></span></td>
			<td><input type="text" id="list_date"></td>
		</tr>
		<tr>
			<td>
				<span id="label_disc"><b>Дисциплина</b></span>
			</td>
			<td>
				<select name="disc" id="select_disc">
					<?php
							$result_array=get_disciplines_by_group($selected_group);
							$i=1;
						
							foreach ($result_array as $d){
								if ($i==1)
									{
										$where=$d['discipline_id'];
									}

								if ($d['discipline_id']==$where)
									{
										$selected='selected';
										$selected_disc=$d['discipline_id'];
									}
								else
									{
										$selected='';
									}

								echo '<option class="save" '.$selected.' value="'.$d['discipline_id'].'" >'.$d['title'].'</option>'."\n";
								$i++;
							}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td><span><b>Подгруппа</b></span></td>
			<td>
				<select name="subgroup" id="select_subgroup">
					<option class="save"  value='1' selected>1</option>
					<option class="save"  value='2' >2</option>
					<option class="save"  value='3' >3</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<button id="load_lists_subgroups">Открыть списки</button>
			</td>
		</tr>
	</table>
</div>
<br/>
<div id="ajax_status"><div class="loading_progress"><img src="/img/ico-loading.gif"></div><div class="loading_label">Идет загрузка...</div></div>
<div id="Subgroup_Page"></div>
<div id="Overlay_Access_Denied"></div>
<?php
			}
?>