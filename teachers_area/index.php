<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
require_once("../bd_config.php");
require_once("../bd_functions.php");
require_once("../common_functions.php");
checkLoggedIn();
// The name of THIS file - для ограничения доступа на включаемые скрипты
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
$conn = mysql_connect($dbhost, $dbuser, $dbpassword)or die("Connection Error: " . mysql_error());
mysql_select_db($db) or die("Error conecting to db.");
mysql_query("SET NAMES 'utf8'");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Электронный журнал</title>


<link rel="stylesheet" type="text/css" media="screen" href="../css/JQ_Grid/ui-lightness/jquery-ui-1.10.3.custom.css" />
<link rel="stylesheet" type="text/css" media="screen" href="../css/style.css" />
<script src="/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="/js/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/js/jquery.hrzAccordion.js"></script>
<script type="text/javascript" src="/js/jquery.hrzAccordion.examples.js"></script>

<body>
<div class="journal_wrap">
<div class="header_journal_list">
	<div class="header_title"><h1>Журнал группы</h1></div>
	<div class="header_user_block"><?php echo User_Menu_By_The_Rights(); ?></div>
	<div class="clearfix"></div>
</div>

<div class="content-wrap">
	<!--div class="header_menu">
	<div class="menu_item"><a href="index.php?action=subgroups_kontingent">Списки студентов по подгруппам</a></div>
	<div class="menu_item"><a href="index.php?action=journal_month_list">Заполнение текущей успеваемости и посещаемости</a></div>
	<div class="menu_item"><a href="index.php?action=journal_itog_rate">Сводная ведомость по дисциплине за семестр</a></div>
	<div class="menu_item"><a href="index.php?action=total_attendance_list">Сведения о количестве уроков, пропущенных обучающимися</a></div>
	<div class="menu_item"><a href="index.php?action=total_attendance_vedomost">Сводная ведомость посещаемости</a></div>
	<div class="menu_item"><a href="index.php?action=total_grades_vedomost">Сводная ведомость успеваемости</a></div>
	<div class="menu_item"><a href="index.php">Справка по заполнению журнала</a></div>
	<div class="clearfix"></div>
	</div-->

<ul class="widgetSlider">
  <li>
	<div class="handleSlider">
	<img src='/img/journal_img_first.jpg'></div>
	<div class="header_menu">
		<div class="menu_item"><img src="/img/people_2.jpg" style="height:150px;"><div><a href="index.php?action=subgroups_kontingent">Списки студентов по подгруппам</a></div></div>
		<div class="menu_item"><img src="/img/people_1.jpg" style="height:150px;"><div><a href="index.php?action=journal_month_list">Заполнение текущей успеваемости и посещаемости</a></div></div>
		<div class="menu_item"><img src="/img/people_3.jpg" style="height:150px;"><div><a href="index.php?action=journal_itog_rate">Сводная ведомость по дисциплине за семестр</a></div></div>
		<div class="clearfix"></div>
	</div>
  </li>
  <li>
	<div class="handleSlider">
	<img src='/img/journal_img_second.jpg'></div>
	<div class="header_menu">
		<div class="menu_item"><img src="/img/attend_report_1.jpg" style="height:150px;"><div><a href="index.php?action=total_attendance_list">Сведения о количестве уроков, пропущенных обучающимися</a></div></div>
		<div class="menu_item"><img src="/img/attend_report_2.jpg" style="height:150px;"><div><a href="index.php?action=total_attendance_vedomost">Сводная ведомость посещаемости</a></div></div>
		<div class="menu_item"><img src="/img/attend_report_3.jpg" style="height:150px;"><div><a href="index.php?action=total_grades_vedomost">Сводная ведомость успеваемости</a></div></div>
	</div>
  </li>
  <li>
	<div class="handleSlider">
	<img src='/img/journal_img_third.jpg'></div>
	<div class="header_menu">
		<div class="menu_item"><img src="/img/help.jpg" style="height:150px;"><div><a href="index.php?action=help">Справка по заполнению журнала</a></div></div>
	</div>
  </li>
</ul>
<div style="clear:both;"></div>
	
	
<div class="content_area">
		<?php
		
		$action=@$_REQUEST['action'];
		
		if($action=='journal_month_list') 
			{
				 require_once "month_list.php";
			} 
		elseif ($action=='journal_itog_rate') 
			{
				require_once "itog_rate.php";
			}
		elseif ($action=='total_attendance_list') 
			{
				require_once "total_attendance_list.php";
			}
		elseif ($action=='total_attendance_vedomost') 
			{
				require_once "total_attendance_vedomost.php";
			}	
		elseif ($action=='total_grades_vedomost') 
			{
				require_once "total_grades_vedomost.php";
			}
		elseif ($action=='subgroups_kontingent') 
			{
				require_once "subgroups_kontingent.php";
			}
		elseif ($action=='help') 
			{
				require_once "journal_help.php";
			}	
		elseif ($action==NULL) 
			{
				require_once "month_list.php";
			}	
		?>

</div>
</div>
</div>
</body>
</html>
