<?
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Pragma: no-cache');
header('Content-Type: text/html; charset=utf-8');

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])) {
   // Здесь обрабатываем данные, если отключен JavaScript
   exit('Don\'t sent by AJAX');
}
else {

	require_once("../bd_config.php");
	require_once("../bd_functions.php");
	require_once("../common_functions.php");
	if (isset($_POST['group'])) $group = $_POST['group'];
	else $group = '';
	if (isset($_POST['subgroup'])) $subgroup = $_POST['subgroup'];
	else $subgroup = '';
	if (isset($_POST['disc'])) $disc = $_POST['disc'];
	else $disc = '';
	if (isset($_POST['prepod'])) $prepod = $_POST['prepod'];
	else $prepod = '';
	if (isset($_POST['month'])) $month = $_POST['month'];
	else $month = '';
	if (isset($_POST['status'])) $status = $_POST['status'];
	else $status = '';
	if (isset($_POST['semestr'])) $semestr = $_POST['semestr'];
	else $semestr = '';
	if (isset($_POST['lesson_id'])) $lesson_id = $_POST['lesson_id'];
	else $lesson_id = '';
	if (isset($_POST['vid_perioda'])) $vid_perioda = $_POST['vid_perioda'];
	else $vid_perioda = '';
	if (isset($_POST['day_of_month'])) $day_of_month = $_POST['day_of_month'];
	else $day_of_month = '';
	if (isset($_POST['subgroup'])) $subgroup = $_POST['subgroup'];
	else $subgroup = '';

	$conn = mysql_connect($dbhost, $dbuser, $dbpassword) or die("Connection Error: " . mysql_error());
	mysql_select_db($db) or die("Error conecting to db.");
	mysql_query("SET NAMES 'utf8'");

	if (($status=="change_groups") && $group)
		{
			
				$result_array=get_disciplines_by_group($group);
				
				$i=1;
				$first_disc_in_list='';
				foreach ($result_array as $d)
				{
					if ($i==1)
					{
						$first_disc_in_list=$d['discipline_id'];
					}
					foreach ($d as $r_key=>$r_val)
						{
							$responce->disc[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}

				$result_array=get_month_by_disc($group,$first_disc_in_list);
			
				$i=1;
				$first_month_in_list='';
				foreach ($result_array as $d)
				{
					if ($i==1)
					{
						$first_month_in_list=$d['month_id'];
					}
					foreach ($d as $r_key=>$r_val)
						{
							$responce->month[$i][$r_key]=get_rus_names_for_month($d[$r_key]);
						}
					$i++;
				}
				
				$result_array=get_prepod_by_month($group,$first_disc_in_list,$first_month_in_list);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->prepod[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			
			echo json_encode($responce);
		}
	elseif (($status=="change_disc") && $group && $disc)	
		{
				$result_array=get_month_by_disc($group,$disc);
			
				$i=1;
				$first_month_in_list='';
				foreach ($result_array as $d)
				{
					if ($i==1)
					{
						$first_month_in_list=$d['month_id'];
					}
					foreach ($d as $r_key=>$r_val)
						{
							$responce->month[$i][$r_key]=get_rus_names_for_month($d[$r_key]);
						}
					$i++;
				}
				
				$result_array=get_prepod_by_month($group,$disc,$first_month_in_list);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->prepod[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			
			echo json_encode($responce);
		}
	elseif (($status=="change_month") && $group && $disc && $month )	
		{
				
				$result_array=get_prepod_by_month($group,$disc,$month);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->prepod[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			
			echo json_encode($responce);
		}	
	elseif (($status=="get_journal_list") && $group && $subgroup && $month && $disc && $prepod)	
		{
			$table="";

			$edu_periods=get_education_periods();
			
			//тянем студентов			
			if (in_array($month,array("9","10","11","12"))) 
			{
				$year=$edu_periods[0]['year']; //первая строка - первый семестр
			} 
				else 
			{
				$year=$edu_periods[1]['year']; 
			}
	
			$month_begin_date=$year."-".$month."-1";
			$last_date=date("t",strtotime($year."-".$month."-1"));
			$month_end_date=$year."-".$month."-".$last_date;

			$st_arr_begin_month=get_list_of_students($month_begin_date,$group);
			$st_arr_end_month=get_list_of_students($month_end_date,$group);

			$common_student_list=sort_students_by_the_family(array_unique(array_merge($st_arr_begin_month,$st_arr_end_month)));
			
			if ($subgroup!='group')
			{
				$subgroup_list=get_subgroup_list($group,$subgroup,$disc);
				get_common_student_list_by_crossing($common_student_list,$subgroup_list);
			}
			
			//тянем занятия
			$lessons_common_array=array();
			$lessons_list=get_one_journal_list_lessons_by_prepod($month,$disc,$group,$prepod);
			
			if (count($lessons_list)>0) //если есть занятия вообще. тогда
				{
					
					$i=1;
					$lessons_id_list=array(); $att_id_list=array();
					
					//формируем "правую" сторону журнала , и общий спсиок занятий
					$table_description="<table id='description_table'>";
					$table_description.="<tr class='table_description_header'><td></td><td></td><td>Дата занятия</td><td>Тема занятия</td><td>Задание для СР</td><td>Тип занятия</td></tr>";
					foreach ($lessons_list as $lesson)
					{
						$lessons_common_array[$lesson['lesson_id']]['lesson_date']=$lesson['lesson_date'];
						$lessons_common_array[$lesson['lesson_id']]['is_att']=$lesson['att'];
						
						if ($lesson['att'])
							{
								$edit_cell='edit_att_'.$lesson['lesson_id'];
								$att_id_list[]=$lesson['lesson_id'];
							}
						else
							{
								$edit_cell='edit_lesson_'.$lesson['lesson_id'];
								$lessons_id_list[]=$lesson['lesson_id'];
							}
						$delete_cell='delete_lesson_'.$lesson['lesson_id'];
						
						$table_description.='<tr>'."<td><div id='$edit_cell' data-options='{\"lesson\":\"$lesson[lesson_id]\",\"date\":\"$lesson[lesson_date]\"}'><img src='/img/doc-edit.png' width='30' height='30'></div></td><td><div id='$delete_cell' data-options='{\"lesson\":\"$lesson[lesson_id]\"}'><img src='/img/red-cross.png' width='30' height='30'></div></td><td>".get_rus_names_for_month(date("j F Y",strtotime($lesson['lesson_date'])))."</td>".($lesson['att']?"<td colspan='3'>".$lesson['theme']."</td>":"<td>".$lesson['theme']."</td>"."<td>".$lesson['homework']."</td>"."<td>".$lesson['lesson_type']."</td>")."</tr>";
						
						$i++;
					}
					$table_description.='</table>';
					
					//формируем "правую" сторону журнала , и общий спсиок занятий
					
					if (count($lessons_id_list)>0)
					{
						//тянем оценки студентов
						$grades_list=get_one_journal_list_grades(implode(',',$lessons_id_list));
						
						$i=1;
						$lesson_id=$student_id="";
						$grade=array();
						foreach ($grades_list as $grade_str)
						{
							if ($lesson_id!=$grade_str['lesson_id'])
								{
									$lesson_id=$grade_str['lesson_id'];
									$grade=array();
								}
							
							if ($student_id!=$grade_str['student_id'])
								{
									$student_id=$grade_str['student_id'];
									$grade=array();
								}
							
							$grade[]=(($grade_str['rate']=='0')?'отр':$grade_str['rate']);

							$lessons_common_array[$lesson_id]['students'][$grade_str['student_id']]['grade']=implode('/',$grade);
							$i++;
						}
						
						//тянем пропуски
						$attend_list=get_one_journal_list_attend(implode(',',$lessons_id_list));
						
						
						$i=1;
						$lesson_id=$student_id="";
						$attend="";
						foreach ($attend_list as $attend_str)
						{
							if ($lesson_id!=$attend_str['lesson_id'])
								{
									$lesson_id=$attend_str['lesson_id'];
									$attend="";
								}
							
							if ($student_id!=$attend_str['student_id'])
								{
									$student_id=$attend_str['student_id'];
									$attend="";
								}
							
							$attend.=$attend_str['case_title']; // как бээ пропуск может быть только один на урок, но хз

							$lessons_common_array[$lesson_id]['students'][$attend_str['student_id']]['attend']['title']=$attend;
							$lessons_common_array[$lesson_id]['students'][$attend_str['student_id']]['attend']['id']=$attend_str['case_id'];
							$i++;
						}
					}
					
					if (count($att_id_list)>0)
					{
						//тянем сведения об аттестации  
						$attest_list=get_one_journal_list_attest(implode(',',$att_id_list));
						
						$i=1;
						$lesson_id=$student_id="";
						$attest="";
						foreach ($attest_list as $attest_str)
						{
							if ($lesson_id!=$attest_str['lesson_id'])
								{
									$lesson_id=$attest_str['lesson_id'];
								}
							
							if ($student_id!=$attest_str['student_id'])
								{
									$student_id=$attest_str['student_id'];
									$attest="";
								}
							
							$attest=$attest_str['att_rate']; // отметка об аттестации совершенно точно одна :)))

							$lessons_common_array[$lesson_id]['students'][$attest_str['student_id']]['attest']=$attest;
							$i++;
						}
					}
					
					//попытаемся теперь запилить итоговую таблицу
					$table.="<table id='grades_table'>";
					$table.='<tr class="table_grades_header"><td class="student_column header">Студенты/Даты занятий</td>';
					foreach ($lessons_common_array as $lesson_key=>$lesson_val)
						{
							$table.="<td id='".($lesson_val['is_att']?'att':'lesson')." $lesson_key' class='".($lesson_val['is_att']?'att_column header':'lesson_column header')."'>".date("j",strtotime($lesson_val['lesson_date']))."</td>";
						}
					$table.="</tr>";
					
					$student_number=1;
					foreach ($common_student_list as $student_id=>$student)
					{
						//смотрим, входит ли студент в подгруппу и выставляем ему класс
						if ($subgroup!='group')
						{
							if (isset($subgroup_list[$student_id]))
							{
								$student_ids=array_keys($subgroup_list);
								//оформление подчеркиванием последней строка
								$class_border_td="";
								if (array_search($student_id,$student_ids)==(count($student_ids)-1))
									$class_border_td="last_subgroup_st";
								$table.="<tr><td class='student_column rows subgroup_st $class_border_td' id='$student_id'>".$student_number.". ".$student."<span class='star_mark'>*</span></td>";
							}
							else
								$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td>";
						}
						else
							$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td>";
						
							$student_number++;
						
						foreach ($lessons_common_array as $lesson_key=>$lesson_val)
							{
								$total_spell_in_cell="";
								$grade_arr=array(); //оценок нет
								$attend_data="0";  //пропуска нет 
								$attest_data='{"type":"0","grade":"0"}';  //аттестация не установлена 
								
								
								if (isset($lesson_val['students']))
								{
									foreach ($lesson_val['students'] as $student_info_id=>$student_info_val)
									{
										
										if (('st_'.$student_info_id)==$student_id)  // искомый студент - не пустая ячейка
										{
											//в нашем случае может и пропуск стоять, и потом оценка, одновременно 
											if (isset($student_info_val['attend']))
												{
													$total_spell_in_cell.=$student_info_val['attend']['title']." ";
													$attend_data=$student_info_val['attend']['id'];
													
												}	
											if (isset($student_info_val['grade']))
												{
													$total_spell_in_cell.=$student_info_val['grade'];
													$grade_arr=explode('/',$student_info_val['grade']);
												}
											if (isset($student_info_val['attest']))
												{
													$total_spell_in_cell.=get_rus_names_for_attest_label($student_info_val['attest']);
													
													//остановимся на фиксированном списке для отметок об аттестации
													$attest_data=get_attest_data($student_info_val['attest']);
												}	
										}
										
									}
								}
								
								//формируем поле data
								if (count($grade_arr)>0)
								{
									$i=1;
									$g_str=array();
									foreach ($grade_arr as $gr)
									{
										$g_str[]='{"number":"'.$i.'","grade":"'.(($gr=='отр')?'0':$gr).'"}';
										$i++;
									}
									$grade_arr='['.implode(',',$g_str).']';
								}
								else
								{
									$grade_arr='[]';
								}
								
								$class_border_td="";
								if ($subgroup!='group')
								{
									if (isset($subgroup_list[$student_id]))
									{
										$student_ids=array_keys($subgroup_list);
										//оформление подчеркиванием последней строки
										if (array_search($student_id,$student_ids)==(count($student_ids)-1))
											$class_border_td="last_subgroup_st";
									}
								}
								
								
								if (!$lesson_val['is_att']) //если урок
								{
									$table.="<td id='GradeCell ".$student_id."_les_".$lesson_key."'"." class='lesson_column rows $class_border_td' "." data-options='{\"lesson\":\"$lesson_key\",\"student_id\":\"$student_id\"}' data-gr_att='{\"grade\":$grade_arr,\"attend\":\"$attend_data\"}'>".$total_spell_in_cell."</td>";
								}
								else //а если аттестация
								{
									$table.="<td id='AttCell ".$student_id."_les_".$lesson_key."'"." class='att_column rows $class_border_td' "." data-options='{\"lesson\":\"$lesson_key\",\"student_id\":\"$student_id\"}' data-attest='$attest_data'>".$total_spell_in_cell."</td>";
								}
								
							}
						$table.="</tr>";	
					}
					$table.='</table>';
					
				}
			else //а если их нет, тогда список студентов и заголовок таблицы КТП
				{
					$table=""; $table_description="";
					create_journal_tables_for_zero_lessons();
				}
			$html='<div style="">'.$table.'</div><div style="margin-top:80px;">'.$table_description.'</div><div style="clear:both;"></div><div><div id="add_lesson">Добавить занятие</div><div id="add_att">Добавить аттестацию</div></div>';	
			echo json_encode($html);
		}
	elseif (($status=="get_disc_list_with_prepod") && $group && $month && $disc)	
		{
			$result_array=get_disc_by_group_month($group,$month);
			
				$i=1;
				foreach ($result_array as $d)
				{
					if ($disc==$d['disc_id']) continue;// если это исключаемая дисциплина
					if ($i==1) $first_disc=$d['disc_id'];
					foreach ($d as $r_key=>$r_val)
						{
							$responce->disc[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
				
				if ($first_disc) //если вообще есть смежные дисциплины
					{
						$result_array=get_prepod_by_month($group,$first_disc,$month);
				
						$i=1;
						foreach ($result_array as $d)
						{
							foreach ($d as $r_key=>$r_val)
								{
									$responce->prepod[$i][$r_key]=$d[$r_key];
								}
							$i++;
						}
						echo json_encode($responce);
					}
				else	
					{
						echo json_encode('No data');
					}				
			
			
		}
	elseif ($status=="get_lesson_param")	
		{
			$result_array=get_lesson_param($lesson_id);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->lesson_param[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			echo json_encode($responce);	
		}
	elseif ($status=="get_date_range")	
		{
			$result_array=get_education_periods();
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->date_range[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			echo json_encode($responce);	
		}	
	elseif (($status=="total_grades_get_journal_list") && $group && $disc && $semestr)	
		{
			$table="";

			$edu_periods=get_education_periods();
			
			//тянем студентов			
			if ($semestr=='1') 
			{
				$year=$edu_periods[0]['year']; //первая строка - первый семестр
			} 
				else 
			{
				$year=$edu_periods[1]['year']; //вторая строка - второй семестр
			}
	
			//список за семестр формируется путем создания срезов на начало каждого и конец последнего месяца, входящего в семестр
			
			$month_and_sem=array("2"=>array(1,2,3,4,5),"1"=>array(9,10,11,12));
			$month_number_array=$month_and_sem[$semestr];
			
			$month_begin_and_end_array=array();
			
			$i=1;
			foreach ($month_number_array as $month_number)
			{
				if ($i==count($month_number_array))
				{
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
					$last_date=date("t",strtotime($year."-".$month_number."-1"));
					$month_begin_and_end_array[]=$year."-".$month_number."-".$last_date;//дата окончания месяца
				}
				else
				{
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
				}
				
				$i++;
			}
			
			
			$common_student_list=array();
			foreach ($month_begin_and_end_array as $month_date)
			{
					$common_student_list=array_merge($common_student_list,get_list_of_students($month_date,$group));
			}

			$common_student_list=sort_students_by_the_family(array_unique($common_student_list));
			
			if ($subgroup!='group')
			{
				$subgroup_list=get_subgroup_list($group,$subgroup,$disc);
				get_common_student_list_by_crossing($common_student_list,$subgroup_list);
			}
			
			//тянем занятия
			$lessons_common_array=array();
			$lessons_list=get_one_journal_list_lessons($month_number_array,$disc,$group);
			
			if (count($lessons_list)>0) //если есть занятия вообще. тогда
				{
					
					$i=1;
					$lessons_id_list=array(); $att_id_list=array();
					
					//формируем таблицу описания уроков..начало 
					$table_description="<table id='description_table'>";
					$table_description.="<tr class='table_description_header'><td>Дата занятия</td><td>Тема занятия</td><td>Задание для СР</td><td>Тип занятия</td></tr>";
					foreach ($lessons_list as $lesson)
					{
						$lessons_common_array[$lesson['lesson_id']]['lesson_date']=$lesson['lesson_date'];
						$lessons_common_array[$lesson['lesson_id']]['is_att']=$lesson['att'];
						
						if ($lesson['att'])
							{
								//$edit_cell='edit_att_'.$lesson['lesson_id'];
								$att_id_list[]=$lesson['lesson_id'];
							}
						else
							{
								//$edit_cell='edit_lesson_'.$lesson['lesson_id'];
								$lessons_id_list[]=$lesson['lesson_id'];
							}
						//$delete_cell='delete_lesson_'.$lesson['lesson_id'];
						
						$table_description.='<tr>'."<td>".get_rus_names_for_month(date("j F Y",strtotime($lesson['lesson_date'])))."</td>".($lesson['att']?"<td colspan='3'>".$lesson['theme']."</td>":"<td>".$lesson['theme']."</td>"."<td>".$lesson['homework']."</td>"."<td>".$lesson['lesson_type']."</td>")."</tr>";
						
						$i++;
					}
					$table_description.='</table>';
					//формируем таблицу описания уроков..конец 
					
					if (count($lessons_id_list)>0)
					{
						//тянем оценки студентов
						$grades_list=get_one_journal_list_grades(implode(',',$lessons_id_list));
						
						$i=1;
						$lesson_id=$student_id="";
						$grade=array();
						foreach ($grades_list as $grade_str)
						{
							if ($lesson_id!=$grade_str['lesson_id'])
								{
									$lesson_id=$grade_str['lesson_id'];
									$grade=array();
								}
							
							if ($student_id!=$grade_str['student_id'])
								{
									$student_id=$grade_str['student_id'];
									$grade=array();
								}
							
							$grade[]=(($grade_str['rate']=='0')?'отр':$grade_str['rate']);

							$lessons_common_array[$lesson_id]['students'][$grade_str['student_id']]['grade']=implode('/',$grade);
							$i++;
						}
						
						//тянем пропуски
						$attend_list=get_one_journal_list_attend(implode(',',$lessons_id_list));
						
						
						$i=1;
						$lesson_id=$student_id="";
						$attend="";
						foreach ($attend_list as $attend_str)
						{
							if ($lesson_id!=$attend_str['lesson_id'])
								{
									$lesson_id=$attend_str['lesson_id'];
									$attend="";
								}
							
							if ($student_id!=$attend_str['student_id'])
								{
									$student_id=$attend_str['student_id'];
									$attend="";
								}
							
							$attend.=$attend_str['case_title']; // как бээ пропуск может быть только один на урок, но хз

							$lessons_common_array[$lesson_id]['students'][$attend_str['student_id']]['attend']['title']=$attend;
							$lessons_common_array[$lesson_id]['students'][$attend_str['student_id']]['attend']['id']=$attend_str['case_id'];
							$i++;
						}
					}
					
					if (count($att_id_list)>0)
					{
						//тянем сведения об аттестации  
						$attest_list=get_one_journal_list_attest(implode(',',$att_id_list));
						
						$i=1;
						$lesson_id=$student_id="";
						$attest="";
						foreach ($attest_list as $attest_str)
						{
							if ($lesson_id!=$attest_str['lesson_id'])
								{
									$lesson_id=$attest_str['lesson_id'];
								}
							
							if ($student_id!=$attest_str['student_id'])
								{
									$student_id=$attest_str['student_id'];
									$attest="";
								}
							
							$attest=$attest_str['att_rate']; // отметка об аттестации совершенно точно одна :)))

							$lessons_common_array[$lesson_id]['students'][$attest_str['student_id']]['attest']=$attest;
							$i++;
						}
					}
					
					//тянем итоговые оценки
					$itog_grades=get_total_grades_by_group_disc_sem($group,$disc,$semestr);
					
					$itog_array=array();
					foreach ($itog_grades as $itog_grade_str)
						{
							$itog_array['st_'.$itog_grade_str['student_id']][$itog_grade_str['variant']]=array('rate'=>$itog_grade_str['rate'],'zach'=>$itog_grade_str['zach'],'rate_type'=>$itog_grade_str['rate_type']);
						}
					
					//попытаемся теперь запилить итоговую таблицу
					$table.="<table id='grades_table'>";
					//первая строка - хедер таблицы..начало
					$table.='<tr class="table_grades_header"><td rowspan="2" class="student_column header">Студенты/Даты занятий</td>';
					
					$current_month='';
					$i=1;
					$table_month_array=array();
					foreach ($lessons_common_array as $lesson_key=>$lesson_val)
						{
							
							if ($current_month!=date('F',strtotime($lesson_val['lesson_date'])))
							{
								$current_month=date('F',strtotime($lesson_val['lesson_date']));
								$i=1;
							}
							$table_month_array[$current_month]=$i;
							$i++;
						}
					foreach ($table_month_array as $month_title=>$cell_count)
						{
							$table.="<td colspan='".$cell_count."'>".get_rus_names_for_month($month_title)."</td>";
						}
						
					$table.='<td rowspan="2" class="ekz_zach header">Форма контроля</td>';	
					$table.='<td rowspan="2" class="sem_itog header">Оценка за семестр</td>';
					$table.='<td rowspan="2" class="year_itog header">Оценка за год</td>';
					// if ($semestr=='2') //второй семестр, выводим графу для итоговой оценки за год
					// {
						
					// }
					
					$table.="</tr>";
					//первая строка - хедер таблицы..конец
					
					//вторая строка -отображение дат..начало
					$table.='<tr class="table_grades_header">';
					foreach ($lessons_common_array as $lesson_key=>$lesson_val)
						{
							$table.="<td id='".($lesson_val['is_att']?'att':'lesson')." $lesson_key' class='".($lesson_val['is_att']?'att_column header':'lesson_column header')."'>".date("j",strtotime($lesson_val['lesson_date']))."</td>";
						}
					$table.="</tr>";
					//вторая строка..конец
					
					$student_number=1;
					//начинаем раскручивать итоговые массивы 
					foreach ($common_student_list as $student_id=>$student)
					{
						//$table.="<tr><td class='student_column student_name' id='$student_id'>$student</td>";
						
						//оформление подчеркиванием последней строки
						$class_border_td="";
						if ($subgroup!='group')
						{
							if (isset($subgroup_list[$student_id]))
							{
								$student_ids=array_keys($subgroup_list);
								if (array_search($student_id,$student_ids)==(count($student_ids)-1))
									$class_border_td="last_subgroup_st";
							}
						}
						
						//смотрим, входит ли студент в подгруппу и выставляем ему класс
						if ($subgroup!='group')
						{
							if (isset($subgroup_list[$student_id]))
							{
								$student_ids=array_keys($subgroup_list);
								//оформление подчеркиванием последней строка
								$class_border_td="";
								if (array_search($student_id,$student_ids)==(count($student_ids)-1))
									$class_border_td="last_subgroup_st";
								$table.="<tr><td class='student_column rows subgroup_st $class_border_td' id='$student_id'>".$student_number." ".$student."<span class='star_mark'>*</span></td>";
							}
							else
								$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td>";
						}
						else
							$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td>";
						
						$student_number++;
						
						//отображаем промежуточные оценки из журнала..начало
						foreach ($lessons_common_array as $lesson_key=>$lesson_val)
							{
								$total_spell_in_cell="";
								$grade_arr=array(); //оценок нет
								$attend_data="0";  //пропуска нет 
								$attest_data='{"type":"0","grade":"0"}';  //аттестация не установлена 
								
								
								if (isset($lesson_val['students']))
								{
									foreach ($lesson_val['students'] as $student_info_id=>$student_info_val)
									{
										
										if (('st_'.$student_info_id)==$student_id)  // искомый студент - не пустая ячейка
										{
											//в нашем случае может и пропуск стоять, и потом оценка, одновременно 
											if (isset($student_info_val['attend']))
												{
													$total_spell_in_cell.=$student_info_val['attend']['title']." ";
													$attend_data=$student_info_val['attend']['id'];
													
												}	
											if (isset($student_info_val['grade']))
												{
													$total_spell_in_cell.=$student_info_val['grade'];
													$grade_arr=explode('/',$student_info_val['grade']);
												}
											if (isset($student_info_val['attest']))
												{
													$total_spell_in_cell.=get_rus_names_for_attest_label($student_info_val['attest']);
													
													//остановимся на фиксированном списке для отметок об аттестации
													$attest_data=get_attest_data($student_info_val['attest']);
												}	
										}
										
									}
								}
								
								//формируем поле data
								if (count($grade_arr)>0)
								{
									$i=1;
									$g_str=array();
									foreach ($grade_arr as $gr)
									{
										$g_str[]='{"number":"'.$i.'","grade":"'.(($gr=='отр')?'0':$gr).'"}';
										$i++;
									}
									$grade_arr='['.implode(',',$g_str).']';
								}
								else
								{
									$grade_arr='[]';
								}
								
								
								if (!$lesson_val['is_att']) //если урок
								{
									$table.="<td id='GradeCell ".$student_id."_les_".$lesson_key."'"." class='lesson_column rows $class_border_td' "." data-options='{\"lesson\":\"$lesson_key\",\"student_id\":\"$student_id\"}' data-gr_att='{\"grade\":$grade_arr,\"attend\":\"$attend_data\"}'>".$total_spell_in_cell."</td>";
								}
								else //а если аттестация
								{
									$table.="<td id='AttCell ".$student_id."_les_".$lesson_key."'"." class='att_column rows $class_border_td' "." data-options='{\"lesson\":\"$lesson_key\",\"student_id\":\"$student_id\"}' data-attest='$attest_data'>".$total_spell_in_cell."</td>";
								}
								
							}
						//отображаем промежуточные оценки из журнала..конец
						
						//вывод итоговыx оценок из журнала... начало
						if (isset($itog_array[$student_id]))
						{
							$variant_title_array=array('ekz_zach','sem_itog','year_itog');
							foreach ($variant_title_array as $variant_title)
							{
								if (isset($itog_array[$student_id][$variant_title]))
								{
									$total_spell_in_cell="";
									$grade_opt=0; $zach_opt=0;
									if ($itog_array[$student_id][$variant_title]['rate_type']=='grade')
										{	
											$total_spell_in_cell.=$itog_array[$student_id][$variant_title]['rate'];
											$rate_type=$itog_array[$student_id][$variant_title]['rate_type']; $grade_opt=$itog_array[$student_id][$variant_title]['rate'];
										}
							
									if ($itog_array[$student_id][$variant_title]['rate_type']=='zach')
										{	
											$total_spell_in_cell.=($itog_array[$student_id][$variant_title]['zach']==1?'зач':'н/з');
											$rate_type=$itog_array[$student_id][$variant_title]['rate_type'];
											$zach_opt=$itog_array[$student_id][$variant_title]['zach'];
										}
									
									if ($itog_array[$student_id][$variant_title]['rate_type']=='dif_zach')
										{	
											$total_spell_in_cell.=$itog_array[$student_id][$variant_title]['rate'];
											$rate_type=$itog_array[$student_id][$variant_title]['rate_type'];
											$grade_opt=$itog_array[$student_id][$variant_title]['rate'];
										}
										
									if ($variant_title=='ekz_zach')
											$td_id='ExamsGradeCell '.$student_id;
									else
											$td_id='TotalGradeCell '.$student_id.' '.$variant_title;
										
									$table.="<td id='$td_id' data-options='{\"student\":\"$student_id\",\"variant\":\"$variant_title\",\"rate_opt\":{\"rate_type\":\"$rate_type\",\"grade\":\"$grade_opt\",\"zach\":\"$zach_opt\"}}' class=\"$variant_title $class_border_td rows\">".$total_spell_in_cell."</td>";
								}
									else
								{
									//оценку за год выводим и для первого, и для второго семсетра - где найдут нужным, там и заполнят
									if ($variant_title=='ekz_zach')
											$td_id='ExamsGradeCell '.$student_id;
									else
											$td_id='TotalGradeCell '.$student_id.' '.$variant_title;
									
									$table.="<td id='$td_id' data-options='{\"student\":\"$student_id\",\"variant\":\"$variant_title\",\"rate_opt\":{}}' class=\"$variant_title $class_border_td rows\"></td>"; 
								}
							}
						}
						else
						{
							$table.="<td id='ExamsGradeCell $student_id' data-options='{\"student\":\"$student_id\",\"variant\":\"ekz_zach\",\"rate_opt\":{}}' class=\"ekz_zach rows $class_border_td\"></td>";
							$table.="<td id='TotalGradeCell $student_id sem_itog' data-options='{\"student\":\"$student_id\",\"variant\":\"sem_itog\",\"rate_opt\":{}}' class=\"sem_itog rows $class_border_td\"></td>";
							$table.="<td id='TotalGradeCell $student_id year_itog' data-options='{\"student\":\"$student_id\",\"variant\":\"year_itog\",\"rate_opt\":{}}' class=\"year_itog rows $class_border_td\"></td>";
								
						}
						//вывод итоговыx оценок из журнала... конец
						
						$table.="</tr>";	
					}
					$table.='</table>';
					
				}
			else //а если их нет, тогда просто список студентов
				{
					$table=""; $table_description="";
					create_journal_tables_for_zero_lessons();
				}
			$html='<div style="">'.$table.'</div><div style="margin-top:100px;">'.$table_description.'</div></div>';	
			echo json_encode($html);
		}	
	elseif (($status=="total_grades_change_groups") && $group)
		{
			
				$result_array=get_disciplines_by_group($group);
				
				$i=1;
				$first_disc_in_list='';
				foreach ($result_array as $d)
				{
					if ($i==1)
					{
						$first_disc_in_list=$d['discipline_id'];
					}
					foreach ($d as $r_key=>$r_val)
						{
							$responce->disc[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}

				$result_array=get_semestr_by_disc($group,$first_disc_in_list);
			
				$i=1;
				$first_semestr_in_list='';
				foreach ($result_array as $d)
				{
					if ($i==1)
					{
						$first_semestr_in_list=$d;
					}
					
					$responce->semestr[$i]['semestr_id']=$d;
					$responce->semestr[$i]['title']=($d=='1'?'1 семестр':'2 семестр');
						
					$i++;
				}
				
				$result_array=get_prepod_by_semestr($group,$first_disc_in_list,$first_semestr_in_list);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->prepod[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			
			echo json_encode($responce);
		}
	elseif (($status=="total_grades_change_disc") && $group && $disc)
		{
			
				$result_array=get_semestr_by_disc($group,$disc);
			
				$i=1;
				$first_semestr_in_list='';
				foreach ($result_array as $d)
				{
					if ($i==1)
					{
						$first_semestr_in_list=$d;
					}
					
					$responce->semestr[$i]['semestr_id']=$d;
					$responce->semestr[$i]['title']=($d=='1'?'1 семестр':'2 семестр');
						
					$i++;
				}
				
				$result_array=get_prepod_by_semestr($group,$disc,$first_semestr_in_list);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->prepod[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			
			echo json_encode($responce);
		}	
	elseif (($status=="total_grades_change_semestr") && $group && $disc && $semestr)
		{
			
				$result_array=get_prepod_by_semestr($group,$disc,$semestr);
			
				$i=1;
				foreach ($result_array as $d)
				{
					foreach ($d as $r_key=>$r_val)
						{
							$responce->prepod[$i][$r_key]=$d[$r_key];
						}
					$i++;
				}
			
			echo json_encode($responce);
		}
	elseif (($status=="get_total_attendance_list") && $group)	
		{
			$table="";

			$edu_periods=get_education_periods();
			$year=""; $first_sem_year=""; $second_sem_year="";
			$month_and_sem=array("2"=>array(1,2,3,4,5),"1"=>array(9,10,11,12));
			$sem_and_month=array("9"=>"1","10"=>"1","11"=>"1","12"=>"1","1"=>"2","2"=>"2","3"=>"2","4"=>"2","5"=>"2","6"=>"2");
			
			if ($vid_perioda=='year')
			{
				$month_number_array=array(9,10,11,12,1,2,3,4,5);
				
				$first_sem_year=$edu_periods[0]['year']; //первая строка - первый семестр
				$second_sem_year=$edu_periods[1]['year']; //вторая строка - второй семестр
				
				$begin_date=$first_sem_year."-".$month_number_array[0]."-1";
			
				$last_date=date("t",strtotime($second_sem_year."-".$month_number_array[count($month_number_array)-1]."-1"));
				
				$end_date=$second_sem_year."-".$month_number_array[count($month_number_array)-1]."-".$last_date;
			}
			elseif ($vid_perioda=='semestr')
			{
				$month_number_array=$month_and_sem[$semestr];
				if ($semestr=='1') 
					{
						$year=$edu_periods[0]['year']; //первая строка - первый семестр
					} 
				else 
					{
						$year=$edu_periods[1]['year']; //вторая строка - второй семестр
					}
				
				$begin_date=$year."-".$month_number_array[0]."-1";
			
				$last_date=date("t",strtotime($year."-".$month_number_array[count($month_number_array)-1]."-1"));
				
				$end_date=$year."-".$month_number_array[count($month_number_array)-1]."-".$last_date;
			}
			elseif ($vid_perioda=='month')
			{
				$month_number_array=array($month);
				if ($sem_and_month[$month]=='1')
					$year=$edu_periods[0]['year'];
				else
					$year=$edu_periods[1]['year'];
					
				$begin_date=$year."-".$month."-1";
			
				$last_date=date("t",strtotime($year."-".$month."-1"));
				
				$end_date=$year."-".$month."-".$last_date;	
			}	
			
			
			//список за семестр формируется путем создания срезов на начало каждого и конец последнего месяца, входящего в семестр
			$month_begin_and_end_array=array();
			
			$i=1;
			foreach ($month_number_array as $month_number)
			{
				if ($i==count($month_number_array))
				{
					if ($vid_perioda=='year')
						{
							if ($sem_and_month[$month_number]=='1')
								$year=$first_sem_year;
							else
								$year=$second_sem_year;
						}
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
					$last_date=date("t",strtotime($year."-".$month_number."-1"));
					$month_begin_and_end_array[]=$year."-".$month_number."-".$last_date;//дата окончания месяца
				}
				else
				{
					if ($vid_perioda=='year')
						{
							if ($sem_and_month[$month_number]=='1')
								$year=$first_sem_year;
							else
								$year=$second_sem_year;
						}
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
				}
				
				$i++;
			}
			
			//тянем студентов
			$common_student_list=array();
			foreach ($month_begin_and_end_array as $month_date)
			{
					$common_student_list=array_merge($common_student_list,get_list_of_students($month_date,$group));
			}

			$common_student_list=sort_students_by_the_family(array_unique($common_student_list));
			
			//тянем пропуски
			$attendance_list=get_total_attendance($begin_date,$end_date,$group,'group_by_date');
			$att_assoc=array();
			
			//делаем из него ассоциативный массив =>для увеличения скорости обработки в дальнейшем
			foreach ($attendance_list as $att_str)
				{
					$att_assoc[$att_str['lesson_date']]['st_'.$att_str['student_id']]=array('total'=>$att_str['count'],'resp'=>$att_str['resp'],'unresp'=>$att_str['unresp']);
				}
			
			//тянем даты занятий
			$lessons_date_list=get_lessons_dates($begin_date,$end_date,$group);
			
			
			if (count($lessons_date_list)>0) //если есть занятия вообще. тогда
				{
					//попытаемся теперь запилить итоговую таблицу
					$table.="<table class='total_attendance_list'>";
					
					//первая строка - хедер таблицы..начало
					$table.='<tr><td rowspan="2">Студенты</td>';
					//сначала формируем массив мясяцев и количество дней в них
					$i=1; $current_month=''; $table_month_array=array();
					foreach ($lessons_date_list as $lesson_key=>$lesson_val)
						{
							
							if ($current_month!=date('F',strtotime($lesson_val['lesson_date'])))
							{
								$current_month=date('F',strtotime($lesson_val['lesson_date']));
								$i=1;
							}
							$table_month_array[$current_month]=$i;
							$i++;
						}
						
					foreach ($table_month_array as $month_title=>$cell_count)
						{
							$table.="<td colspan='".($cell_count+3)."' class='Vedomost_Month_Cell'>".get_rus_names_for_month($month_title)."</td>"; //+3 - это для итоговых колонок по месяцам
						}
						
					
					$table.="</tr>";
					//первая строка - хедер таблицы..конец
					
					//вторая строка -отображение дат и подзаголовков..начало
					$table.='<tr>';
					$i=1;
					foreach ($lessons_date_list as $lesson_key=>$lesson_val)
						{
							$table.="<td id='date' class='Vedomost_Cell'>".date("j",strtotime($lesson_val['lesson_date']))."</td>";
							
							if ($table_month_array[date('F',strtotime($lesson_val['lesson_date']))]==$i) //проверяем количество дней в месяце
									{
										$table.="<td>Всего за месяц</td><td>По ув. причине</td><td>По неув. причине</td>";
										$i=0;
									}
								
							$i++;
						}
					$table.="</tr>";
					//вторая строка..конец
					
					
					//раскручиваем итоговые массивы.. начало
					foreach ($common_student_list as $student_id=>$student)
					{
						$table.="<tr><td class='student_column student_name' id='$student_id'>$student</td>";
						
						$i=1; $total_att_in_month=0; $total_resp_in_month=0; $total_unresp_in_month=0;
						foreach ($lessons_date_list as $lesson_date)
							{
								 if (!isset($att_assoc[$lesson_date['lesson_date']][$student_id]))
								 $table.="<td></td>";
									 else
								 $table.="<td>".($att_assoc[$lesson_date['lesson_date']][$student_id]['total']*2)."</td>";	
								
								$total_att_in_month+=($att_assoc[$lesson_date['lesson_date']][$student_id]['total']*2);
								$total_resp_in_month+=($att_assoc[$lesson_date['lesson_date']][$student_id]['resp']*2);
								$total_unresp_in_month+=($att_assoc[$lesson_date['lesson_date']][$student_id]['unresp']*2);

								if ($table_month_array[date('F',strtotime($lesson_date['lesson_date']))]==$i) //проверяем количество дней в месяце
									{
										$table.="<td>".$total_att_in_month."</td><td>".$total_resp_in_month."</td><td>".$total_unresp_in_month."</td>";
										$i=0;
										$total_att_in_month=0; $total_resp_in_month=0; $total_unresp_in_month=0;
										
									}
								
								$i++;
							}
						$table.="</tr>";
						
					}
					$table.="</table>";
					$html=$table;
					//раскручиваем итоговые массивы.. конец					
				}
			else
				{
					$html='За указанный период данные в журнале не заполнены';
				}
			
			
			echo json_encode($html);
		}
	elseif (($status=="get_total_attendance_vedomost") && $group)	
		{
			$table="";

			$edu_periods=get_education_periods();
			$year=""; $first_sem_year=""; $second_sem_year="";
			$month_and_sem=array("2"=>array(1,2,3,4,5),"1"=>array(9,10,11,12));
			$sem_and_month=array("9"=>"1","10"=>"1","11"=>"1","12"=>"1","1"=>"2","2"=>"2","3"=>"2","4"=>"2","5"=>"2","6"=>"2");
			
			//получаем параметры для отображения списка студентов за год ..начало
			$month_number_array=array(9,10,11,12,1,2,3,4,5);
			$period_assoc_array=array();	
				

			//для первого семестра

			$year=$edu_periods[0]['year']; //первая строка - первый семестр
		
			$period_assoc_array['sem1']['begin_date']=$year."-"."09"."-1";
		
			$last_date=date("t",strtotime($year."-"."12"."-1"));
			
			$period_assoc_array['sem1']['end_date']=$year."-"."12"."-".$last_date;
			//для второго семестра
			
			$year=$edu_periods[1]['year']; //первая строка - первый семестр
		
			$period_assoc_array['sem2']['begin_date']=$year."-"."01"."-1";
		
			$last_date=date("t",strtotime($year."-"."05"."-1"));
			
			$period_assoc_array['sem2']['end_date']=$year."-"."05"."-".$last_date;
			
			//получаем параметры для отображения списка студентов за год ..конец
			
			//параметры за год	
			$first_sem_year=$edu_periods[0]['year']; //первая строка - первый семестр
			$second_sem_year=$edu_periods[1]['year']; //вторая строка - второй семестр
			
			$period_assoc_array['year']['begin_date']=$first_sem_year."-".$month_number_array[0]."-1";
		
			$last_date=date("t",strtotime($second_sem_year."-".$month_number_array[count($month_number_array)-1]."-1"));
			
			$period_assoc_array['year']['end_date']=$second_sem_year."-".$month_number_array[count($month_number_array)-1]."-".$last_date;
			//параметры за год
			
			//получаем список дат месяцев и тянем студентов
			//надо бы выделить в отдельную функцию
			$i=1; $month_begin_and_end_array=array();
			foreach ($month_number_array as $month_number)
			{
				if ($i==count($month_number_array))
				{
					
						if ($sem_and_month[$month_number]=='1')
							$year=$first_sem_year;
						else
							$year=$second_sem_year;
					
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
					$last_date=date("t",strtotime($year."-".$month_number."-1"));
					$month_begin_and_end_array[]=$year."-".$month_number."-".$last_date;//дата окончания месяца
				}
				else
				{
					
						if ($sem_and_month[$month_number]=='1')
							$year=$first_sem_year;
						else
							$year=$second_sem_year;
						
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
				}
				
				$i++;
			}
			
			//тянем студентов
			$common_student_list=array();
			foreach ($month_begin_and_end_array as $month_date)
			{
					$common_student_list=array_merge($common_student_list,get_list_of_students($month_date,$group));
			}

			$common_student_list=sort_students_by_the_family(array_unique($common_student_list));
			
			
			//получаем для всех трех периодов данные
			foreach ($period_assoc_array as $period_key=>$period_val)
			{
				//тянем пропуски
				$attendance_list=get_total_attendance($period_val['begin_date'],$period_val['end_date'],$group,'group_by_student');
				$att_assoc=array();
				
				//делаем из него ассоциативный массив =>для увеличения скорости обработки в дальнейшем
				foreach ($attendance_list as $att_str)
					{
						$att_assoc['st_'.$att_str['student_id']]=array('total'=>($att_str['count']*2),'resp'=>($att_str['resp']*2),'unresp'=>($att_str['unresp']*2));
					}
				$period_assoc_array[$period_key]['att_list']=$att_assoc;
			}
			
			//попытаемся теперь запилить итоговую таблицу
					$table.="<table class='total_attendance_vedomost'>";
					
					//первая строка - хедер таблицы..начало
					$table.='<tr class="important"><td rowspan="4">Студенты</td><td colspan="9">Пропущено</td></tr>';
					//вторая строка 
					$table.='<tr class="important"><td colspan="3">1 семестр</td><td colspan="3">2 семестр</td><td colspan="3">За год</td></tr>';
					//третья строка 
					$table.='<tr class="important"><td colspan="3">Занятий</td><td colspan="3">Занятий</td><td colspan="3">Занятий</td></tr>';
					//четвертая строка 
					$table.='<tr class="important"><td>Всего</td><td>По уваж. причине</td><td>По неуваж. причине</td><td>Всего</td><td>По уваж. причине</td><td>По неуваж. причине</td><td>Всего</td><td>По уваж. причине</td><td>По неуваж. причине</td></tr>';
					
					//раскручиваем итоговые массивы.. начало
					//рассчитываем общее число пропусков массив для этого
					$itog_attend_array=array(0,0,0,0,0,0,0,0,0); 
					foreach ($common_student_list as $student_id=>$student)
					{
						$table.="<tr><td class='student_column student_name' id='$student_id'>$student</td>";
						
						$i=0; 
						
						foreach ($period_assoc_array as $period_key=>$period_val)
							{
								 if (!isset($period_val['att_list'][$student_id]))
										$table.="<td></td><td></td><td></td>";
									 else
								 {
									  $table.="<td>".$period_val['att_list'][$student_id]['total']."</td><td>".$period_val['att_list'][$student_id]['resp']."</td><td>".$period_val['att_list'][$student_id]['unresp']."</td>";
									  $itog_attend_array[$i*3]+=($period_val['att_list'][$student_id]['total']);
									  $itog_attend_array[$i*3+1]+=($period_val['att_list'][$student_id]['resp']);
									  $itog_attend_array[$i*3+2]+=($period_val['att_list'][$student_id]['unresp']);
								 }
								$i++;
							}
						$table.="</tr>";
						
					}
					
					//строки с итоговыми показателями
					$table.="<tr class='important'><td>По группе</td>";
						foreach ($itog_attend_array as $total_count)
						{
							$table.="<td>".$total_count."</td>";
						}
					$table.="</tr>";
					$table.="<tr class='important'><td>На одного студента</td>";
						foreach ($itog_attend_array as $total_count)
						{
							$table.="<td>".round($total_count/count($common_student_list),1)."</td>";
						}
					$table.="</tr>";
					
					$table.="</table>";
					$html=$table;
			
			echo json_encode($html);
		}
	elseif (($status=="get_total_grades_vedomost") && $group)	
		{
			$table="";

			$edu_periods=get_education_periods();
			$year=""; $first_sem_year=""; $second_sem_year="";
			$month_and_sem=array("2"=>array(1,2,3,4,5),"1"=>array(9,10,11,12));
			$sem_and_month=array("9"=>"1","10"=>"1","11"=>"1","12"=>"1","1"=>"2","2"=>"2","3"=>"2","4"=>"2","5"=>"2","6"=>"2");
			
			//получаем параметры для отображения списка студентов за год ..начало
			$month_number_array=array(9,10,11,12,1,2,3,4,5);
			
			//параметры за год	
			$first_sem_year=$edu_periods[0]['year']; //первая строка - первый семестр
			$second_sem_year=$edu_periods[1]['year']; //вторая строка - второй семестр
			//параметры за год
			//получаем параметры для отображения списка студентов за год ..конец
			
			//получаем список дат месяцев и тянем студентов
			//надо бы выделить в отдельную функцию
			$i=1; $month_begin_and_end_array=array();
			foreach ($month_number_array as $month_number)
			{
				if ($i==count($month_number_array))
				{
					
						if ($sem_and_month[$month_number]=='1')
							$year=$first_sem_year;
						else
							$year=$second_sem_year;
					
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
					$last_date=date("t",strtotime($year."-".$month_number."-1"));
					$month_begin_and_end_array[]=$year."-".$month_number."-".$last_date;//дата окончания месяца
				}
				else
				{
					
						if ($sem_and_month[$month_number]=='1')
							$year=$first_sem_year;
						else
							$year=$second_sem_year;
						
					$month_begin_and_end_array[]=$year."-".$month_number."-1"; //дата начала месяца
				}
				
				$i++;
			}
			
			//тянем студентов
			$common_student_list=array();
			foreach ($month_begin_and_end_array as $month_date)
			{
					$common_student_list=array_merge($common_student_list,get_list_of_students($month_date,$group));
			}

			$common_student_list=sort_students_by_the_family(array_unique($common_student_list));
			
			
			//тянем итоговые оценки
			$grade_list=get_total_grades($group);
			$grade_assoc=array();
			
			//делаем из него ассоциативный массив =>для увеличения скорости обработки в дальнейшем
			foreach ($grade_list as $grade_str)
				{
					$period_col="";
					
					if ($grade_str['variant']=='ekz_zach')
						$period_col='EKZ '.$grade_str['semestr'];
					else if ($grade_str['variant']=='sem_itog')
						$period_col='SEM '.$grade_str['semestr'];
					else if ($grade_str['variant']=='year_itog')
						$period_col='YEAR';	
					
					$grade_assoc['st_'.$grade_str['student_id']][$grade_str['disc_id']][$period_col]=($grade_str['rate_type']=='zach'?($grade_str['grade']=='1'?'зач':'незач'):$grade_str['grade']);
				}
			
			$disc_list=get_disciplines_by_group($group);
			$periods_list=array('SEM 1'=>'1 Семестр','EKZ 1'=>'Экзамен 1 семестр','SEM 2'=>'2 Семестр','EKZ 2'=>'Экзамен 2 семестр','YEAR'=>'Год');
			
			//попытаемся теперь запилить итоговую таблицу
					$table.="<table class='total_grades_vedomost'>";
					
					//первая строка - хедер таблицы..начало
					$table.='<tr><td>Студенты</td><td>Дисциплины/Сроки</td>';
					foreach ($disc_list as $disc_key=>$disc_val)
					{
						$table.='<td>'.$disc_val['title'].'</td>';
					}					
					$table.='</tr>';
					
					//раскручиваем итоговые массивы.. начало
					
					foreach ($common_student_list as $student_id=>$student)
					{
						$table.="<tr><td class='student_column student_name' id='$student_id' rowspan='5'>$student</td>";
						$i=1;
						foreach ($periods_list as $per_key=>$per_title)
						{
							if ($i!=1) $table.="<tr>";
							$table.='<td>'.$per_title.'</td>';
							foreach ($disc_list as $disc_key=>$disc_val)
								{
									 if (isset($grade_assoc[$student_id][$disc_val['discipline_id']][$per_key]))
										$table.='<td>'.$grade_assoc[$student_id][$disc_val['discipline_id']][$per_key].'</td>';
									else
										$table.='<td></td>';
								}
							$table.="</tr>";
							$i++;
						}
						
					}
					$table.="</table>";
					$html=$table;
			
			echo json_encode($html);
		}
	elseif ($status=="get_subgroups_lists")
		{
			$table_subgroup_list="";
			$table_group_list="";
			
			$subgroup_st_list=sort_students_by_the_family(get_subgroup_list($group,$subgroup,$disc));
		
			$table_subgroup_list.="<table class='subgroup_list'>";
			$table_subgroup_list.="<tr class='header'><td colspan='2'>Список студентов в подгруппе</td></tr>";
			foreach ($subgroup_st_list as $st_id=>$st_FIO)
			{
				$table_subgroup_list.="<tr id='".$st_id."'><td class='st_FIO' id='st_".$st_id."'>".$st_FIO."</td><td class='del_st_subgroup'><div id='del_st_subgroup' data-st_subgroup_options='{\"st_id\":\"".$st_id."\",\"st_FIO\":\"".$st_FIO."\"}'><img src='/img/red-cross.png'></div></td></tr>";
			}
			$table_subgroup_list.="</table>";
			
			
			//если не указан, берем текущую дату
			if ($day_of_month=="")
			{
				$day_of_month=date('Y-m-d');
			}
			
			$st_list=sort_students_by_the_family(get_list_of_students($day_of_month,$group));
			$table_group_list.="<table class='group_list'>";
			$table_group_list.="<tr class='header'><td colspan='2'>Список студентов в группе на ".get_rus_names_for_month(date("d F Y",strtotime($day_of_month)))."</td></tr>";
			foreach ($st_list as $st_id=>$st_FIO)
			{
				$table_group_list.="<tr id='".str_replace("st_","",$st_id)."'><td class='move_st_group'><div id='move_st_group' data-st_group_options='{\"st_id\":\"$st_id\",\"st_FIO\":\"$st_FIO\"}'><img src='/img/green_arrow.png'></div></td><td class='st_FIO' id='".$st_id."'>".$st_FIO."</td></tr>";
			}
			$table_group_list.="</table>";
			
			$html="<div class='save_subgroup'><button id='save_subgroup'>Сохранить список подгруппы</button></div><div id='sub_container'><div class='subgroup_column'>$table_subgroup_list</div><div class='group_column'>$table_group_list</div><div class='clearfix'></div></div>";
			echo json_encode($html);
		}
	elseif (($status=="print_journal_list") && $group && $subgroup && $month && $disc && $prepod)	
		{
			$table="";

			$edu_periods=get_education_periods();
			
			//тянем студентов			
			if (in_array($month,array("9","10","11","12"))) 
			{
				$year=$edu_periods[0]['year']; //первая строка - первый семестр
			} 
				else 
			{
				$year=$edu_periods[1]['year']; 
			}
	
			$month_begin_date=$year."-".$month."-1";
			$last_date=date("t",strtotime($year."-".$month."-1"));
			$month_end_date=$year."-".$month."-".$last_date;

			$st_arr_begin_month=get_list_of_students($month_begin_date,$group);
			$st_arr_end_month=get_list_of_students($month_end_date,$group);

			$common_student_list=sort_students_by_the_family(array_unique(array_merge($st_arr_begin_month,$st_arr_end_month)));
			
			if ($subgroup!='group')
			{
				$subgroup_list=get_subgroup_list($group,$subgroup,$disc);
				get_common_student_list_by_crossing($common_student_list,$subgroup_list);
			}
			
			//тянем занятия
			$lessons_common_array=array();
			$lessons_list=get_one_journal_list_lessons_by_prepod($month,$disc,$group,$prepod);
			
			if (count($lessons_list)>0) //если есть занятия вообще. тогда
				{
					
					$i=1;
					$lessons_id_list=array(); $att_id_list=array();
					
					//формируем "правую" сторону журнала , и общий спсиок занятий
					$table_description="<table id='description_table'>";
					$table_description.="<tr class='table_description_header'><td></td><td></td><td>Дата занятия</td><td>Тема занятия</td><td>Задание для СР</td><td>Тип занятия</td></tr>";
					foreach ($lessons_list as $lesson)
					{
						$lessons_common_array[$lesson['lesson_id']]['lesson_date']=$lesson['lesson_date'];
						$lessons_common_array[$lesson['lesson_id']]['is_att']=$lesson['att'];
						
						if ($lesson['att'])
							{
								$edit_cell='edit_att_'.$lesson['lesson_id'];
								$att_id_list[]=$lesson['lesson_id'];
							}
						else
							{
								$edit_cell='edit_lesson_'.$lesson['lesson_id'];
								$lessons_id_list[]=$lesson['lesson_id'];
							}
						$delete_cell='delete_lesson_'.$lesson['lesson_id'];
						
						$table_description.='<tr>'."<td><div id='$edit_cell' data-options='{\"lesson\":\"$lesson[lesson_id]\",\"date\":\"$lesson[lesson_date]\"}'><img src='/img/doc-edit.png' width='30' height='30'></div></td><td><div id='$delete_cell' data-options='{\"lesson\":\"$lesson[lesson_id]\"}'><img src='/img/red-cross.png' width='30' height='30'></div></td><td>".get_rus_names_for_month(date("j F Y",strtotime($lesson['lesson_date'])))."</td>".($lesson['att']?"<td colspan='3'>".$lesson['theme']."</td>":"<td>".$lesson['theme']."</td>"."<td>".$lesson['homework']."</td>"."<td>".$lesson['lesson_type']."</td>")."</tr>";
						
						$i++;
					}
					$table_description.='</table>';
					
					//формируем "правую" сторону журнала , и общий спсиок занятий
					
					if (count($lessons_id_list)>0)
					{
						//тянем оценки студентов
						$grades_list=get_one_journal_list_grades(implode(',',$lessons_id_list));
						
						$i=1;
						$lesson_id=$student_id="";
						$grade=array();
						foreach ($grades_list as $grade_str)
						{
							if ($lesson_id!=$grade_str['lesson_id'])
								{
									$lesson_id=$grade_str['lesson_id'];
									$grade=array();
								}
							
							if ($student_id!=$grade_str['student_id'])
								{
									$student_id=$grade_str['student_id'];
									$grade=array();
								}
							
							$grade[]=(($grade_str['rate']=='0')?'отр':$grade_str['rate']);

							$lessons_common_array[$lesson_id]['students'][$grade_str['student_id']]['grade']=implode('/',$grade);
							$i++;
						}
						
						//тянем пропуски
						$attend_list=get_one_journal_list_attend(implode(',',$lessons_id_list));
						
						
						$i=1;
						$lesson_id=$student_id="";
						$attend="";
						foreach ($attend_list as $attend_str)
						{
							if ($lesson_id!=$attend_str['lesson_id'])
								{
									$lesson_id=$attend_str['lesson_id'];
									$attend="";
								}
							
							if ($student_id!=$attend_str['student_id'])
								{
									$student_id=$attend_str['student_id'];
									$attend="";
								}
							
							$attend.=$attend_str['case_title']; // как бээ пропуск может быть только один на урок, но хз

							$lessons_common_array[$lesson_id]['students'][$attend_str['student_id']]['attend']['title']=$attend;
							$lessons_common_array[$lesson_id]['students'][$attend_str['student_id']]['attend']['id']=$attend_str['case_id'];
							$i++;
						}
					}
					
					if (count($att_id_list)>0)
					{
						//тянем сведения об аттестации  
						$attest_list=get_one_journal_list_attest(implode(',',$att_id_list));
						
						$i=1;
						$lesson_id=$student_id="";
						$attest="";
						foreach ($attest_list as $attest_str)
						{
							if ($lesson_id!=$attest_str['lesson_id'])
								{
									$lesson_id=$attest_str['lesson_id'];
								}
							
							if ($student_id!=$attest_str['student_id'])
								{
									$student_id=$attest_str['student_id'];
									$attest="";
								}
							
							$attest=$attest_str['att_rate']; // отметка об аттестации совершенно точно одна :)))

							$lessons_common_array[$lesson_id]['students'][$attest_str['student_id']]['attest']=$attest;
							$i++;
						}
					}
					
					//попытаемся теперь запилить итоговую таблицу
					$table.="<table id='grades_table'>";
					$table.='<tr class="table_grades_header"><td class="student_column header">Студенты/Даты занятий</td>';
					foreach ($lessons_common_array as $lesson_key=>$lesson_val)
						{
							$table.="<td id='".($lesson_val['is_att']?'att':'lesson')." $lesson_key' class='".($lesson_val['is_att']?'att_column header':'lesson_column header')."'>".date("j",strtotime($lesson_val['lesson_date']))."</td>";
						}
					$table.="</tr>";
					
					$student_number=1;
					foreach ($common_student_list as $student_id=>$student)
					{
						//смотрим, входит ли студент в подгруппу и выставляем ему класс
						if ($subgroup!='group')
						{
							if (isset($subgroup_list[$student_id]))
							{
								$student_ids=array_keys($subgroup_list);
								//оформление подчеркиванием последней строка
								$class_border_td="";
								if (array_search($student_id,$student_ids)==(count($student_ids)-1))
									$class_border_td="last_subgroup_st";
								$table.="<tr><td class='student_column rows subgroup_st $class_border_td' id='$student_id'>".$student_number.". ".$student."<span class='star_mark'>*</span></td>";
							}
							else
								$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td>";
						}
						else
							$table.="<tr><td class='student_column rows' id='$student_id'>$student_number. $student</td>";
						
							$student_number++;
						
						foreach ($lessons_common_array as $lesson_key=>$lesson_val)
							{
								$total_spell_in_cell="";
								$grade_arr=array(); //оценок нет
								$attend_data="0";  //пропуска нет 
								$attest_data='{"type":"0","grade":"0"}';  //аттестация не установлена 
								
								
								if (isset($lesson_val['students']))
								{
									foreach ($lesson_val['students'] as $student_info_id=>$student_info_val)
									{
										
										if (('st_'.$student_info_id)==$student_id)  // искомый студент - не пустая ячейка
										{
											//в нашем случае может и пропуск стоять, и потом оценка, одновременно 
											if (isset($student_info_val['attend']))
												{
													$total_spell_in_cell.=$student_info_val['attend']['title']." ";
													$attend_data=$student_info_val['attend']['id'];
													
												}	
											if (isset($student_info_val['grade']))
												{
													$total_spell_in_cell.=$student_info_val['grade'];
													$grade_arr=explode('/',$student_info_val['grade']);
												}
											if (isset($student_info_val['attest']))
												{
													$total_spell_in_cell.=get_rus_names_for_attest_label($student_info_val['attest']);
													
													//остановимся на фиксированном списке для отметок об аттестации
													$attest_data=get_attest_data($student_info_val['attest']);
												}	
										}
										
									}
								}
								
								//формируем поле data
								if (count($grade_arr)>0)
								{
									$i=1;
									$g_str=array();
									foreach ($grade_arr as $gr)
									{
										$g_str[]='{"number":"'.$i.'","grade":"'.(($gr=='отр')?'0':$gr).'"}';
										$i++;
									}
									$grade_arr='['.implode(',',$g_str).']';
								}
								else
								{
									$grade_arr='[]';
								}
								
								$class_border_td="";
								if ($subgroup!='group')
								{
									if (isset($subgroup_list[$student_id]))
									{
										$student_ids=array_keys($subgroup_list);
										//оформление подчеркиванием последней строки
										if (array_search($student_id,$student_ids)==(count($student_ids)-1))
											$class_border_td="last_subgroup_st";
									}
								}
								
								
								if (!$lesson_val['is_att']) //если урок
								{
									$table.="<td id='GradeCell ".$student_id."_les_".$lesson_key."'"." class='lesson_column rows $class_border_td' "." data-options='{\"lesson\":\"$lesson_key\",\"student_id\":\"$student_id\"}' data-gr_att='{\"grade\":$grade_arr,\"attend\":\"$attend_data\"}'>".$total_spell_in_cell."</td>";
								}
								else //а если аттестация
								{
									$table.="<td id='AttCell ".$student_id."_les_".$lesson_key."'"." class='att_column rows $class_border_td' "." data-options='{\"lesson\":\"$lesson_key\",\"student_id\":\"$student_id\"}' data-attest='$attest_data'>".$total_spell_in_cell."</td>";
								}
								
							}
						$table.="</tr>";	
					}
					$table.='</table>';
					
				}
			else //а если их нет, тогда список студентов и заголовок таблицы КТП
				{
					$table=""; $table_description="";
					create_journal_tables_for_zero_lessons();
				}
			$html='<div style="">'.$table.'</div><div style="margin-top:80px;">'.$table_description.'</div><div style="clear:both;"></div><div><div id="add_lesson">Добавить занятие</div><div id="add_att">Добавить аттестацию</div></div>';	
			//echo json_encode($html);
			
			
			
			require_once '../Classes/PHPExcel.php';
			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
										 ->setLastModifiedBy("Maarten Balliauw")
										 ->setTitle("Office 2007 XLSX Test Document")
										 ->setSubject("Office 2007 XLSX Test Document")
										 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
										 ->setKeywords("office 2007 openxml php")
										 ->setCategory("Test result file");
			// Add some data, we will use printing features
			for ($i = 1; $i < 200; $i++) {
				$objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $i);
				$objPHPExcel->getActiveSheet()->setCellValue('B' . $i, 'Test value');
			}

			// Set header and footer. When no different headers for odd/even are used, odd header is assumed.

			$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddHeader('&L&G&C&HPlease treat this document as confidential!');
			$objPHPExcel->getActiveSheet()->getHeaderFooter()->setOddFooter('&L&B' . $objPHPExcel->getProperties()->getTitle() . '&RPage &P of &N');

			// Add a drawing to the header

			$objPHPExcel->getActiveSheet()->getHeaderFooter()->addImage($objDrawing, PHPExcel_Worksheet_HeaderFooter::IMAGE_HEADER_LEFT);

			// Set page orientation and size

			$objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
			$objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);

			// Rename worksheet

			$objPHPExcel->getActiveSheet()->setTitle('Printing');


			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);


			// Save Excel 2007 file

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save(str_replace('.php', '.xls', __FILE__));
			echo json_encode("File has been saved");
		}	
}


?>