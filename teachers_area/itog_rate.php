<?php  if ( ! defined('SELF')) exit('No direct script access allowed'); ?>
<script src="../js/Itog_Rate.js" type="text/javascript"></script>
<h2>Сводная ведомость по дисциплине за семестр</h2>
<br>
<?php
			$selected_group=$selected_disc=$selected_semestr="";


			$MsgText=check_refs_filling();
			if ($MsgText=='Success')
				{
					$refs_filled=1;
				}
			else
				{
					$refs_filled=0;
					echo $MsgText;
				}

			if ($refs_filled)
				{
?>
<div id="Toolbar_Panel">
	<div class="journal_options_panel">
		<table class="Group_UI_Tools">
			<tr>
				<td>
					<span id="label_group"><b>Группа</b></span>
				</td>
				<td>
					<select name="group" id="select_group">
						<?php
							$result_array=get_nagr_strings();
							$i=1;
							foreach($result_array as $d){
								if ($i==1)
									{
										$where=$d['group_id'];
									}

								if ($d['group_id']==$where)
									{
										$selected='selected';
										$selected_group=$d['group_id'];
									}
								else
									{
										$selected='';
									}

								echo '<option class="save" '.$selected.' value="'.$d['group_id'].'" >'.$d['literal'].''."\n";
								$i++;
							}
						?>
					</select>
				</td>
				<td>
					<span id="label_subgroup"><b>Подгруппа</b></span>
				</td>
				<td>
					<select name="subgroup" id="select_subgroup">
						<option class="save" selected value='group'>Группа целиком</option>
						<option class="save" value='1'>1</option>
						<option class="save" value='2'>2</option>
						<option class="save" value='3'>3</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<span id="label_disc"><b>Дисциплина</b></span>
				</td>
				<td colspan="3">
					<select name="disc" id="select_disc">
						<?php
								$result_array=get_disciplines_by_group($selected_group);
								$i=1;

								foreach ($result_array as $d){
									if ($i==1)
										{
											$where=$d['discipline_id'];
										}

									if ($d['discipline_id']==$where)
										{
											$selected='selected';
											$selected_disc=$d['discipline_id'];
										}
									else
										{
											$selected='';
										}

									echo '<option class="save" '.$selected.' value="'.$d['discipline_id'].'" >'.$d['title'].''."\n";
									$i++;
								}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<span id="label_disc"><b>Семестр</b></span>
				</td>
				<td colspan="3">
					<select name="semestr" id="select_semestr">

			<?php

					$result_array=get_semestr_by_disc($selected_group,$selected_disc);
					$i=1;
					foreach ($result_array as $d)
					{
						if ($i==1)
								{
									$where=$d;
								}

							if ($d==$where)
								{
								$selected='selected';
								$selected_semestr=$d;
								}
							else
								{
								$selected='';
								}
							echo '<option class="save" '.$selected.' value="'.$d.'" >'.($d=='1'?'1 семестр':'2 семестр').''."\n";
						$i++;
					}
			?>

					</select>
				</td>
			</tr>
			<tr>
				<td>
					<span id="label_prepod"><b>Преподаватель/и</b></span>
				</td>
				<td colspan="3">
					<?php
							$result_array=get_prepod_by_semestr($selected_group,$selected_disc,$selected_semestr);
							
							$FIO_pr=array();
							foreach ($result_array as $d) //в нашем случае один препод, но на всякий...
							{
								$FIO_pr[]=$d['prep_FIO'];
							}
							$FIO_pr=implode(", ",$FIO_pr);
							echo '<div class="prepod_FIO" >'.$FIO_pr.'</div>';
					?>
				</td>
			</tr>
			<tr>
				<td colspan="4">
				<button id="load_journal">Открыть ведомость</button>
				</td>
			</tr>
		</table>
	</div>
	<div class="fake_place"></div>
	<div class="itog_rate_options">
		<table class="Group_UI_Tools">
				<tr>
					<td><span id="vid_kontrol"><b>Форма контроля</b></span></td>
					<td>
						<select name="vid_control_select" class="vid_control_select" id="vid_control_select">
								<option value="zach" selected>Зачет</option>
								<option value="grade">Экзамен/оценка по 5-бальной шкале</option>
								<option value="dif_zach" >Диф.зачет</option>
						</select>
					</td>
				</tr>
		</table>		
	</div>
	<div class="clearfix"></div>
</div>

<div id="ajax_status"><div class="loading_progress"><img src="/img/ico-loading.gif"></div><div class="loading_label">Идет загрузка...</div></div>
<div id="Journal_Page" class="Area_IS_Granted"></div>
<div id="Overlay_Access_Denied"></div>

<div id="Total_Grade_form" style="display:none;">
	<form>
		<table class="hidden_lines">
		<tr>
			<td><span id="total_grade_select_label"><b>Вид оценивания</b></span></td>
			<td>
				<select name="total_grade_select" class="total_grade_select" id="total_grade_select">
						<option value="zach" selected>Зачет</option>
						<option value="grade">Экзамен</option>
						<option value="dif_zach" >Диф.зачет</option>
				</select>
			</td>
			<td><span id="total_grade_zach_label"><b>Зачет</b></span><input type="checkbox" name="total_grade_zach" id="total_grade_zach"></input></td>
			<td><span id="total_grade_rate_label"><b>Оценка</b></span></td>
			<td>
				<select name="total_grade_rate_select" class="total_grade_rate_select" id="total_grade_rate_select">
						<option value="2" selected>2</option>
						<option value="3" >3</option>
						<option value="4" >4</option>
						<option value="5" >5</option>
				</select>
			</td>
		</tr>
		</table>
	</form>
	<div id="total_grade_status"></div>
	<div id="total_grade_data" style="display:none;"></div>
</div>
<div id="Semestr_Grade_form" style="display:none;">
	<form>
		<table class="hidden_lines">
		<tr>
			<td><span id="semestr_grade_select_label"><b>Вид оценивания</b></span></td>
			<td>
				<select name="semestr_grade_select" class="semestr_grade_select" id="semestr_grade_select">
						<option value="zach" selected>Зачет</option>
						<option value="grade">Оценка</option>
				</select>
			</td>
			<td><span id="semestr_grade_zach_label"><b>Зачет</b></span><input type="checkbox" name="semestr_grade_zach" id="semestr_grade_zach"></input></td>
		
			<td><span id="semestr_grade_rate_label"><b>Оценка</b></span></td>
			<td>
				<select name="semestr_grade_rate_select" class="semestr_grade_rate_select" id="semestr_grade_rate_select">
						<option value="2" selected>2</option>
						<option value="3" >3</option>
						<option value="4" >4</option>
						<option value="5" >5</option>
				</select>
			</td>
		</tr>
		</table>
	</form>
	<div id="semestr_grade_status"></div>
	<div id="semestr_grade_data" style="display:none;"></div>
</div>
<?php
			}
?>